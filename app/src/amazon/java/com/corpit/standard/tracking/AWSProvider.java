package com.corpit.standard.tracking;

import android.content.Context;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.mobile.auth.core.IdentityManager;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.pinpoint.PinpointConfiguration;
import com.amazonaws.mobileconnectors.pinpoint.PinpointManager;

import java.lang.ref.WeakReference;

public class AWSProvider {

    private static final String TAG = "AWSProvider";

    private static volatile AWSProvider instance = null;

    private WeakReference<Context> contextRef;
    private AWSConfiguration awsConfiguration;
    private PinpointManager pinpointManager;

    public static AWSProvider getInstance() {
        return instance;
    }

    public static void initialize(Context context) {
        if (instance == null) {
            instance = new AWSProvider(context);
        }
    }

    private AWSProvider(Context context) {
        contextRef = new WeakReference<>(context);

        this.awsConfiguration = new AWSConfiguration(context);

        if (IdentityManager.getDefaultIdentityManager() == null) {
            IdentityManager identityManager = new IdentityManager(context, awsConfiguration);
            IdentityManager.setDefaultIdentityManager(identityManager);
        }

    }

    public Context getContext() {
        if (contextRef != null) {
            return contextRef.get();
        }
        return null;
    }

    public AWSConfiguration getConfiguration() {
        return this.awsConfiguration;
    }

    public IdentityManager getIdentityManager() {
        return IdentityManager.getDefaultIdentityManager();
    }

    public PinpointManager getPinpointManager() {
        if (pinpointManager == null) {
            final AWSCredentialsProvider cp = getIdentityManager().getCredentialsProvider();
            PinpointConfiguration config = new PinpointConfiguration(
                    getContext(), cp, getConfiguration());
            pinpointManager = new PinpointManager(config);
        }
        return pinpointManager;
    }
}
