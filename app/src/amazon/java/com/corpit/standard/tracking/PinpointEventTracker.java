package com.corpit.standard.tracking;

import com.amazonaws.mobileconnectors.pinpoint.analytics.AnalyticsClient;
import com.amazonaws.mobileconnectors.pinpoint.analytics.AnalyticsEvent;
import com.corpit.standard.model.Conference;
import com.corpit.standard.model.Event;
import com.corpit.standard.model.ExhibitingCompany;
import com.corpit.standard.model.GeneralInfo;
import com.corpit.standard.model.Location;
import com.corpit.standard.model.Menu;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.model.Sponsor;
import com.corpit.standard.tracking.AWSProvider;
import com.corpit.standard.tracking.EventTracker;

/**
 * Helper class to track event. Events will be sent to AWS after the session ended.
 */
public class PinpointEventTracker implements EventTracker {

    @Override
    public void trackSpeaker(Speaker speaker) {
        final AnalyticsClient mgr = AWSProvider.getInstance()
                .getPinpointManager()
                .getAnalyticsClient();
        final AnalyticsEvent evt = mgr.createEvent(SPEAKER_EVENT_TYPE)
                .withAttribute(SPEAKER_NAME, speaker.getFullName())
                .withAttribute(SPEAKER_ID, speaker.getId());
        mgr.recordEvent(evt);
    }

    @Override
    public void trackSponsor(Sponsor sponsor) {
        final AnalyticsClient mgr = AWSProvider.getInstance()
                .getPinpointManager()
                .getAnalyticsClient();
        final AnalyticsEvent evt = mgr.createEvent(SPONSOR_EVENT_TYPE)
                .withAttribute(SPONSOR_ATTR_NAME, sponsor.getName())
                .withAttribute(SPONSOR_ATTR_ID, sponsor.getId());
        mgr.recordEvent(evt);
    }

    @Override
    public void trackConference(Conference conference) {
        final AnalyticsClient mgr = AWSProvider.getInstance()
                .getPinpointManager()
                .getAnalyticsClient();
        final AnalyticsEvent evt = mgr.createEvent(CONFERENCE_EVENT_TYPE)
                .withAttribute(CONFERENCE_TITLE, String.format("%s %s-%s %s",
                        conference.getDate(), conference.getStartTime(), conference.getEndTime(),
                        conference.getTitle()))
                .withAttribute(CONFERENCE_ID, conference.getId());
        mgr.recordEvent(evt);
    }

    @Override
    public void trackEventSchedule(Event event) {
        final AnalyticsClient mgr = AWSProvider.getInstance()
                .getPinpointManager()
                .getAnalyticsClient();
        final AnalyticsEvent evt = mgr.createEvent(EVENT_EVENT_TYPE)
                .withAttribute(EVENT_TITLE, String.format("%s %s %s",
                        event.getDate(), event.getTime(),
                        event.getTitle()))
                .withAttribute(EVENT_ID, event.getId());
        mgr.recordEvent(evt);
    }

    @Override
    public void trackExhibitor(ExhibitingCompany exhibitor) {
        final AnalyticsClient mgr = AWSProvider.getInstance()
                .getPinpointManager()
                .getAnalyticsClient();
        final AnalyticsEvent evt = mgr.createEvent(EXHIBITOR_EVENT_TYPE)
                .withAttribute(EXHIBITOR_COMPANY_NAME, exhibitor.getName())
                .withAttribute(EXHIBITOR_HALL, exhibitor.getHall())
                .withAttribute(EXHIBITOR_BOOTH, exhibitor.getBoothId())
                .withAttribute(EXHIBITOR_ID, exhibitor.getId());
        mgr.recordEvent(evt);
    }

    @Override
    public void trackFloorplan(Location floorplan) {
        final AnalyticsClient mgr = AWSProvider.getInstance()
                .getPinpointManager()
                .getAnalyticsClient();
        final AnalyticsEvent evt = mgr.createEvent(FLOORPLAN_EVENT_TYPE)
                .withAttribute(FLOORPLAN_HALL_NAME, floorplan.getName())
                .withAttribute(FLOORPLAN_ID, floorplan.getId());
        mgr.recordEvent(evt);
    }

    @Override
    public void trackGeneralInformation(GeneralInfo info) {
        final AnalyticsClient mgr = AWSProvider.getInstance()
                .getPinpointManager()
                .getAnalyticsClient();
        final AnalyticsEvent evt = mgr.createEvent(INFO_EVENT_TYPE)
                .withAttribute(INFO_TITLE, info.getTitle())
                .withAttribute(INFO_ID, info.getId());
        mgr.recordEvent(evt);
    }

    @Override
    public void trackMenuClick(Menu menu) {
        final AnalyticsClient mgr = AWSProvider.getInstance()
                .getPinpointManager()
                .getAnalyticsClient();
        final AnalyticsEvent evt = mgr.createEvent(MENU_EVENT_TYPE)
                .withAttribute(MENU_TITLE, menu.getEnTitle())
                .withAttribute(MENU_ID, menu.getId());
        mgr.recordEvent(evt);
    }

    @Override
    public void onPageStart(String tag) {

    }

    @Override
    public void onPageEnd(String tag) {

    }
}
