package com.corpit.standard.di;

import android.app.Application;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.amazonaws.mobile.auth.core.IdentityHandler;
import com.amazonaws.mobile.auth.core.IdentityManager;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.AWSStartupHandler;
import com.amazonaws.mobile.client.AWSStartupResult;
import com.amazonaws.mobileconnectors.pinpoint.targeting.endpointProfile.EndpointProfile;
import com.amazonaws.mobileconnectors.pinpoint.targeting.endpointProfile.EndpointProfileUser;
import com.corpit.standard.BuildConfig;
import com.corpit.standard.tracking.AWSProvider;
import com.corpit.standard.tracking.ActivityLifeCycle;
import com.corpit.standard.tracking.EventTracker;
import com.corpit.standard.tracking.PinpointEventTracker;

import java.util.Locale;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TrackModule {

    private static final String TAG = "TrackModule";

    public TrackModule(Application application) {
        AWSProvider.initialize(application);
        application.registerActivityLifecycleCallbacks(new ActivityLifeCycle());

        String countryCode = getUserCountry(application);

        AWSMobileClient.getInstance()
                .initialize(application, new AWSHandler(countryCode))
                .execute();
    }

    @Provides
    @Singleton
    EventTracker eventTracker() {
        return new PinpointEventTracker();
    }

    /**
     * Get ISO 3166-1 alpha-2 country code for this device (or null if not available)
     *
     * @param context Context reference to get the TelephonyManager instance from
     * @return country code or null
     */
    private String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry;
            if (tm != null) {
                simCountry = tm.getSimCountryIso();
                if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                    return simCountry.toLowerCase(Locale.US);
                } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                    String networkCountry = tm.getNetworkCountryIso();
                    if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                        return networkCountry.toLowerCase(Locale.US);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static class AWSHandler implements AWSStartupHandler {

        String countryCode;

        AWSHandler(String countryCode) {
            this.countryCode = countryCode;
        }

        @Override
        public void onComplete(AWSStartupResult awsStartupResult) {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "AWSMobileClient is instantiated and you are connected to AWS!");

            // Use IdentityManager#getUserID to fetch the identity id.
            IdentityManager.getDefaultIdentityManager().getUserID(new IdentityHandler() {
                @Override
                public void onIdentityId(String identityId) {
                    Log.d(TAG, "Identity ID = " + identityId);

                    // Use IdentityManager#getCachedUserID to
                    //  fetch the locally cached identity id.
                    final String cachedIdentityId =
                            IdentityManager.getDefaultIdentityManager().getCachedUserID();

                    EndpointProfile endpointProfile = AWSProvider.getInstance()
                            .getPinpointManager()
                            .getTargetingClient()
                            .currentEndpoint();

                    EndpointProfileUser user = new EndpointProfileUser();
                    user.setUserId(cachedIdentityId);
                    endpointProfile.setUser(user);

                    endpointProfile.getDemographic().setLocale(
                            new Locale(Locale.getDefault().getLanguage(), countryCode));
                    endpointProfile.getLocation().setCountry(countryCode);
                    AWSProvider.getInstance()
                            .getPinpointManager()
                            .getTargetingClient()
                            .updateEndpointProfile(endpointProfile);
                }

                @Override
                public void handleError(Exception exception) {
                    if (BuildConfig.DEBUG)
                        Log.d(TAG, "Error in retrieving the identity" + exception);
                }
            });
        }
    }
}
