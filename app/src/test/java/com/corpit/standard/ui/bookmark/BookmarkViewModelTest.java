package com.corpit.standard.ui.bookmark;

import com.corpit.standard.model.BookmarkDao;
import com.corpit.standard.share.StandardApp;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class BookmarkViewModelTest {

    @Mock
    private BookmarkDao bookmarkDao;

    @Mock
    private StandardApp mContext;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private BookmarkViewModel viewModel;

    @Before
    public void setupBookmarkViewModel() {
        MockitoAnnotations.initMocks(this);

        setupContext();

        viewModel = new BookmarkViewModel(mContext);

    }

    private void setupContext() {
        when(mContext.getApplicationContext()).thenReturn(mContext);
    }

    @Test
    public void loadBookmarks() {
        viewModel.loadBookmarks();

        assertTrue(viewModel.isEmpty.get());
    }
}