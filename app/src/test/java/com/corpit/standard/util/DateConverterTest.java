package com.corpit.standard.util;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DateConverterTest {

    @Test
    public void formatTimeElapsed() {
        assertTrue("3 Mar".equals(DateConverter.formatTimeElapsed("1520063889.1234", 1524563943)));
    }
}