package com.corpit.standard.tracking;

import android.content.Context;
import android.support.annotation.NonNull;

import com.corpit.standard.model.Conference;
import com.corpit.standard.model.Event;
import com.corpit.standard.model.ExhibitingCompany;
import com.corpit.standard.model.GeneralInfo;
import com.corpit.standard.model.Location;
import com.corpit.standard.model.Menu;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.model.Sponsor;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

public class UmengEventTracker implements EventTracker {

    private Context mContext;

    public UmengEventTracker(Context context) {
        mContext = context.getApplicationContext();
    }

    private void trackSpeaker(@NonNull Context context, @NonNull Speaker speaker) {
        HashMap<String, String> data = new HashMap<>();
        data.put(SPEAKER_NAME, speaker.getFullName());
        data.put(SPEAKER_ID, speaker.getId());
        MobclickAgent.onEvent(context, SPEAKER_EVENT_TYPE, data);
    }

    private void trackSponsor(@NonNull Context context, @NonNull Sponsor sponsor) {
        HashMap<String, String> data = new HashMap<>();
        data.put(SPONSOR_ATTR_NAME, sponsor.getName());
        data.put(SPONSOR_ATTR_ID, sponsor.getId());
        MobclickAgent.onEvent(context, SPONSOR_EVENT_TYPE, data);
    }

    private void trackConference(@NonNull Context context, @NonNull Conference conference) {
        HashMap<String, String> data = new HashMap<>();
        data.put(CONFERENCE_TITLE, String.format("%s %s-%s %s",
                conference.getDate(), conference.getStartTime(), conference.getEndTime(),
                conference.getTitle()));
        data.put(CONFERENCE_ID, conference.getId());
        MobclickAgent.onEvent(context, CONFERENCE_TITLE, data);
    }

    private void trackEventSchedule(@NonNull Context context, @NonNull Event event) {
        HashMap<String, String> data = new HashMap<>();
        data.put(EVENT_TITLE, String.format("%s %s %s",
                event.getDate(), event.getTime(),
                event.getTitle()));
        data.put(EVENT_ID, event.getId());
        MobclickAgent.onEvent(context, EVENT_EVENT_TYPE, data);
    }

    private void trackExhibitor(@NonNull Context context, @NonNull ExhibitingCompany exhibitor) {
        HashMap<String, String> data = new HashMap<>();
        data.put(EXHIBITOR_COMPANY_NAME, exhibitor.getName());
        data.put(EXHIBITOR_HALL, exhibitor.getHall());
        data.put(EXHIBITOR_BOOTH, exhibitor.getBoothId());
        data.put(EXHIBITOR_ID, exhibitor.getId());
        MobclickAgent.onEvent(context, EXHIBITOR_EVENT_TYPE, data);
    }

    private void trackFloorplan(@NonNull Context context, @NonNull Location floorplan) {
        HashMap<String, String> data = new HashMap<>();
        data.put(FLOORPLAN_HALL_NAME, floorplan.getName());
        data.put(FLOORPLAN_ID, floorplan.getId());
        MobclickAgent.onEvent(context, FLOORPLAN_EVENT_TYPE, data);
    }

    private void trackGeneralInformation(@NonNull Context context, @NonNull GeneralInfo info) {
        HashMap<String, String> data = new HashMap<>();
        data.put(INFO_TITLE, info.getTitle());
        data.put(INFO_ID, info.getId());
        MobclickAgent.onEvent(context, INFO_EVENT_TYPE, data);
    }

    private void trackMenuClick(@NonNull Context context, @NonNull Menu menu) {
        HashMap<String, String> data = new HashMap<>();
        data.put(MENU_EVENT_TYPE, menu.getEnTitle());
        data.put(MENU_ID, menu.getId());
        MobclickAgent.onEvent(context, MENU_EVENT_TYPE, data);
    }

    @Override
    public void trackSpeaker(Speaker speaker) {
        trackSpeaker(mContext, speaker);
    }

    @Override
    public void trackSponsor(Sponsor sponsor) {
        trackSponsor(mContext, sponsor);
    }

    @Override
    public void trackConference(Conference conference) {
        trackConference(mContext, conference);
    }

    @Override
    public void trackEventSchedule(Event event) {
        trackEventSchedule(mContext, event);
    }

    @Override
    public void trackExhibitor(ExhibitingCompany exhibitor) {
        trackExhibitor(mContext, exhibitor);
    }

    @Override
    public void trackFloorplan(Location floorplan) {
        trackFloorplan(mContext, floorplan);
    }

    @Override
    public void trackGeneralInformation(GeneralInfo info) {
        trackGeneralInformation(mContext, info);
    }

    @Override
    public void trackMenuClick(Menu menu) {
        trackMenuClick(mContext, menu);
    }

    @Override
    public void onPageStart(String tag) {
        MobclickAgent.onPageStart(tag);
    }

    @Override
    public void onPageEnd(String tag) {
        MobclickAgent.onPageEnd(tag);
    }
}
