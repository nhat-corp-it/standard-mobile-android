package com.corpit.standard.di;

import android.app.Application;
import android.content.Context;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.di.module.AppModule;
import com.corpit.standard.tracking.EventTracker;
import com.corpit.standard.tracking.UmengEventTracker;
import com.umeng.analytics.MobclickAgent;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {AppModule.class})
public class TrackModule {

    public TrackModule(Application application) {
        MobclickAgent.UMAnalyticsConfig config = new MobclickAgent.UMAnalyticsConfig(
                application,
                BuildConfig.UMENG_APPKEY,
                BuildConfig.UMENG_CHANNEL,
                MobclickAgent.EScenarioType.E_UM_NORMAL);
        MobclickAgent.startWithConfigure(config);
    }

    @Provides
    @Singleton
    EventTracker eventTracker(Context context) {
        return new UmengEventTracker(context);
    }
}
