package com.corpit.standard.di.module;

import android.content.Context;
import android.util.Log;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.R;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.Configuration;
import com.corpit.standard.model.DaoMaster;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.model.DataVersion;
import com.corpit.standard.model.EPoster;
import com.corpit.standard.model.Inbox;
import com.corpit.standard.model.Menu;
import com.corpit.standard.model.News;
import com.corpit.standard.model.PollingConference;
import com.corpit.standard.util.BootstrapHelper;

import org.greenrobot.greendao.database.Database;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.corpit.standard.model.DaoMaster.dropAllTables;

@Module(includes = AppModule.class)
public class DaoModule {

    private static final String DATABASE_NAME = "Standard2018v1.db";

    @Provides
    @Singleton
    DaoSession daoSession(Context context) {
        StandardOpenHelper helper = new StandardOpenHelper(context.getApplicationContext(), DATABASE_NAME);
        Database db = helper.getWritableDb();
        return new DaoMaster(db).newSession();
    }

    @Provides
    ColorScheme colorScheme(DaoSession daoSession) {
        return daoSession.getColorSchemeDao().loadAll().get(0);
    }

    @Provides
    Configuration configuration(DaoSession daoSession) {
        return daoSession.getConfigurationDao().loadAll().get(0);
    }

    public static class StandardOpenHelper extends DaoMaster.OpenHelper {

        private Context context;

        StandardOpenHelper(Context context, String name) {
            super(context, name);
            this.context = context;
        }

        @Override
        public void onCreate(Database db) {
            super.onCreate(db);

            DaoSession daoSession = new DaoMaster(db).newSession();

            BootstrapHelper.addBootstrap(context, daoSession, DataVersion.class, R.raw.dataversion);
            BootstrapHelper.addBootstrap(context, daoSession, ColorScheme.class, R.raw.colorscheme);
            BootstrapHelper.addBootstrap(context, daoSession, Configuration.class, R.raw.configuration);
            BootstrapHelper.addBootstrap(context, daoSession, Menu.class, R.raw.menu);

            if (BuildConfig.DEBUG) {
                BootstrapHelper.addBootstrap(context, daoSession, EPoster.class, R.raw.eposter);
                BootstrapHelper.addBootstrap(context, daoSession, News.class, R.raw.news);
                BootstrapHelper.addBootstrap(context, daoSession, Inbox.class, R.raw.inbox);
                BootstrapHelper.addBootstrap(context, daoSession, PollingConference.class, R.raw.polling);
            }
        }

        @Override
        public void onUpgrade(Database db, int oldVersion, int newVersion) {
            if (BuildConfig.DEBUG)
                Log.i("greenDAO", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");
            dropAllTables(db, true);
            onCreate(db);
        }
    }
}
