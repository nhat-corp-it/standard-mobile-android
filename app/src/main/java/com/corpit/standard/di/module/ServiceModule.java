package com.corpit.standard.di.module;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.data.remote.StandardRetrofitService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ServiceModule {

    private static final String ROOT_URL = BuildConfig.API_ROOT_URL;

    @Provides
    @Singleton
    Retrofit retrofit() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    StandardRetrofitService apiService(Retrofit retrofit) {
        return retrofit.create(StandardRetrofitService.class);
    }
}
