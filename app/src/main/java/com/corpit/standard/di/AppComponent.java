package com.corpit.standard.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.corpit.standard.data.remote.StandardRetrofitService;
import com.corpit.standard.di.module.AppModule;
import com.corpit.standard.di.module.DaoModule;
import com.corpit.standard.di.module.ServiceModule;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.Configuration;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.tracking.EventTracker;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, DaoModule.class, ServiceModule.class, TrackModule.class})
@Singleton
public interface AppComponent {

    Context appContext();

    SharedPreferences sharedPreferences();

    StandardRetrofitService apiService();

    DaoSession daoSession();

    ColorScheme colorScheme();

    Configuration configuration();

    EventTracker eventTracker();

}
