package com.corpit.standard.di;

import com.corpit.standard.share.StandardApp;

public class Injector {

    public static AppComponent get() {
        return StandardApp.get().getComponent();
    }
}
