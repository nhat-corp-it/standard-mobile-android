package com.corpit.standard.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.corpit.standard.BuildConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static android.content.Context.MODE_PRIVATE;

@Module
public class AppModule {

    private final Application app;

    public AppModule(Application application) {
        app = application;
    }

    @Provides
    @Singleton
    protected Context appContext() {
        return app.getApplicationContext();
    }

    @Provides
    @Singleton
    protected SharedPreferences sharedPreferences() {
        return app.getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
    }

}
