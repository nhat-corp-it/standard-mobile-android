package com.corpit.standard.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.corpit.standard.R;
import com.corpit.standard.util.SizeConverter;


public class AssortView extends AppCompatButton {

    public static final int NORMAL_TEXT_SIZE = 20;
    public static final int SELECTED_TEXT_SIZE = 30;
    private String[] assort = {"#", "A", "B", "C", "D", "E", "F", "G",
            "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
            "U", "V", "W", "X", "Y", "Z"};
    private Paint paint = new Paint();
    private int selectIndex = -1;
    private OnTouchAssortListener onTouch;

    public AssortView(Context context) {
        super(context);
    }

    public AssortView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AssortView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnTouchAssortListener(OnTouchAssortListener onTouch) {
        this.onTouch = onTouch;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = getHeight();
        int width = getWidth();
        int interval = height / assort.length;
        int length = assort.length;

        for (int i = 0; i < length; i++) {
            // 抗锯齿
            paint.setAntiAlias(true);
            // 默认粗体

            // 白色
            paint.setColor(Color.WHITE);
            paint.setTextSize(SizeConverter.dipToPixel(NORMAL_TEXT_SIZE, getContext()));
            if (i == selectIndex) {
                // 被选择的字母改变颜色和粗体
                paint.setColor(getContext().getResources().getColor(R.color.colorPrimary));
                paint.setFakeBoldText(true);
                paint.setTextSize(SizeConverter.dipToPixel(SELECTED_TEXT_SIZE, getContext()));
            }
            // 计算字母的X坐标
            float xPos = width / 2 - paint.measureText(assort[i]) / 2;
            // 计算字母的Y坐标
            float yPos = interval * i + interval;
            canvas.drawText(assort[i], xPos, yPos, paint);
            paint.reset();
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        float y = event.getY();
        int index = (int) (y / getHeight() * assort.length);
        if (index >= 0 && index < assort.length) {

            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:

                    if (selectIndex != index) {
                        selectIndex = index;
                        if (onTouch != null) {
                            onTouch.onTouchAssortListener(assort[selectIndex]);
                        }

                    }
                    break;
                case MotionEvent.ACTION_DOWN:
                    selectIndex = index;
                    if (onTouch != null) {
                        onTouch.onTouchAssortListener(assort[selectIndex]);
                    }

                    break;
                case MotionEvent.ACTION_UP:
                    if (onTouch != null) {
                        onTouch.onTouchAssortUp();
                    }
                    selectIndex = -1;
                    break;
            }
        } else {
            selectIndex = -1;
            if (onTouch != null) {
                onTouch.onTouchAssortUp();
            }
        }
        invalidate();

        return true;
    }

    public interface OnTouchAssortListener {
        void onTouchAssortListener(String s);

        void onTouchAssortUp();
    }

}
