package com.corpit.standard.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.util.SizeConverter;
import com.corpit.standard.util.VectorUtil;

/**
 * ThaiFex custom view for category list in exhibitor page
 */
public class CategoryTextView extends LinearLayout {

    private static final int BORDER_WIDTH = 2;

    private String mText;
    private int mIndicatorColor = getResources().getColor(R.color.colorPrimary);

    public CategoryTextView(Context context) {
        super(context);
        setOrientation(VERTICAL);
        LayoutInflater.from(context).inflate(R.layout.category_text_view, this, true);
        init(null, 0);
    }

    public CategoryTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        LayoutInflater.from(context).inflate(R.layout.category_text_view, this, true);
        init(attrs, 0);
    }

    public CategoryTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOrientation(VERTICAL);
        LayoutInflater.from(context).inflate(R.layout.category_text_view, this, true);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray typedArray = getContext().obtainStyledAttributes(
                attrs, R.styleable.CategoryTextView, defStyle, 0);

        mText = typedArray.getString(R.styleable.CategoryTextView_text);

        if (mText == null) mText = "";
        ((TextView) findViewById(android.R.id.text1)).setText(mText);

        Drawable drawable = VectorUtil.getVector(R.drawable.ic_arrow_right, getContext());
        ((TextView) findViewById(android.R.id.text1))
                .setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);

        mIndicatorColor = typedArray.getColor(R.styleable.CategoryTextView_indicatorColor, getResources().getColor(R.color.colorPrimary));
        findViewById(R.id.indicator).setBackgroundColor(mIndicatorColor);
        GradientDrawable background = (GradientDrawable) findViewById(R.id.layout).getBackground();
        background.setStroke(SizeConverter.dipToPixel(BORDER_WIDTH, getContext()), mIndicatorColor);

        typedArray.recycle();
    }

    /**
     * Gets the text attribute value.
     *
     * @return The text displayed by the text view.
     */
    public String getText() {
        return mText;
    }

    /**
     * Sets the view's example string attribute value. In the example view, this string
     * is the text to draw.
     *
     * @param text The example string attribute value to use.
     */
    public void setText(String text) {
        mText = text;
        ((TextView) findViewById(R.id.text)).setText(mText);
    }

    /**
     * Gets the example color attribute value.
     *
     * @return The example color attribute value.
     */
    public int getIndicatorColor() {
        return mIndicatorColor;
    }

    /**
     * Sets the view's example color attribute value. In the example view, this color
     * is the font color.
     *
     * @param color The example color attribute value to use.
     */
    public void setIndicatorColor(int color) {
        mIndicatorColor = color;
        findViewById(R.id.indicator).setBackgroundColor(mIndicatorColor);
        GradientDrawable background = (GradientDrawable) findViewById(R.id.layout).getBackground();
        background.setStroke(SizeConverter.dipToPixel(BORDER_WIDTH, getContext()), mIndicatorColor);
    }

}
