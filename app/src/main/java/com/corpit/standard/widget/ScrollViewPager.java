package com.corpit.standard.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class ScrollViewPager extends AutoScrollViewPager {

    public ScrollViewPager(Context paramContext) {
        super(paramContext);
    }

    public ScrollViewPager(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int height = 0;
        for(int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight();
            if(h > height) height = h;
        }

        if (height != 0) {
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
