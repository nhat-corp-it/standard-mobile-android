package com.corpit.standard.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.corpit.standard.R;
import com.corpit.standard.model.Booth;
import com.corpit.standard.ui.floorplans.OverallMapFragment;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

public class PinView extends SubsamplingScaleImageView {

    private final Paint paint = new Paint();
    private PointF sPin;
    private Bitmap pin;

    private Paint mPaint;

    private RectF mBoothRect;

    private Booth booth;

    private OverallMapFragment.Venue venue;

    public PinView(Context context) {
        this(context, null);
    }

    public PinView(Context context, AttributeSet attr) {
        super(context, attr);
        initialise();
    }

    public void setPin(PointF sPin) {
        this.sPin = sPin;
        initialise();
        invalidate();
    }

    public void setPin(float x, float y) {
        this.sPin = new PointF(x, y);
        initialise();
        invalidate();
    }

    private void initialise() {
        float density = getResources().getDisplayMetrics().densityDpi;

//        pin = getBitmapFromVectorDrawable(getContext(), R.drawable.ic_location);
        pin = BitmapFactory.decodeResource(this.getResources(), R.drawable.marker);

        float w = (density / 1000f) * pin.getWidth();
        float h = (density / 1000f) * pin.getHeight();
        pin = Bitmap.createScaledBitmap(pin, (int) w, (int) h, true);

        mPaint = new Paint();
    }

//    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
//        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            drawable = (DrawableCompat.wrap(drawable)).mutate();
//        }
//
//        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
//                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//        drawable.draw(canvas);
//
//        return bitmap;
//    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Don't draw pin before image is ready so it doesn't move around during setup.
        if (!isReady()) {
            return;
        }

        paint.setAntiAlias(true);

        mPaint.setColor(getResources().getColor(R.color.blue));
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAlpha(70);
        mPaint.setAntiAlias(true);


        PointF vPoint;
        float vWidth;
        float vHeight;
        if (venue != null) {
            vPoint = sourceToViewCoord(venue.x, venue.y);

            vHeight = venue.height * getScale();
            vWidth = venue.width * getScale();

            mBoothRect.top = vPoint.y;
            mBoothRect.bottom = vPoint.y + vHeight;
            mBoothRect.left = vPoint.x;
            mBoothRect.right = vPoint.x + vWidth;

            canvas.drawRect(mBoothRect, mPaint);
        }

        if (booth != null) {
            try {
                float boothX = Float.parseFloat(booth.getX());
                float boothY = Float.parseFloat(booth.getY());
                float boothWidth = Float.parseFloat(booth.getWidth());
                float boothHeight = Float.parseFloat(booth.getHeight());

                vPoint = sourceToViewCoord(boothX, boothY);

                vHeight = boothHeight * getScale();
                vWidth = boothWidth * getScale();

                mBoothRect.top = vPoint.y;
                mBoothRect.bottom = vPoint.y + vHeight;
                mBoothRect.left = vPoint.x;
                mBoothRect.right = vPoint.x + vWidth;

                canvas.drawRect(mBoothRect, mPaint);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }

        if (sPin != null && pin != null) {
            PointF vPin = sourceToViewCoord(sPin);
            float vX = vPin.x - (pin.getWidth() / 2);
            float vY = vPin.y - pin.getHeight();
            canvas.drawBitmap(pin, vX, vY, paint);
        }

    }

    public void drawSelectedBooth(Booth booth) {
        mBoothRect = new RectF();
        this.booth = booth;

        invalidate();
    }

    public void drawSelectedVenue(OverallMapFragment.Venue venue) {
        mBoothRect = new RectF();
        this.venue = venue;

        invalidate();
    }

}
