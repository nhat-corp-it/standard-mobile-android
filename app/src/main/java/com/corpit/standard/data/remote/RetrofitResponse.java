package com.corpit.standard.data.remote;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nhatton on 24/3/18.
 */

public class RetrofitResponse<T> {

    @SerializedName("data")
    private ArrayList<T> data;

    @SerializedName("dataversion")
    private String dataVersion;

    @SerializedName("message")
    private String message;

    @SerializedName("Status")
    private String status;

    public ArrayList<T> getData() {
        return data;
    }

    public void setData(ArrayList<T> data) {
        this.data = data;
    }

    public String getDataVersion() {
        return dataVersion;
    }

    public void setDataVersion(String dataVersion) {
        this.dataVersion = dataVersion;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
