package com.corpit.standard.data.remote;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.corpit.standard.di.Injector;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.model.DataVersion;
import com.corpit.standard.model.DataVersionDao;

import org.greenrobot.greendao.async.AsyncSession;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DownloadTaskManager {

    static final int DOWNLOAD_SUCCESS = 25;
    static final int DOWNLOAD_ERROR = 9;

    private static final String MODEL_PACKAGE_NAME = DataVersion.class.getPackage().getName();

    // Sets the amount of time an idle thread will wait for a task before terminating
    private static final int KEEP_ALIVE_TIME = 1;

    // Sets the Time Unit to seconds
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;

    // Sets the initial threadpool size to 8
    private static final int CORE_POOL_SIZE = 8;

    // Sets the maximum threadpool size to 8
    private static final int MAXIMUM_POOL_SIZE = 8;
    private static DownloadTaskManager sInstance;
    private static int progressDividend = 0;
    private static int progress = 0;
    private static WeakReference<DownloadProgressCallBack> notifierRef;

    static {
        // The time unit for "keep alive" is in seconds
        KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

        // Creates a single static instance of DownloadTaskManager
        sInstance = new DownloadTaskManager();
    }

    private final BlockingQueue<Runnable> mDownloadWorkQueue;
    private final BlockingQueue<DownloadTask<?>> mDownloadTaskWorkQueue;
    private DaoSession daoSession;
    private ThreadPoolExecutor mDownloadThreadPool;
    private DownloadHandler mHandler;

    private Map<String, String> dataVersionMap = new HashMap<>();

    private DownloadTaskManager() {

        this.daoSession = Injector.get().daoSession();

        mDownloadWorkQueue = new LinkedBlockingQueue<>();

        mDownloadTaskWorkQueue = new LinkedBlockingQueue<>();

        mDownloadThreadPool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, mDownloadWorkQueue);

        mHandler = new DownloadHandler();
    }

    public static DownloadTaskManager getInstance() {
        return sInstance;
    }

    /**
     * @param dataVersionMap Array of model classes to make requests
     * @param notifier       Callback for tasks
     */
    public void startDownloadTasks(@NonNull Map<String, String> dataVersionMap,
                                   @Nullable DownloadProgressCallBack notifier,
                                   boolean forceUpdate) {

        clear();

        notifierRef = new WeakReference<>(notifier);

        this.dataVersionMap = dataVersionMap;

        ArrayList<DownloadTask<?>> tasks = new ArrayList<>();

        Set<Map.Entry<String, String>> entries = dataVersionMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            try {
                if (entry.getKey().equals("EPoster")) continue;

                String currentVer = daoSession.getDataVersionDao().load(entry.getKey()).getDataVersion();
                if (forceUpdate || !currentVer.equals(entry.getValue())) {
                    String className = daoSession.getDataVersionDao().load(entry.getKey()).getClassName();
                    tasks.add(new DownloadTask<>(Class.forName(MODEL_PACKAGE_NAME + "." + className), entry.getKey()));
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (tasks.size() == 0) {
            if (notifier != null) {
                notifier.onProgressUpdate(100);
                notifier.onProgressComplete();
            }
        } else {
            progress = 100 % tasks.size();
            progressDividend = 100 / tasks.size();
        }

        for (DownloadTask task : tasks) {
            sInstance.mDownloadThreadPool.execute(task.getDownloadRunnable());
        }

        sInstance.mDownloadThreadPool.shutdown();

    }

    @SuppressWarnings("unchecked")
    public <T> void handleState(DownloadTask<T> downloadTask, int state) {
        switch (state) {
            case DOWNLOAD_SUCCESS:
                daoSession.deleteAll(downloadTask.getCls());
                AsyncSession session = daoSession.startAsyncSession();
                session.insertInTx(downloadTask.getCls(), downloadTask.getData());

                updateDataVersion(downloadTask.getCls().getSimpleName());
                break;
            case DOWNLOAD_ERROR:
                break;
        }
        mHandler.obtainMessage(state, downloadTask).sendToTarget();
    }

    private void updateDataVersion(String className) {
        DataVersion dataVersion = daoSession.getDataVersionDao()
                .queryBuilder()
                .where(DataVersionDao.Properties.ClassName.eq(className))
                .unique();
        dataVersion.setDataVersion(dataVersionMap.get(dataVersion.getPage()));
        daoSession.getDataVersionDao().update(dataVersion);
    }

    public void clear() {
        sInstance.mDownloadThreadPool.shutdownNow();
        progress = 0;
        progressDividend = 0;
        if (notifierRef != null) {
            notifierRef.clear();
        }
        sInstance.mDownloadThreadPool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, new LinkedBlockingQueue<Runnable>());
    }

    private void recycle(DownloadTask task) {
        task.recycle();
        mDownloadTaskWorkQueue.offer(task);
    }

    static class DownloadHandler extends Handler {

        DownloadHandler() {

        }

        public void handleMessage(Message msg) {
            DownloadProgressCallBack notifier = notifierRef.get();
//            DownloadTask task = (DownloadTask) msg.obj;
            if (notifier != null) {
                switch (msg.what) {
                    case DOWNLOAD_SUCCESS:
                        progress += progressDividend;
                        notifier.onProgressUpdate(progress);

                        if (progress == 100) {
                            notifier.onProgressComplete();
                        }
                        break;
                    case DOWNLOAD_ERROR:
                        notifier.onError();
                        break;
                    default:
                        break;
                }
            }
//            task.recycle();
        }
    }

    public static class DownloadLifecycleObserver implements LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        void onDestroy() {
            DownloadTaskManager.getInstance().clear();
        }

    }
}
