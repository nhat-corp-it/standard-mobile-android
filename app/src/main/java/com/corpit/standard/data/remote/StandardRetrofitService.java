package com.corpit.standard.data.remote;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.model.Account;
import com.corpit.standard.model.Barista;
import com.corpit.standard.model.Bookmark;
import com.corpit.standard.model.Booth;
import com.corpit.standard.model.BottomBanner;
import com.corpit.standard.model.BusinessMatching;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.Conference;
import com.corpit.standard.model.ConferenceCategory;
import com.corpit.standard.model.ConferenceSpeakers;
import com.corpit.standard.model.ConferenceSponsors;
import com.corpit.standard.model.Configuration;
import com.corpit.standard.model.ConfigurationImages;
import com.corpit.standard.model.DataVersion;
import com.corpit.standard.model.Day;
import com.corpit.standard.model.EPoster;
import com.corpit.standard.model.Event;
import com.corpit.standard.model.ExhibitingCompany;
import com.corpit.standard.model.GeneralInfo;
import com.corpit.standard.model.HomePage;
import com.corpit.standard.model.Inbox;
import com.corpit.standard.model.Location;
import com.corpit.standard.model.Menu;
import com.corpit.standard.model.Moments;
import com.corpit.standard.model.News;
import com.corpit.standard.model.NewsType;
import com.corpit.standard.model.Newsletter;
import com.corpit.standard.model.Product;
import com.corpit.standard.model.ProductCategory;
import com.corpit.standard.model.Reward;
import com.corpit.standard.model.ScrollingBanner;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.model.SpeakerCategory;
import com.corpit.standard.model.SpecialEvent;
import com.corpit.standard.model.Sponsor;
import com.corpit.standard.model.SponsorCategory;
import com.corpit.standard.model.TimeList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by nhatton on 24/3/18.
 */

public interface StandardRetrofitService {

    @GET(BuildConfig.API_END_POINT + "?Page=Account")
    Call<RetrofitResponse<Account>> getAccounts(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Baristas")
    Call<RetrofitResponse<Barista>> getBaristas(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Bookmark")
    Call<RetrofitResponse<Bookmark>> getBookmarks(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=VenueBooth")
    Call<RetrofitResponse<Booth>> getBooths(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=BottomBanner")
    Call<RetrofitResponse<BottomBanner>> getBottomBanners(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=BusinessMatching")
    Call<RetrofitResponse<BusinessMatching>> getBusinessMatchings(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=ColorScheme")
    Call<RetrofitResponse<ColorScheme>> getColorSchemes(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Conference")
    Call<RetrofitResponse<Conference>> getConferences(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=ConferenceCategory")
    Call<RetrofitResponse<ConferenceCategory>> getConferenceCategories(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=ConferenceSpeakers")
    Call<RetrofitResponse<ConferenceSpeakers>> getConferenceSpeakers(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=ConferenceSponsors")
    Call<RetrofitResponse<ConferenceSponsors>> getConferenceSponsors(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Configuration")
    Call<RetrofitResponse<Configuration>> getConfigurations(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=ConfigurationImages")
    Call<RetrofitResponse<ConfigurationImages>> getConfigurationImages(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Day")
    Call<RetrofitResponse<Day>> getDays(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=EPoster")
    Call<RetrofitResponse<EPoster>> getEPosters(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Event")
    Call<RetrofitResponse<Event>> getEvents(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=ExhibitingCompany")
    Call<RetrofitResponse<ExhibitingCompany>> getExhibitingCompanies(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=GeneralInfo")
    Call<RetrofitResponse<GeneralInfo>> getGeneralInfos(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=HomePage")
    Call<RetrofitResponse<HomePage>> getHomePages(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Inbox")
    Call<RetrofitResponse<Inbox>> getInboxes(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=LocationFloorplan")
    Call<RetrofitResponse<Location>> getLocations(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Menu")
    Call<RetrofitResponse<Menu>> getMenus(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Moments")
    Call<RetrofitResponse<Moments>> getMoments(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=News")
    Call<RetrofitResponse<News>> getNews(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Newsletter")
    Call<RetrofitResponse<Newsletter>> getNewsletters(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=NewsType")
    Call<RetrofitResponse<NewsType>> getNewsType(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Product")
    Call<RetrofitResponse<Product>> getProducts(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=ProductCategory")
    Call<RetrofitResponse<ProductCategory>> getProductCategories(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Reward")
    Call<RetrofitResponse<Reward>> getRewards(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=ScrollingBanner")
    Call<RetrofitResponse<ScrollingBanner>> getScrollingBanners(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Speaker")
    Call<RetrofitResponse<Speaker>> getSpeakers(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=SpeakerCategory")
    Call<RetrofitResponse<SpeakerCategory>> getSpeakerCategories(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=SpecialEvent")
    Call<RetrofitResponse<SpecialEvent>> getSpecialEvents(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=Sponsor")
    Call<RetrofitResponse<Sponsor>> getSponsors(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=SponsorCategory")
    Call<RetrofitResponse<SponsorCategory>> getSponsorCategories(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=TimeList")
    Call<RetrofitResponse<TimeList>> getTimeLists(@Query("lang") String lang);

    @GET(BuildConfig.API_END_POINT + "?Page=dataversion")
    Call<RetrofitResponse<DataVersion>> getDataVersions(@Query("Lang") String lang);

    @GET(BuildConfig.API_END_POINT)
    Call<RetrofitGenericResponse> getData(@Query("Page") String page, @Query("lang") String lang);

    @GET
    Call<RetrofitGenericResponse> getData(@Url String url);

}
