package com.corpit.standard.data.remote;

public interface DownloadProgressCallBack {
    void onProgressUpdate(int currentProgress);

    void onProgressComplete();

    void onError();
}
