package com.corpit.standard.data.remote;

import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

/**
 * Model class for Retrofit to cast the response
 */
public class RetrofitGenericResponse {

    @SerializedName("data")
    private JsonArray data;

    @SerializedName("dataversion")
    private String dataVersion;

    @SerializedName("message")
    private String message;

    @SerializedName("Status")
    private String status;

    public JsonArray getData() {
        return data;
    }

    public void setData(JsonArray data) {
        this.data = data;
    }

    public String getDataVersion() {
        return dataVersion;
    }

    public void setDataVersion(String dataVersion) {
        this.dataVersion = dataVersion;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
