package com.corpit.standard.data.remote;

import android.text.TextUtils;

import java.util.ArrayList;

public class DownloadTask<T> implements DownloadRunnable.TaskRunnableDownloadMethods<T> {

    private static final DownloadTaskManager sDownloadManager = DownloadTaskManager.getInstance();
    private Runnable mDownloadRunnable;
    private ArrayList<T> data;
    private Class<T> cls;
    // The Thread on which this task is currently running.
    private Thread mCurrentThread;

    private String page;

    /**
     * @param cls model class
     */
    DownloadTask(Class<T> cls) {
        this(cls, "");
    }

    /**
     * @param cls  model class
     * @param page custom page name if different than class name
     */
    DownloadTask(Class<T> cls, String page) {
        this.cls = cls;
        this.page = page;
        mDownloadRunnable = new DownloadRunnable<>(this, cls);
    }

    @Override
    public void setDownloadThread(Thread currentThread) {
        setCurrentThread(currentThread);
    }

    @Override
    public ArrayList<T> getData() {
        return data;
    }

    @Override
    public void setData(ArrayList<T> data) {
        this.data = data;
    }

    @Override
    public void handleDownloadState(int state) {
        sDownloadManager.handleState(this, state);
    }

    public Class getCls() {
        return cls;
    }

    public String getPage() {
        return TextUtils.isEmpty(page) ? cls.getSimpleName() : page;
    }

    public Runnable getDownloadRunnable() {
        return mDownloadRunnable;
    }

    /*
     * Returns the Thread that this Task is running on. The method must first get a lock on a
     * static field, in this case the ThreadPool singleton. The lock is needed because the
     * Thread object reference is stored in the Thread object itself, and that object can be
     * changed by processes outside of this app.
     */
    public Thread getCurrentThread() {
        synchronized (sDownloadManager) {
            return mCurrentThread;
        }
    }

    /*
     * Sets the identifier for the current Thread. This must be a synchronized operation; see the
     * notes for getCurrentThread()
     */
    public void setCurrentThread(Thread thread) {
        synchronized (sDownloadManager) {
            mCurrentThread = thread;
        }
    }

    void recycle() {
        cls = null;
        data = null;
        mCurrentThread = null;
    }

}
