package com.corpit.standard.data.remote;

/**
 * Interface to indicate a {@link DownloadTask} to download images
 */
public interface WithImage {

    /**
     * @return the url for the image to be downloaded
     */
    String getUrl();
}
