package com.corpit.standard.data.local;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Conference;
import com.corpit.standard.model.ConferenceDao;
import com.corpit.standard.model.EPosterDao;
import com.corpit.standard.model.Event;
import com.corpit.standard.model.EventDao;
import com.corpit.standard.model.ExhibitingCompanyDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.corpit.standard.model.EPosterDao.Properties.Category;

public class DaoHelper {

    private DaoHelper() {
    }

    public static ArrayList<String> getEventDays(String trackType) {
        ArrayList<String> days = new ArrayList<>();

        String query = TextUtils.isEmpty(trackType) ?
                "SELECT DISTINCT " + EventDao.Properties.Date.columnName + " FROM " + EventDao.TABLENAME
                : "SELECT DISTINCT " + EventDao.Properties.Date.columnName +
                " FROM " + EventDao.TABLENAME +
                " WHERE " + EventDao.Properties.Category.columnName + " = ?";

        Cursor cursor = Injector.get().daoSession().getDatabase().rawQuery(query,
                TextUtils.isEmpty(trackType) ? null : new String[]{trackType});
        while (cursor.moveToNext()) {
            days.add(cursor.getString(0));
        }
        cursor.close();

        return days;
    }

    public static ArrayList<String> getEventDays() {
        return getEventDays(null);
    }

    public static ArrayList<String> getEPosterCategoryList() {
        ArrayList<String> categories = new ArrayList<>();
        Cursor cursor = Injector.get().daoSession().getDatabase().rawQuery("SELECT DISTINCT " + Category.columnName +
                " FROM " + EPosterDao.TABLENAME, null);
        while (cursor.moveToNext()) {
            categories.add(cursor.getString(0));
        }
        cursor.close();

        Collections.sort(categories);

        return categories;
    }

    public static ArrayList<String> getConferenceDays(String category) {
        ArrayList<String> days = new ArrayList<>();
        if (TextUtils.isEmpty(category)) {
            Cursor cursor = Injector.get().daoSession().getDatabase().rawQuery(
                    "SELECT DISTINCT " + ConferenceDao.Properties.Date.columnName +
                            " FROM " + ConferenceDao.TABLENAME, null);

            while (cursor.moveToNext()) {
                days.add(cursor.getString(0));
            }
        } else {
            Cursor cursor = Injector.get().daoSession().getDatabase().rawQuery(
                    "SELECT DISTINCT " + ConferenceDao.Properties.Date.columnName +
                            " FROM " + ConferenceDao.TABLENAME +
                            " WHERE " + ConferenceDao.Properties.Category.columnName + " = ?",
                    new String[]{category});

            while (cursor.moveToNext()) {
                days.add(cursor.getString(0));
            }
        }

        return days;
    }

    public static ArrayList<String> getConferenceDays() {
        return getConferenceDays(null);
    }

    public static ArrayList<Conference> getConferences(@NonNull String day, String trackType) {
        List<Conference> list;
        if (TextUtils.isEmpty(trackType)) {
            list = Injector.get().daoSession().getConferenceDao()
                    .queryBuilder()
                    .where(ConferenceDao.Properties.Date.eq(day))
                    .list();
        } else {
            list = Injector.get().daoSession().getConferenceDao()
                    .queryBuilder()
                    .where(ConferenceDao.Properties.Category.eq(trackType))
                    .where(ConferenceDao.Properties.Date.eq(day))
                    .list();
        }

        Collections.sort(list);

        return (ArrayList<Conference>) list;
    }

    public static ArrayList<Conference> getConferences(@NonNull String day) {
        return getConferences(day, null);
    }

    public static ArrayList<Event> getEvents(@NonNull String day, String category) {
        List<Event> list;
        if (TextUtils.isEmpty(category)) {
            list = Injector.get().daoSession().getEventDao()
                    .queryBuilder()
                    .where(EventDao.Properties.Date.eq(day))
                    .list();
        } else {
            list = Injector.get().daoSession().getEventDao()
                    .queryBuilder()
                    .where(EventDao.Properties.Date.eq(day))
                    .where(EventDao.Properties.Category.eq(category))
                    .list();
        }

        Collections.sort(list);

        return (ArrayList<Event>) list;
    }

    public static ArrayList<Event> getEvents(@NonNull String day) {
        return getEvents(day, null);
    }

    public static ArrayList<String> getEventTracks() {
        ArrayList<String> tracks = new ArrayList<>();
        Cursor cursor = Injector.get().daoSession().getDatabase().rawQuery(
                "SELECT DISTINCT " + EventDao.Properties.Category.columnName +
                        " FROM " + EventDao.TABLENAME, null);
        while (cursor.moveToNext()) {
            tracks.add(cursor.getString(0));
        }
        cursor.close();

        Collections.sort(tracks);

        return tracks;
    }

    public static ArrayList<String> getCountries() {
        ArrayList<String> countries = new ArrayList<>();
        Cursor cursor = Injector.get().daoSession().getDatabase().rawQuery(
                "SELECT DISTINCT " + ExhibitingCompanyDao.Properties.Country.columnName +
                        " FROM " + ExhibitingCompanyDao.TABLENAME +
                        " WHERE " + ExhibitingCompanyDao.Properties.Country.columnName + " != ''"
                , null);
        while (cursor.moveToNext()) {
            countries.add(cursor.getString(0));
        }
        cursor.close();

        Collections.sort(countries);

        return countries;
    }

}
