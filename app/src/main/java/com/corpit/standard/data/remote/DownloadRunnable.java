package com.corpit.standard.data.remote;

import android.accounts.NetworkErrorException;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.corpit.standard.BuildConfig;
import com.corpit.standard.di.Injector;
import com.corpit.standard.share.UserManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.corpit.standard.data.remote.DownloadTaskManager.DOWNLOAD_ERROR;
import static com.corpit.standard.data.remote.DownloadTaskManager.DOWNLOAD_SUCCESS;

/**
 * Task includes download json from server and then save to local database.
 * If API includes image, the model class need to implement {@link WithImage}.
 * If the API is different from the class name, need to add the API name to the hash map in class
 * {@link DownloadRunnable}.
 *
 * @param <T> model class required for this task
 * @author nhatton
 */
public class DownloadRunnable<T> implements Runnable {

    private static final String TAG = "DownloadRunnable";

    private final DownloadTask<T> downloadTask;
    private Class<T> cls;
    private Handler handler;
    private Gson gson;

    DownloadRunnable(DownloadTask<T> downloadTask, Class<T> cls) {
        this.downloadTask = downloadTask;
        this.cls = cls;
        this.handler = new Handler(Looper.getMainLooper());
        gson = new Gson();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void run() {
        try {
            downloadTask.setDownloadThread(Thread.currentThread());
            // Moves the current Thread into the background
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            Call<RetrofitGenericResponse> call = Injector.get().apiService()
                    .getData(downloadTask.getPage(), UserManager.getInstance().getLanguage().locale);
            if (BuildConfig.DEBUG) Log.e(TAG, "page: " + downloadTask.getPage());

            Response<RetrofitGenericResponse> response = call.execute();

            int count = 0;
            while (response.code() != 200 && count < 4) {
                response = call.clone().execute();
                count++;
            }

            if (response.code() != 200) {
                response.raw().body().close();
                throw new NetworkErrorException();
            }

            JsonArray data = response.body().getData();

            ArrayList<T> list = new ArrayList<>();
            List<String> urls = new ArrayList<>();

            boolean hasImage = WithImage.class.isAssignableFrom(downloadTask.getCls());

            for (JsonElement json : data) {
                T element = gson.fromJson(json, cls);

                if (hasImage) {
                    WithImage withImage = (WithImage) element;
                    urls.add(withImage.getUrl());
                }

                list.add(element);
            }

            handler.post(new ImageTask(urls));

            downloadTask.setData(list);

            downloadTask.handleDownloadState(DOWNLOAD_SUCCESS);
            if (BuildConfig.DEBUG) Log.d(TAG, "Download completed: " + cls.getSimpleName());


        } catch (Exception e) {
            e.printStackTrace();
            if (downloadTask.getData() == null) {
                downloadTask.handleDownloadState(DOWNLOAD_ERROR);
                if (BuildConfig.DEBUG) Log.e(TAG, "Download failed: " + cls.getSimpleName());
            }
        } finally {
            // Sets the reference to the current Thread to null, releasing its storage
            downloadTask.setDownloadThread(null);

            // Clears the Thread's interrupt flag
            Thread.interrupted();
        }
    }

    class ImageTask implements Runnable {

        private final List<String> urls;

        ImageTask(List<String> urls) {
            this.urls = urls;
        }

        @Override
        public void run() {
            for (String url : urls) {
                Glide.with(Injector.get().appContext())
                        .download(url)
                        .listener(new RequestListener<File>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                        Target<File> target, boolean isFirstResource) {
                                if (BuildConfig.DEBUG)
                                    Log.e(TAG, "Download image failed: " + model);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(File resource, Object model, Target<File> target,
                                                           DataSource dataSource, boolean isFirstResource) {
                                if (BuildConfig.DEBUG)
                                    Log.i(TAG, "Download image completed: " + model);
                                return true;
                            }
                        })
                        .preload();
            }
        }
    }

    interface TaskRunnableDownloadMethods<T> {

        /**
         * Sets the Thread that this instance is running on
         *
         * @param currentThread the current Thread
         */
        void setDownloadThread(Thread currentThread);

        ArrayList<T> getData();

        void setData(ArrayList<T> data);

        /**
         * Defines the actions for each state of the PhotoTask instance.
         *
         * @param state The current state of the task
         */
        void handleDownloadState(int state);
    }
}
