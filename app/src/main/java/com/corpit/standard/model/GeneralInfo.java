package com.corpit.standard.model;

import com.corpit.standard.ui.base.Expandable;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.GENERAL_INFO_CONTENT;
import static com.corpit.standard.share.ParameterNames.GENERAL_INFO_ICON;
import static com.corpit.standard.share.ParameterNames.GENERAL_INFO_ID;
import static com.corpit.standard.share.ParameterNames.GENERAL_INFO_MAIN_ID;
import static com.corpit.standard.share.ParameterNames.GENERAL_INFO_MENU_ID;
import static com.corpit.standard.share.ParameterNames.GENERAL_INFO_SEQ;
import static com.corpit.standard.share.ParameterNames.GENERAL_INFO_TITLE;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class GeneralInfo implements Expandable {

    @Id
    @SerializedName(GENERAL_INFO_ID)
    private String id;

    @SerializedName(GENERAL_INFO_TITLE)
    private String title;

    @SerializedName(GENERAL_INFO_CONTENT)
    private String content;

    @SerializedName(GENERAL_INFO_ICON)
    private String icon;

    @SerializedName(GENERAL_INFO_MAIN_ID)
    private String mainId;

    @SerializedName(GENERAL_INFO_SEQ)
    private String sortOrder;

    @SerializedName(GENERAL_INFO_MENU_ID)
    private String menuId;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 993431348)
    public GeneralInfo(String id, String title, String content, String icon,
                       String mainId, String sortOrder, String menuId, String deleteFlag,
                       String updatedDate) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.icon = icon;
        this.mainId = mainId;
        this.sortOrder = sortOrder;
        this.menuId = menuId;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1107499534)
    public GeneralInfo() {
    }

    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return title;
    }

    @Override
    public String getIconUrl() {
        return icon;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMainId() {
        return mainId;
    }

    public void setMainId(String mainId) {
        this.mainId = mainId;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
