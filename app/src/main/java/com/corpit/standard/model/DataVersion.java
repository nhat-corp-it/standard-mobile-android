package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class DataVersion {

    @Id
    @SerializedName("Page")
    private String page;

    @SerializedName("dataVersion")
    private String dataVersion;

    @SerializedName("class")
    private String className;

    @Generated(hash = 1412089170)
    public DataVersion(String page, String dataVersion, String className) {
        this.page = page;
        this.dataVersion = dataVersion;
        this.className = className;
    }

    @Generated(hash = 2042361301)
    public DataVersion() {
    }

    public String getPage() {
        return this.page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getDataVersion() {
        return this.dataVersion;
    }

    public void setDataVersion(String dataVersion) {
        this.dataVersion = dataVersion;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
