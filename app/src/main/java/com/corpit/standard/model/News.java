package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.converter.PropertyConverter;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.NEWS_CONTENT;
import static com.corpit.standard.share.ParameterNames.NEWS_ID;
import static com.corpit.standard.share.ParameterNames.NEWS_MENU_ID;
import static com.corpit.standard.share.ParameterNames.NEWS_PICTURE;
import static com.corpit.standard.share.ParameterNames.NEWS_TITLE;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class News implements ListItem<News> {

    @Id
    @SerializedName(NEWS_ID)
    private String id;

    @SerializedName(NEWS_TITLE)
    private String title;

    @SerializedName(NEWS_CONTENT)
    private String content;

    @SerializedName(NEWS_PICTURE)
    private String picture;

    @SerializedName(NEWS_MENU_ID)
    private String newsTypeId;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    @Convert(converter = TimeStampConverter.class, columnType = Long.class)
    private String updatedDate;

    @Generated(hash = 821302380)
    public News(String id, String title, String content, String picture,
                String newsTypeId, String deleteFlag, String updatedDate) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.picture = picture;
        this.newsTypeId = newsTypeId;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1579685679)
    public News() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getNewsTypeId() {
        return newsTypeId;
    }

    public void setNewsTypeId(String newsTypeId) {
        this.newsTypeId = newsTypeId;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public int compareTo(@NonNull News o) {
        float update1 = Float.parseFloat(updatedDate);
        float update2 = Float.parseFloat(o.updatedDate);

        return (int) (update1 - update2);
    }

    public static class TimeStampConverter implements PropertyConverter<String, Long> {
        @Override
        public String convertToEntityProperty(Long databaseValue) {
            return databaseValue.toString();
        }

        @Override
        public Long convertToDatabaseValue(String entityProperty) {
            return (long) Float.parseFloat(entityProperty);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (id != null ? !id.equals(news.id) : news.id != null) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;
        if (content != null ? !content.equals(news.content) : news.content != null) return false;
        if (picture != null ? !picture.equals(news.picture) : news.picture != null) return false;
        if (newsTypeId != null ? !newsTypeId.equals(news.newsTypeId) : news.newsTypeId != null)
            return false;
        if (deleteFlag != null ? !deleteFlag.equals(news.deleteFlag) : news.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(news.updatedDate) : news.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (picture != null ? picture.hashCode() : 0);
        result = 31 * result + (newsTypeId != null ? newsTypeId.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
