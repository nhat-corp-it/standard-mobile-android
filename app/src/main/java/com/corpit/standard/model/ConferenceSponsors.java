package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.CONF_SPONSOR_CONF_ID;
import static com.corpit.standard.share.ParameterNames.CONF_SPONSOR_ID;
import static com.corpit.standard.share.ParameterNames.CONF_SPONSOR_SPONSOR_CAT;
import static com.corpit.standard.share.ParameterNames.CONF_SPONSOR_SPONSOR_ID;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class ConferenceSponsors {

    @Id
    @SerializedName(CONF_SPONSOR_ID)
    private String id;

    @SerializedName(CONF_SPONSOR_SPONSOR_ID)
    private String sponsorId;

    @SerializedName(CONF_SPONSOR_SPONSOR_CAT)
    private String sponsorCat;

    @SerializedName(CONF_SPONSOR_CONF_ID)
    private String conferenceId;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 99794161)
    public ConferenceSponsors(String id, String sponsorId, String sponsorCat,
            String conferenceId, String deleteFlag, String updatedDate) {
        this.id = id;
        this.sponsorId = sponsorId;
        this.sponsorCat = sponsorCat;
        this.conferenceId = conferenceId;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 251193186)
    public ConferenceSponsors() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getSponsorCat() {
        return sponsorCat;
    }

    public void setSponsorCat(String sponsorCat) {
        this.sponsorCat = sponsorCat;
    }

    public String getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(String conferenceId) {
        this.conferenceId = conferenceId;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
