package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.corpit.standard.data.remote.WithImage;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.LOCATION_ID;
import static com.corpit.standard.share.ParameterNames.LOCATION_MAP;
import static com.corpit.standard.share.ParameterNames.LOCATION_MENU_ID;
import static com.corpit.standard.share.ParameterNames.LOCATION_NAME;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Location implements WithImage, Comparable<Location> {

    @Id
    @SerializedName(LOCATION_ID)
    private String id;

    @SerializedName(LOCATION_NAME)
    private String name;

    @SerializedName(LOCATION_MAP)
    private String map;

    @SerializedName(LOCATION_MENU_ID)
    private String menuId;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1736461070)
    public Location(String id, String name, String map, String menuId,
                    String deleteFlag, String updatedDate) {
        this.id = id;
        this.name = name;
        this.map = map;
        this.menuId = menuId;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 375979639)
    public Location() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String getUrl() {
        return map.replaceAll(" ", "%20");
    }

    @Override
    public int compareTo(@NonNull Location o) {
//        if(!menuId.equals(o.menuId)){
//            return menuId.compareTo(o.menuId);
//        } else {
//            return id.compareTo(o.id);
//        }
        return name.compareTo(o.name);
    }
}
