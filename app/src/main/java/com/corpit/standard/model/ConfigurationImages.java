package com.corpit.standard.model;

import android.support.annotation.StringDef;

import com.corpit.standard.data.remote.WithImage;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.corpit.standard.share.ParameterNames.CONFIG_IMG_FILE;
import static com.corpit.standard.share.ParameterNames.CONFIG_IMG_ID;
import static com.corpit.standard.share.ParameterNames.CONFIG_IMG_KEY;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class ConfigurationImages implements WithImage {

    @StringDef({ImageKey.SIDE_MENU_ICON, ImageKey.TOP_BANNER, ImageKey.SPLASH, ImageKey.LOADING})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ImageKey {
        String SIDE_MENU_ICON = "SideMenuIconImage";
        String TOP_BANNER = "TopBannerImage";
        String SPLASH = "SplashImage";
        String LOADING = "LoadingImage";
    }

    @Id
    @SerializedName(CONFIG_IMG_ID)
    private String id;

    @ImageKey
    @SerializedName(CONFIG_IMG_KEY)
    private String key;

    @SerializedName(CONFIG_IMG_FILE)
    private String file;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 767382434)
    public ConfigurationImages(String id, String key, String file,
                               String deleteFlag, String updatedDate) {
        this.id = id;
        this.key = key;
        this.file = file;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 649772540)
    public ConfigurationImages() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @ImageKey
    public String getKey() {
        return key;
    }

    public void setKey(@ImageKey String key) {
        this.key = key;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String getUrl() {
        return file;
    }

}
