package com.corpit.standard.model;

import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;

import com.corpit.standard.ui.base.WithDetails;
import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_ADDRESS;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_BOOTH;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_BROCHURE;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_CONTACT_NO;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_CONTACT_PERSON_ID;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_COUNTRY;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_EMAIL;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_HALL;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_ID;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_LOGO;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_MARKET_SEGMENT;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_NAME;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_PRODUCT;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_PROFILE;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_TRENDS;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_TYPE;
import static com.corpit.standard.share.ParameterNames.EXHIBITING_COMPANY_WEBSITE;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class ExhibitingCompany implements ListItem<ExhibitingCompany>, WithDetails {

    @Id
    @SerializedName(EXHIBITING_COMPANY_ID)
    private String id;

    @SerializedName(EXHIBITING_COMPANY_NAME)
    private String name;

    @SerializedName(EXHIBITING_COMPANY_ADDRESS)
    private String address;

    @SerializedName(EXHIBITING_COMPANY_COUNTRY)
    private String country;

    @SerializedName(EXHIBITING_COMPANY_EMAIL)
    private String email;

    @SerializedName(EXHIBITING_COMPANY_CONTACT_NO)
    private String contactNo;

    @SerializedName(EXHIBITING_COMPANY_WEBSITE)
    private String website;

    @SerializedName(EXHIBITING_COMPANY_BROCHURE)
    private String brochure;

    @SerializedName(EXHIBITING_COMPANY_LOGO)
    private String logo;

    @SerializedName(EXHIBITING_COMPANY_PROFILE)
    private String companyProfile;

    @SerializedName(EXHIBITING_COMPANY_TYPE)
    private String companyType;

    @SerializedName(EXHIBITING_COMPANY_HALL)
    private String hall;

    @SerializedName(EXHIBITING_COMPANY_BOOTH)
    private String boothId;

    @SerializedName(EXHIBITING_COMPANY_TRENDS)
    private String trends;

    @SerializedName(EXHIBITING_COMPANY_MARKET_SEGMENT)
    private String marketSegment;

    @SerializedName(EXHIBITING_COMPANY_CONTACT_PERSON_ID)
    private String contactPersonId;

    @SerializedName(EXHIBITING_COMPANY_PRODUCT)
    private String product;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 895670739)
    public ExhibitingCompany(String id, String name, String address, String country, String email, String contactNo,
                             String website, String brochure, String logo, String companyProfile, String companyType, String hall,
                             String boothId, String trends, String marketSegment, String contactPersonId, String product, String deleteFlag,
                             String updatedDate) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.country = country;
        this.email = email;
        this.contactNo = contactNo;
        this.website = website;
        this.brochure = brochure;
        this.logo = logo;
        this.companyProfile = companyProfile;
        this.companyType = companyType;
        this.hall = hall;
        this.boothId = boothId;
        this.trends = trends;
        this.marketSegment = marketSegment;
        this.contactPersonId = contactPersonId;
        this.product = product;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1949850404)
    public ExhibitingCompany() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBrochure() {
        return brochure;
    }

    public void setBrochure(String brochure) {
        this.brochure = brochure;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompanyProfile() {
        return companyProfile;
    }

    public void setCompanyProfile(String companyProfile) {
        this.companyProfile = companyProfile;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

    public String getBoothId() {
        return boothId;
    }

    public void setBoothId(String boothId) {
        this.boothId = boothId;
    }

    public String getTrends() {
        return this.trends;
    }

    public void setTrends(String trends) {
        this.trends = trends;
    }

    public String getMarketSegment() {
        return this.marketSegment;
    }

    public void setMarketSegment(String marketSegment) {
        this.marketSegment = marketSegment;
    }

    public String getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(String contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public int compareTo(@NonNull ExhibitingCompany company) {
        return name.toUpperCase().compareTo(company.name.toUpperCase());
    }

    @Override
    public String getSectionName() {
        String s = name.substring(0, 1).toUpperCase();

        return s.matches("[A-Za-z]")
                ? s
                : "#";
    }

    public Spanned getCompanyProfileToDisplay() {
        StringBuilder html = new StringBuilder();
        html.append("<html><head><meta http-equiv=\"Content-Type\" companyProfile=\"text/html; charset=utf-8\" />");
        html.append("</head><body>");
        if (companyProfile != null && !companyProfile.isEmpty()) {
            companyProfile = companyProfile.replaceAll(" width: [0-9]*px;", "");
            companyProfile = companyProfile.replaceAll("font-family:[^;]*;", "");
            companyProfile = companyProfile.replaceAll("14px", "18px");
            companyProfile = companyProfile.replaceAll("13px", "18px");
            companyProfile = companyProfile.replaceAll("12px", "18px");
            companyProfile = companyProfile.replaceAll("background-color:[^;]*;", "");
        }
        html.append(companyProfile);
        html.append("</body>");
        html.append("</html>");

        return Html.fromHtml(html.toString());
    }



    public String getProductToDisplay() {
        if (product.isEmpty()) return "";

        String[] products = product.split(";");

        for(int i = 0; i < products.length; i ++) {
            products[i] = products[i].trim();
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < products.length - 2; i++) {
            builder.append(products[i]).append("\n");
        }
        builder.append(products[products.length - 1]);
        return builder.toString();
    }

    @Override
    public String getText() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExhibitingCompany that = (ExhibitingCompany) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (contactNo != null ? !contactNo.equals(that.contactNo) : that.contactNo != null)
            return false;
        if (website != null ? !website.equals(that.website) : that.website != null) return false;
        if (brochure != null ? !brochure.equals(that.brochure) : that.brochure != null)
            return false;
        if (logo != null ? !logo.equals(that.logo) : that.logo != null) return false;
        if (companyProfile != null ? !companyProfile.equals(that.companyProfile) : that.companyProfile != null)
            return false;
        if (companyType != null ? !companyType.equals(that.companyType) : that.companyType != null)
            return false;
        if (hall != null ? !hall.equals(that.hall) : that.hall != null) return false;
        if (boothId != null ? !boothId.equals(that.boothId) : that.boothId != null) return false;
        if (trends != null ? !trends.equals(that.trends) : that.trends != null) return false;
        if (marketSegment != null ? !marketSegment.equals(that.marketSegment) : that.marketSegment != null)
            return false;
        if (contactPersonId != null ? !contactPersonId.equals(that.contactPersonId) : that.contactPersonId != null)
            return false;
        if (product != null ? !product.equals(that.product) : that.product != null) return false;
        if (deleteFlag != null ? !deleteFlag.equals(that.deleteFlag) : that.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(that.updatedDate) : that.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (contactNo != null ? contactNo.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (brochure != null ? brochure.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + (companyProfile != null ? companyProfile.hashCode() : 0);
        result = 31 * result + (companyType != null ? companyType.hashCode() : 0);
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        result = 31 * result + (boothId != null ? boothId.hashCode() : 0);
        result = 31 * result + (trends != null ? trends.hashCode() : 0);
        result = 31 * result + (marketSegment != null ? marketSegment.hashCode() : 0);
        result = 31 * result + (contactPersonId != null ? contactPersonId.hashCode() : 0);
        result = 31 * result + (product != null ? product.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
