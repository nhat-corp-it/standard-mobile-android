package com.corpit.standard.model;

import com.corpit.standard.data.remote.WithImage;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.BOTTOM_BANNER_ID;
import static com.corpit.standard.share.ParameterNames.BOTTOM_BANNER_IMAGE;
import static com.corpit.standard.share.ParameterNames.BOTTOM_BANNER_TITLE;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class BottomBanner implements WithImage {

    @Id
    @SerializedName(BOTTOM_BANNER_ID)
    private String id;

    @SerializedName(BOTTOM_BANNER_TITLE)
    private String title;

    @SerializedName(BOTTOM_BANNER_IMAGE)
    private String image;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1009499106)
    public BottomBanner(String id, String title, String image, String deleteFlag,
                        String updatedDate) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 946315989)
    public BottomBanner() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String getUrl() {
        return image.replaceAll(" ", "%20");
    }
}
