package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

@Entity
public class InnovationShow implements Comparable<InnovationShow> {

    @Id
    @SerializedName("Ino_ID")
    private String id;

    @SerializedName("Ino_rating")
    private String rating;

    @SerializedName("Ino_exhID")
    private String exhibitingCompanyId;

    @SerializedName("Ino_QCategories")
    private String productCategories;

    @SerializedName("Ino_productInfo")
    private String info;

    @SerializedName("Ino_region")
    private String regions;

    @SerializedName("Ino_productPic")
    private String productPic;

    @SerializedName("Ino_productName")
    private String name;

    @SerializedName("Ino_certification")
    private String certification;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1074085396)
    public InnovationShow(String id, String rating, String exhibitingCompanyId,
                          String productCategories, String info, String regions,
                          String productPic, String name, String certification, String deleteFlag,
                          String updatedDate) {
        this.id = id;
        this.rating = rating;
        this.exhibitingCompanyId = exhibitingCompanyId;
        this.productCategories = productCategories;
        this.info = info;
        this.regions = regions;
        this.productPic = productPic;
        this.name = name;
        this.certification = certification;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1701974207)
    public InnovationShow() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRating() {
        return this.rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getExhibitingCompanyId() {
        return this.exhibitingCompanyId;
    }

    public void setExhibitingCompanyId(String exhibitingCompanyId) {
        this.exhibitingCompanyId = exhibitingCompanyId;
    }

    public String getProductCategories() {
        return this.productCategories;
    }

    public void setProductCategories(String productCategories) {
        this.productCategories = productCategories;
    }

    public String getInfo() {
        return this.info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getRegions() {
        return this.regions;
    }

    public void setRegions(String regions) {
        this.regions = regions;
    }

    public String getProductPic() {
        return this.productPic;
    }

    public void setProductPic(String productPic) {
        this.productPic = productPic;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCertification() {
        return this.certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }

    public String getDeleteFlag() {
        return this.deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return this.updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public int compareTo(@NonNull InnovationShow o) {
        return name.compareTo(o.name);
    }
}
