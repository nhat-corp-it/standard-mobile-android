package com.corpit.standard.model;

import android.support.annotation.NonNull;
import android.support.annotation.StringDef;

import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.corpit.standard.share.ParameterNames.BOOKMARK_EXHIBITOR_ID;
import static com.corpit.standard.share.ParameterNames.BOOKMARK_ID;
import static com.corpit.standard.share.ParameterNames.BOOKMARK_USER_ID;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import static java.lang.System.currentTimeMillis;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Bookmark implements ListItem<Bookmark> {

    @StringDef({Type.EXHIBITOR, Type.SPEAKER, Type.CONFERENCE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
        String EXHIBITOR = "Exhibitor";
        String SPEAKER = "Speaker";
        String CONFERENCE = "Conference";
    }

    @Id(autoincrement = true)
    @SerializedName(BOOKMARK_ID)
    private Long longId;

    @SerializedName(BOOKMARK_USER_ID)
    private String userId;

    @Index(unique = true)
    @SerializedName(BOOKMARK_EXHIBITOR_ID)
    private String itemId;

    @Type
    private String bookmarkType;

    private String title;

    private String details;

    private long calendarEventId;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1551643848)
    public Bookmark(Long longId, String userId, String itemId, String bookmarkType, String title,
                    String details, long calendarEventId, String deleteFlag, String updatedDate) {
        this.longId = longId;
        this.userId = userId;
        this.itemId = itemId;
        this.bookmarkType = bookmarkType;
        this.title = title;
        this.details = details;
        this.calendarEventId = calendarEventId;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1206029275)
    public Bookmark() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public static Bookmark create(@Type String type, String id, DaoSession daoSession) {
        switch (type) {
            case Type.EXHIBITOR:
                return createExhibitorBookmark(id, daoSession);
            case Type.SPEAKER:
                return createSpeakerBookmark(id, daoSession);
            case Type.CONFERENCE:
                return createConferenceBookmark(id, daoSession);
            default:
                return new Bookmark();
        }
    }

    private static Bookmark createExhibitorBookmark(String exhibitorId, DaoSession daoSession) {
        ExhibitingCompany exhibitor = daoSession.getExhibitingCompanyDao().load(exhibitorId);
        Booth booth = daoSession.getBoothDao().load(exhibitor.getBoothId());
        String boothName = booth == null ? "" : booth.getName();

        Bookmark bookmark = new Bookmark();
        bookmark.setDeleteFlag("false");
        bookmark.setUpdatedDate(String.valueOf(currentTimeMillis()));
        bookmark.setUserId("U0001");
        bookmark.setItemId(exhibitorId);
        bookmark.setBookmarkType(Type.EXHIBITOR);
        bookmark.setTitle(exhibitor.getName());
        String detail = exhibitor.getHall() + "\n" + boothName;
        bookmark.setDetails(detail);

        return bookmark;
    }

    private static Bookmark createSpeakerBookmark(String speakerId, DaoSession daoSession) {
        Speaker speaker = daoSession.getSpeakerDao().load(speakerId);

        Bookmark bookmark = new Bookmark();
        bookmark.setDeleteFlag("false");
        bookmark.setUpdatedDate(String.valueOf(currentTimeMillis()));
        bookmark.setUserId("U0001");
        bookmark.setItemId(speakerId);
        bookmark.setBookmarkType(Type.SPEAKER);
        bookmark.setTitle(speaker.getFullName());
        bookmark.setDetails("");

        return bookmark;
    }

    private static Bookmark createConferenceBookmark(String conferenceId, DaoSession daoSession) {
        Conference conference = daoSession.getConferenceDao().load(conferenceId);

        Bookmark bookmark = new Bookmark();
        bookmark.setDeleteFlag("false");
        bookmark.setUpdatedDate(String.valueOf(currentTimeMillis()));
        bookmark.setUserId("U0001");
        bookmark.setItemId(conferenceId);
        bookmark.setBookmarkType(Type.CONFERENCE);
        bookmark.setTitle(conference.getTitle());
        String detail = String.format("%s\n%s - %s", conference.getDate(),
                conference.getFormattedStartTime(), conference.getFormattedEndTime());
        bookmark.setDetails(detail);

        return bookmark;
    }

    @Type
    public String getBookmarkType() {
        return this.bookmarkType;
    }

    public void setBookmarkType(@Type String bookmarkType) {
        this.bookmarkType = bookmarkType;
    }

    public String getId() {
        return longId.toString();
    }

    public Long getLongId() {
        return this.longId;
    }

    public void setLongId(Long id) {
        this.longId = id;
    }

    public String getTitle() {
        return title;
    }

    public long getCalendarEventId() {
        return this.calendarEventId;
    }

    public void setCalendarEventId(long calendarEventId) {
        this.calendarEventId = calendarEventId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public int compareTo(@NonNull Bookmark o) {
        if (!bookmarkType.equals(o.bookmarkType)) {
            return bookmarkType.compareTo(o.bookmarkType);
        } else {
            return longId.compareTo(o.longId);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bookmark bookmark = (Bookmark) o;

        if (calendarEventId != bookmark.calendarEventId) return false;
        if (longId != null ? !longId.equals(bookmark.longId) : bookmark.longId != null)
            return false;
        if (userId != null ? !userId.equals(bookmark.userId) : bookmark.userId != null)
            return false;
        if (itemId != null ? !itemId.equals(bookmark.itemId) : bookmark.itemId != null)
            return false;
        if (bookmarkType != null ? !bookmarkType.equals(bookmark.bookmarkType) : bookmark.bookmarkType != null)
            return false;
        if (title != null ? !title.equals(bookmark.title) : bookmark.title != null) return false;
        if (details != null ? !details.equals(bookmark.details) : bookmark.details != null)
            return false;
        if (deleteFlag != null ? !deleteFlag.equals(bookmark.deleteFlag) : bookmark.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(bookmark.updatedDate) : bookmark.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = longId != null ? longId.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (itemId != null ? itemId.hashCode() : 0);
        result = 31 * result + (bookmarkType != null ? bookmarkType.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (details != null ? details.hashCode() : 0);
        result = 31 * result + (int) (calendarEventId ^ (calendarEventId >>> 32));
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
