package com.corpit.standard.model;

import android.graphics.Color;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;

import static com.corpit.standard.share.ParameterNames.COLOR_BACKGROUND_1;
import static com.corpit.standard.share.ParameterNames.COLOR_BACKGROUND_2;
import static com.corpit.standard.share.ParameterNames.COLOR_BUTTON_1;
import static com.corpit.standard.share.ParameterNames.COLOR_BUTTON_2;
import static com.corpit.standard.share.ParameterNames.COLOR_COLUMN_1;
import static com.corpit.standard.share.ParameterNames.COLOR_COLUMN_2;
import static com.corpit.standard.share.ParameterNames.COLOR_FONT_1;
import static com.corpit.standard.share.ParameterNames.COLOR_FONT_2;
import static com.corpit.standard.share.ParameterNames.COLOR_FONT_3;
import static com.corpit.standard.share.ParameterNames.COLOR_MARGIN_1;
import static com.corpit.standard.share.ParameterNames.COLOR_MARGIN_2;
import static com.corpit.standard.share.ParameterNames.COLOR_TIMESTAMP;

@Entity
public class ColorScheme {

    @Id
    @SerializedName(COLOR_TIMESTAMP)
    private String timestamp;

    @SerializedName(COLOR_MARGIN_1)
    private String colorMargin1;

    @SerializedName(COLOR_MARGIN_2)
    private String colorMargin2;

    @SerializedName(COLOR_BUTTON_1)
    private String colorButton1;

    @SerializedName(COLOR_BUTTON_2)
    private String colorButton2;

    @SerializedName(COLOR_COLUMN_1)
    private String colorColumn1;

    @SerializedName(COLOR_COLUMN_2)
    private String colorColumn2;

    @SerializedName(COLOR_BACKGROUND_1)
    private String colorBackground1;

    @SerializedName(COLOR_BACKGROUND_2)
    private String colorBackground2;

    @SerializedName(COLOR_FONT_1)
    private String colorFont1;

    @SerializedName(COLOR_FONT_2)
    private String colorFont2;

    @SerializedName(COLOR_FONT_3)
    private String colorFont3;

    @Generated(hash = 358854793)
    public ColorScheme(String timestamp, String colorMargin1, String colorMargin2,
                       String colorButton1, String colorButton2, String colorColumn1,
                       String colorColumn2, String colorBackground1, String colorBackground2,
                       String colorFont1, String colorFont2, String colorFont3) {
        this.timestamp = timestamp;
        this.colorMargin1 = colorMargin1;
        this.colorMargin2 = colorMargin2;
        this.colorButton1 = colorButton1;
        this.colorButton2 = colorButton2;
        this.colorColumn1 = colorColumn1;
        this.colorColumn2 = colorColumn2;
        this.colorBackground1 = colorBackground1;
        this.colorBackground2 = colorBackground2;
        this.colorFont1 = colorFont1;
        this.colorFont2 = colorFont2;
        this.colorFont3 = colorFont3;
    }

    @Generated(hash = 719162740)
    public ColorScheme() {
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getColorMargin1() {
        return this.colorMargin1;
    }

    public void setColorMargin1(String colorMargin1) {
        this.colorMargin1 = colorMargin1;
    }

    public String getColorMargin2() {
        return this.colorMargin2;
    }

    public void setColorMargin2(String colorMargin2) {
        this.colorMargin2 = colorMargin2;
    }

    public String getColorButton1() {
        return this.colorButton1;
    }

    public void setColorButton1(String colorButton1) {
        this.colorButton1 = colorButton1;
    }

    public String getColorButton2() {
        return this.colorButton2;
    }

    public void setColorButton2(String colorButton2) {
        this.colorButton2 = colorButton2;
    }

    public String getColorColumn1() {
        return this.colorColumn1;
    }

    public void setColorColumn1(String colorColumn1) {
        this.colorColumn1 = colorColumn1;
    }

    public String getColorColumn2() {
        return this.colorColumn2;
    }

    public void setColorColumn2(String colorColumn2) {
        this.colorColumn2 = colorColumn2;
    }

    public String getColorBackground1() {
        return this.colorBackground1;
    }

    public void setColorBackground1(String colorBackground1) {
        this.colorBackground1 = colorBackground1;
    }

    public String getColorBackground2() {
        return this.colorBackground2;
    }

    public void setColorBackground2(String colorBackground2) {
        this.colorBackground2 = colorBackground2;
    }

    public String getColorFont1() {
        return this.colorFont1;
    }

    public void setColorFont1(String colorFont1) {
        this.colorFont1 = colorFont1;
    }

    public String getColorFont2() {
        return this.colorFont2;
    }

    public void setColorFont2(String colorFont2) {
        this.colorFont2 = colorFont2;
    }

    public String getColorFont3() {
        return this.colorFont3;
    }

    public void setColorFont3(String colorFont3) {
        this.colorFont3 = colorFont3;
    }


    @Transient
    private int colorMargin1Code;

    @Transient
    private int colorMargin2Code;

    @Transient
    private int colorButton1Code;

    @Transient
    private int colorButton2Code;

    @Transient
    private int colorColumn1Code;

    @Transient
    private int colorColumn2Code;

    @Transient
    private int colorBackground1Code;

    @Transient
    private int colorBackground2Code;

    @Transient
    private int colorFont1Code;

    @Transient
    private int colorFont2Code;

    @Transient
    private int colorFont3Code;

    public int getColorMargin1Code() {
        return translateColorCode(colorMargin1);
    }

    public int getColorMargin2Code() {
        return translateColorCode(colorMargin2);
    }

    public int getColorButton1Code() {
        return translateColorCode(colorButton1);
    }

    public int getColorButton2Code() {
        return translateColorCode(colorButton2);
    }

    public int getColorColumn1Code() {
        return translateColorCode(colorColumn1);
    }

    public int getColorColumn2Code() {
        return translateColorCode(colorColumn2);
    }

    public int getColorBackground1Code() {
        return translateColorCode(colorBackground1);
    }

    public int getColorBackground2Code() {
        return translateColorCode(colorBackground2);
    }

    public int getColorFont1Code() {
        return translateColorCode(colorFont1);
    }

    public int getColorFont2Code() {
        return translateColorCode(colorFont2);
    }

    public int getColorFont3Code() {
        return translateColorCode(colorFont3);
    }

    private static int translateColorCode(String serverColorCode) {
        String[] codes = serverColorCode.split(" / ");
        float alpha = Float.parseFloat(codes[1]);
        long alphaFixed = Math.round(alpha * 255);
        String alphaHex = Long.toHexString(alphaFixed);
        String colorCode = "#" + alphaHex + codes[0];
        return Color.parseColor(colorCode);
    }
}
