package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.ACCOUNT_ADDRESS_HOME;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_ADDRESS_OFFICE;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_COMPANY;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_COUNTRY;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_DESIGNATION;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_EMAIL;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_FIRST_NAME;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_FULL_NAME;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_ID;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_IS_ACTIVATED;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_LAST_NAME;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_LOGIN_NAME;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_MOBILE;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_PASSWORD;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_SALUTATION;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_TEL;
import static com.corpit.standard.share.ParameterNames.ACCOUNT_TYPE;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Account {

    @Id
    @SerializedName(ACCOUNT_ID)
    private String id;

    @SerializedName(ACCOUNT_FULL_NAME)
    private String fullName;

    @SerializedName(ACCOUNT_TYPE)
    private String type;

    @SerializedName(ACCOUNT_SALUTATION)
    private String salutation;

    @SerializedName(ACCOUNT_FIRST_NAME)
    private String firstName;

    @SerializedName(ACCOUNT_LAST_NAME)
    private String lastName;

    @SerializedName(ACCOUNT_DESIGNATION)
    private String designation;

    @SerializedName(ACCOUNT_EMAIL)
    private String email;

    @SerializedName(ACCOUNT_COMPANY)
    private String company;

    @SerializedName(ACCOUNT_ADDRESS_OFFICE)
    private String officeAddress;

    @SerializedName(ACCOUNT_ADDRESS_HOME)
    private String homeAddress;

    @SerializedName(ACCOUNT_COUNTRY)
    private String country;

    @SerializedName(ACCOUNT_TEL)
    private String tel; //country code - area code - telephone number

    @SerializedName(ACCOUNT_MOBILE)
    private String mobile; //country code - area code - mobile number

    @SerializedName(ACCOUNT_LOGIN_NAME)
    private String loginName;

    @SerializedName(ACCOUNT_PASSWORD)
    private String password;

    @SerializedName(ACCOUNT_IS_ACTIVATED)
    private String isActivated;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1592163734)
    public Account(String id, String fullName, String type, String salutation,
            String firstName, String lastName, String designation, String email,
            String company, String officeAddress, String homeAddress,
            String country, String tel, String mobile, String loginName,
            String password, String isActivated, String deleteFlag,
            String updatedDate) {
        this.id = id;
        this.fullName = fullName;
        this.type = type;
        this.salutation = salutation;
        this.firstName = firstName;
        this.lastName = lastName;
        this.designation = designation;
        this.email = email;
        this.company = company;
        this.officeAddress = officeAddress;
        this.homeAddress = homeAddress;
        this.country = country;
        this.tel = tel;
        this.mobile = mobile;
        this.loginName = loginName;
        this.password = password;
        this.isActivated = isActivated;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 882125521)
    public Account() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIsActivated() {
        return isActivated;
    }

    public void setIsActivated(String isActivated) {
        this.isActivated = isActivated;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
