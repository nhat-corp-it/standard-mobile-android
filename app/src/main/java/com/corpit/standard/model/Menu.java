package com.corpit.standard.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.text.TextUtils;

import com.corpit.standard.data.remote.WithImage;
import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.MENU_ICON_URL;
import static com.corpit.standard.share.ParameterNames.MENU_ID;
import static com.corpit.standard.share.ParameterNames.MENU_ORDER_ID;
import static com.corpit.standard.share.ParameterNames.MENU_PAGE_TYPE;
import static com.corpit.standard.share.ParameterNames.MENU_PARENT_MENU_ID;
import static com.corpit.standard.share.ParameterNames.MENU_PERMISSION_CONTROL;
import static com.corpit.standard.share.ParameterNames.MENU_STATUS;
import static com.corpit.standard.share.ParameterNames.MENU_TIMESTAMP;
import static com.corpit.standard.share.ParameterNames.MENU_TITLE_CN;
import static com.corpit.standard.share.ParameterNames.MENU_TITLE_EN;
import static com.corpit.standard.share.ParameterNames.MENU_TYPE;
import static com.corpit.standard.share.ParameterNames.MENU_WEB_LINK;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 24/3/18.
 */

@Entity
public class Menu implements ListItem<Menu>, WithImage, Parcelable {

    @StringDef({MenuType.QUICK_MENU, MenuType.SIDE_MENU})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MenuType {
        String QUICK_MENU = "Quick Menu";
        String SIDE_MENU = "Side Menu";
    }

    @StringDef({PageType.BLANK, PageType.MORE_INFORMATION, PageType.EXHIBITORS, PageType.FLOORPLAN,
            PageType.SPONSOR, PageType.CONFERENCE, PageType.EVENT, PageType.SPEAKER, PageType.WEBLINK,
            PageType.INBOX, PageType.BOOKMARK, PageType.LIVE_QA, PageType.POLLING, PageType.NEWS,
            PageType.NEWSLETTERS, PageType.MOMENTS, PageType.E_POSTER, PageType.SURVEY, PageType.UPDATE,
            PageType.LOGOUT})
    public @interface PageType {
        String BLANK = "Blank";
        String MORE_INFORMATION = "MoreInformation";
        String EXHIBITORS = "ExhibitionDirectory";
        String FLOORPLAN = "Floorplan";
        String SPONSOR = "Sponsor";
        String CONFERENCE = "ConferenceSchedule";
        String EVENT = "FullSchedule";
        String SPEAKER = "SpeakerList";
        String WEBLINK = "Weblink";
        String INBOX = "Inbox";
        String BOOKMARK = "MyAgenda";
        String LIVE_QA = "LiveQA";
        String POLLING = "Polling";
        String NEWS = "NewsList";
        String NEWSLETTERS = "NewsLetters";
        String MOMENTS = "Moments";
        String E_POSTER = "E-poster";
        String SURVEY = "Survey";
        String UPDATE = "Updates";
        String LOGOUT = "Logout";
    }

    @Id
    @SerializedName(MENU_ID)
    private String id;

    @SerializedName(MENU_ORDER_ID)
    private String orderId;

    @SerializedName(MENU_PARENT_MENU_ID)
    private String parentId;

    @MenuType
    @SerializedName(MENU_TYPE)
    private String menuType;

    @PageType
    @SerializedName(MENU_PAGE_TYPE)
    private String pageType;

    @SerializedName(MENU_TITLE_EN)
    private String enTitle;

    @SerializedName(MENU_TITLE_CN)
    private String cnTitle;

    public static final Creator<Menu> CREATOR = new Creator<Menu>() {
        @Override
        public Menu createFromParcel(Parcel source) {
            return new Menu(source);
        }

        @Override
        public Menu[] newArray(int size) {
            return new Menu[size];
        }
    };

    @SerializedName(MENU_PERMISSION_CONTROL)
    private String permission;

    @SerializedName(MENU_ICON_URL)
    private String iconUrl;

    @SerializedName(MENU_STATUS)
    private String status;

    @SerializedName(MENU_TIMESTAMP)
    private String timeStamp;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;
    @SerializedName(MENU_WEB_LINK)
    private String webLink;

    @Generated(hash = 1631206187)
    public Menu() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public @MenuType String getMenuType() {
        return menuType;
    }

    public void setMenuType(@MenuType String menuType) {
        this.menuType = menuType;
    }

    public @PageType String getPageType() {
        return pageType;
    }

    public void setPageType(@PageType String pageType) {
        this.pageType = pageType;
    }

    public String getEnTitle() {
        return enTitle;
    }

    public void setEnTitle(String enTitle) {
        this.enTitle = enTitle;
    }

    public String getCnTitle() {
        return cnTitle;
    }

    public void setCnTitle(String cnTitle) {
        this.cnTitle = cnTitle;
    }

    @Generated(hash = 2011226286)
    public Menu(String id, String orderId, String parentId, String menuType,
                String pageType, String enTitle, String cnTitle, String permission,
                String iconUrl, String status, String timeStamp, String deleteFlag,
                String updatedDate, String webLink) {
        this.id = id;
        this.orderId = orderId;
        this.parentId = parentId;
        this.menuType = menuType;
        this.pageType = pageType;
        this.enTitle = enTitle;
        this.cnTitle = cnTitle;
        this.permission = permission;
        this.iconUrl = iconUrl;
        this.status = status;
        this.timeStamp = timeStamp;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
        this.webLink = webLink;
    }

    protected Menu(Parcel in) {
        this.id = in.readString();
        this.orderId = in.readString();
        this.parentId = in.readString();
        this.menuType = in.readString();
        this.pageType = in.readString();
        this.enTitle = in.readString();
        this.cnTitle = in.readString();
        this.webLink = in.readString();
        this.permission = in.readString();
        this.iconUrl = in.readString();
        this.status = in.readString();
        this.timeStamp = in.readString();
        this.deleteFlag = in.readString();
        this.updatedDate = in.readString();
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getIntOrderId() {
        return Integer.parseInt(orderId);
    }

    @Override
    public int compareTo(@NonNull Menu target) {
        return getIntOrderId() - target.getIntOrderId();
    }

    @Override
    public String getUrl() {
        return iconUrl;
    }

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.orderId);
        dest.writeString(this.parentId);
        dest.writeString(this.menuType);
        dest.writeString(this.pageType);
        dest.writeString(this.enTitle);
        dest.writeString(this.cnTitle);
        dest.writeString(this.webLink);
        dest.writeString(this.permission);
        dest.writeString(this.iconUrl);
        dest.writeString(this.status);
        dest.writeString(this.timeStamp);
        dest.writeString(this.deleteFlag);
        dest.writeString(this.updatedDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Menu menu = (Menu) o;

        if (id != null ? !id.equals(menu.id) : menu.id != null) return false;
        if (orderId != null ? !orderId.equals(menu.orderId) : menu.orderId != null) return false;
        if (parentId != null ? !parentId.equals(menu.parentId) : menu.parentId != null)
            return false;
        if (menuType != null ? !menuType.equals(menu.menuType) : menu.menuType != null)
            return false;
        if (pageType != null ? !pageType.equals(menu.pageType) : menu.pageType != null)
            return false;
        if (enTitle != null ? !enTitle.equals(menu.enTitle) : menu.enTitle != null) return false;
        if (cnTitle != null ? !cnTitle.equals(menu.cnTitle) : menu.cnTitle != null) return false;
        if (permission != null ? !permission.equals(menu.permission) : menu.permission != null)
            return false;
        if (iconUrl != null ? !iconUrl.equals(menu.iconUrl) : menu.iconUrl != null) return false;
        if (status != null ? !status.equals(menu.status) : menu.status != null) return false;
        if (timeStamp != null ? !timeStamp.equals(menu.timeStamp) : menu.timeStamp != null)
            return false;
        if (deleteFlag != null ? !deleteFlag.equals(menu.deleteFlag) : menu.deleteFlag != null)
            return false;
        if (updatedDate != null ? !updatedDate.equals(menu.updatedDate) : menu.updatedDate != null)
            return false;
        return webLink != null ? webLink.equals(menu.webLink) : menu.webLink == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (menuType != null ? menuType.hashCode() : 0);
        result = 31 * result + (pageType != null ? pageType.hashCode() : 0);
        result = 31 * result + (enTitle != null ? enTitle.hashCode() : 0);
        result = 31 * result + (cnTitle != null ? cnTitle.hashCode() : 0);
        result = 31 * result + (permission != null ? permission.hashCode() : 0);
        result = 31 * result + (iconUrl != null ? iconUrl.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (timeStamp != null ? timeStamp.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        result = 31 * result + (webLink != null ? webLink.hashCode() : 0);
        return result;
    }

    public boolean isGroup() {
        return TextUtils.isEmpty(parentId);
    }
}
