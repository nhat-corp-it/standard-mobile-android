package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.corpit.standard.util.DateConverter;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.util.Calendar;

import static com.corpit.standard.share.ParameterNames.CONFERENCE_BRIEF;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_CAT;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_DATE;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_END_TIME;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_ID;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_LIVE_QA;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_PARENT_CONF_ID;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_START_TIME;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_TITLE;
import static com.corpit.standard.share.ParameterNames.CONFERENCE_VENUE;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

@Entity
public class PollingConference implements ListItem<PollingConference> {

    @Id
    @SerializedName(CONFERENCE_ID)
    private String id;

    @SerializedName(CONFERENCE_CAT)
    private String category;

    @SerializedName(CONFERENCE_DATE)
    private String date;

    @SerializedName(CONFERENCE_START_TIME)
    private String startTime;

    @SerializedName(CONFERENCE_TITLE)
    private String title;

    @SerializedName(CONFERENCE_VENUE)
    private String boothId;

    @SerializedName(CONFERENCE_LIVE_QA)
    private String liveQA;

    @SerializedName(CONFERENCE_BRIEF)
    private String brief;

    @SerializedName(CONFERENCE_PARENT_CONF_ID)
    private String mainConferenceId;

    @SerializedName(CONFERENCE_END_TIME)
    private String endTime;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 731856731)
    public PollingConference(String id, String category, String date, String startTime, String title,
            String boothId, String liveQA, String brief, String mainConferenceId, String endTime,
            String deleteFlag, String updatedDate) {
        this.id = id;
        this.category = category;
        this.date = date;
        this.startTime = startTime;
        this.title = title;
        this.boothId = boothId;
        this.liveQA = liveQA;
        this.brief = brief;
        this.mainConferenceId = mainConferenceId;
        this.endTime = endTime;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 2041505094)
    public PollingConference() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBoothId() {
        return boothId;
    }

    public void setBoothId(String boothId) {
        this.boothId = boothId;
    }

    public String getLiveQA() {
        return liveQA;
    }

    public void setLiveQA(String liveQA) {
        this.liveQA = liveQA;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getMainConferenceId() {
        return mainConferenceId;
    }

    public void setMainConferenceId(String mainConferenceId) {
        this.mainConferenceId = mainConferenceId;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Calendar getCalendarStartTime() {
        return DateConverter.toCalendar(startTime, date);
    }

    public Calendar getCalendarEndTime() {
        return DateConverter.toCalendar(endTime, date);
    }

    public Calendar getDateObject() {
        return DateConverter.toCalendar(date);
    }

    public String getFormattedStartTime() {
        if (startTime.isEmpty()) return "";

        return startTime.substring(0, 2) + ":"
                + startTime.substring(2, 4);
    }

    public String getFormattedEndTime() {
        if (endTime.isEmpty()) return "";

        return endTime.substring(0, 2) + ":"
                + endTime.substring(2, 4);
    }

    @Override
    public int compareTo(@NonNull PollingConference target) {
        if (!getDateObject().equals(target.getDateObject())) {
            return getDateObject().compareTo(target.getDateObject());
        } else if (!getCalendarStartTime().equals(target.getCalendarStartTime())) {
            return getCalendarStartTime().compareTo(target.getCalendarStartTime());
        } else {
            return getCalendarEndTime().compareTo(target.getCalendarEndTime());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PollingConference that = (PollingConference) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (category != null ? !category.equals(that.category) : that.category != null)
            return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null)
            return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (boothId != null ? !boothId.equals(that.boothId) : that.boothId != null) return false;
        if (liveQA != null ? !liveQA.equals(that.liveQA) : that.liveQA != null) return false;
        if (brief != null ? !brief.equals(that.brief) : that.brief != null) return false;
        if (mainConferenceId != null ? !mainConferenceId.equals(that.mainConferenceId) : that.mainConferenceId != null)
            return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
        if (deleteFlag != null ? !deleteFlag.equals(that.deleteFlag) : that.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(that.updatedDate) : that.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (boothId != null ? boothId.hashCode() : 0);
        result = 31 * result + (liveQA != null ? liveQA.hashCode() : 0);
        result = 31 * result + (brief != null ? brief.hashCode() : 0);
        result = 31 * result + (mainConferenceId != null ? mainConferenceId.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
