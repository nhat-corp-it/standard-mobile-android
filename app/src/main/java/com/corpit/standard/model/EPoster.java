package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.corpit.standard.ui.base.WithDetails;
import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.E_POSTER_ABSTRACT;
import static com.corpit.standard.share.ParameterNames.E_POSTER_AUTHOR;
import static com.corpit.standard.share.ParameterNames.E_POSTER_CAT;
import static com.corpit.standard.share.ParameterNames.E_POSTER_CO_AUTHOR;
import static com.corpit.standard.share.ParameterNames.E_POSTER_DURATION;
import static com.corpit.standard.share.ParameterNames.E_POSTER_FILE;
import static com.corpit.standard.share.ParameterNames.E_POSTER_ID;
import static com.corpit.standard.share.ParameterNames.E_POSTER_TITLE;
import static com.corpit.standard.share.ParameterNames.E_POSTER_VENUE;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class EPoster implements ListItem<EPoster>, WithDetails {

    @Id
    @SerializedName(E_POSTER_ID)
    private String id;

    @SerializedName(E_POSTER_TITLE)
    private String title;

    @SerializedName(E_POSTER_CAT)
    private String category;

    @SerializedName(E_POSTER_AUTHOR)
    private String author;

    @SerializedName(E_POSTER_CO_AUTHOR)
    private String coauthor;

    @SerializedName(E_POSTER_VENUE)
    private String venue;

    @SerializedName(E_POSTER_DURATION)
    private String duration; //( Eg - 09:00 - 10:30 )

    @SerializedName(E_POSTER_FILE)
    private String file;

    @SerializedName(E_POSTER_ABSTRACT)
    private String posterAbstract;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 201605394)
    public EPoster(String id, String title, String category, String author,
                   String coauthor, String venue, String duration, String file,
                   String posterAbstract, String deleteFlag, String updatedDate) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.author = author;
        this.coauthor = coauthor;
        this.venue = venue;
        this.duration = duration;
        this.file = file;
        this.posterAbstract = posterAbstract;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 519804030)
    public EPoster() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCoauthor() {
        return coauthor;
    }

    public void setCoauthor(String coauthor) {
        this.coauthor = coauthor;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getPosterAbstract() {
        return posterAbstract;
    }

    public void setPosterAbstract(String posterAbstract) {
        this.posterAbstract = posterAbstract;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String getText() {
        return title;
    }

    @Override
    public String getSectionName() {
        String s = title.substring(0, 1).toUpperCase();

        return s.matches("[A-Za-z]")
                ? s
                : "#";
    }

    @Override
    public int compareTo(@NonNull EPoster o) {

        if (!category.equals(o.category)) {
            return category.compareTo(o.category);
        } else {
            return title.compareTo(o.title);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EPoster ePoster = (EPoster) o;

        if (id != null ? !id.equals(ePoster.id) : ePoster.id != null) return false;
        if (title != null ? !title.equals(ePoster.title) : ePoster.title != null) return false;
        if (category != null ? !category.equals(ePoster.category) : ePoster.category != null)
            return false;
        if (author != null ? !author.equals(ePoster.author) : ePoster.author != null) return false;
        if (coauthor != null ? !coauthor.equals(ePoster.coauthor) : ePoster.coauthor != null)
            return false;
        if (venue != null ? !venue.equals(ePoster.venue) : ePoster.venue != null) return false;
        if (duration != null ? !duration.equals(ePoster.duration) : ePoster.duration != null)
            return false;
        if (file != null ? !file.equals(ePoster.file) : ePoster.file != null) return false;
        return posterAbstract != null ? posterAbstract.equals(ePoster.posterAbstract) : ePoster.posterAbstract == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (coauthor != null ? coauthor.hashCode() : 0);
        result = 31 * result + (venue != null ? venue.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (file != null ? file.hashCode() : 0);
        result = 31 * result + (posterAbstract != null ? posterAbstract.hashCode() : 0);
        return result;
    }
}
