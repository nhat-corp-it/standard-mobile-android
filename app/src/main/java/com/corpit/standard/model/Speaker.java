package com.corpit.standard.model;

import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;

import com.corpit.standard.data.remote.WithImage;
import com.corpit.standard.ui.base.WithDetails;
import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.SPEAKER_ADDRESS;
import static com.corpit.standard.share.ParameterNames.SPEAKER_BIO;
import static com.corpit.standard.share.ParameterNames.SPEAKER_COMPANY;
import static com.corpit.standard.share.ParameterNames.SPEAKER_COUNTRY;
import static com.corpit.standard.share.ParameterNames.SPEAKER_EMAIL;
import static com.corpit.standard.share.ParameterNames.SPEAKER_FULL_NAME;
import static com.corpit.standard.share.ParameterNames.SPEAKER_ID;
import static com.corpit.standard.share.ParameterNames.SPEAKER_JOB;
import static com.corpit.standard.share.ParameterNames.SPEAKER_MOBILE;
import static com.corpit.standard.share.ParameterNames.SPEAKER_PROFILE_PIC;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Speaker implements ListItem<Speaker>, WithDetails, WithImage {

    @Id
    @SerializedName(SPEAKER_ID)
    private String id;

    @SerializedName(SPEAKER_FULL_NAME)
    private String fullName;

    @SerializedName(SPEAKER_JOB)
    private String job;

    @SerializedName(SPEAKER_COMPANY)
    private String company;

    @SerializedName(SPEAKER_EMAIL)
    private String email;

    @SerializedName(SPEAKER_MOBILE)
    private String mobile;

    @SerializedName(SPEAKER_ADDRESS)
    private String address;

    @SerializedName(SPEAKER_COUNTRY)
    private String country;

    @SerializedName(SPEAKER_BIO)
    private String bio;

    @SerializedName(SPEAKER_PROFILE_PIC)
    private String profilePic;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 124020152)
    public Speaker(String id, String fullName, String job, String company,
                   String email, String mobile, String address, String country, String bio,
                   String profilePic, String deleteFlag, String updatedDate) {
        this.id = id;
        this.fullName = fullName;
        this.job = job;
        this.company = company;
        this.email = email;
        this.mobile = mobile;
        this.address = address;
        this.country = country;
        this.bio = bio;
        this.profilePic = profilePic;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 2083994827)
    public Speaker() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public int compareTo(@NonNull Speaker speaker) {
        return fullName.toUpperCase().compareTo(speaker.fullName.toUpperCase());
    }

    @Override
    public String getText() {
        return fullName;
    }

    public String getSectionName() {
        String s = fullName.substring(0, 1).toUpperCase();

        return s.matches("[A-Za-z]")
                ? s
                : "#";
    }

    public Spanned getBioToDisplay() {

        StringBuilder html = new StringBuilder();
        html.append("<html><head><meta http-equiv=\"Content-Type\" companyProfile=\"text/html; charset=utf-8\" />");
        html.append("</head><body>");
        if (bio != null && !bio.isEmpty()) {
            bio = bio.replaceAll("\n", "<br/>");
            bio = bio.replaceAll(" width: [0-9]*px;", "");
            bio = bio.replaceAll("font-family:[^;]*;", "");
            bio = bio.replaceAll("14px", "18px");
            bio = bio.replaceAll("13px", "18px");
            bio = bio.replaceAll("12px", "18px");
            bio = bio.replaceAll("background-color:[^;]*;", "");
        }
        html.append(bio);
        html.append("</body>");
        html.append("</html>");

        return Html.fromHtml(html.toString());
    }

    @Override
    public String getUrl() {
        return profilePic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Speaker speaker = (Speaker) o;

        if (id != null ? !id.equals(speaker.id) : speaker.id != null) return false;
        if (fullName != null ? !fullName.equals(speaker.fullName) : speaker.fullName != null)
            return false;
        if (job != null ? !job.equals(speaker.job) : speaker.job != null) return false;
        if (company != null ? !company.equals(speaker.company) : speaker.company != null)
            return false;
        if (email != null ? !email.equals(speaker.email) : speaker.email != null) return false;
        if (mobile != null ? !mobile.equals(speaker.mobile) : speaker.mobile != null) return false;
        if (address != null ? !address.equals(speaker.address) : speaker.address != null)
            return false;
        if (country != null ? !country.equals(speaker.country) : speaker.country != null)
            return false;
        if (bio != null ? !bio.equals(speaker.bio) : speaker.bio != null) return false;
        if (profilePic != null ? !profilePic.equals(speaker.profilePic) : speaker.profilePic != null)
            return false;
        if (deleteFlag != null ? !deleteFlag.equals(speaker.deleteFlag) : speaker.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(speaker.updatedDate) : speaker.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
        result = 31 * result + (job != null ? job.hashCode() : 0);
        result = 31 * result + (company != null ? company.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (bio != null ? bio.hashCode() : 0);
        result = 31 * result + (profilePic != null ? profilePic.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
