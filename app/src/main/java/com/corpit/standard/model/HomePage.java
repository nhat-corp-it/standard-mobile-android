package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.HOME_PAGE_CONTENT;
import static com.corpit.standard.share.ParameterNames.HOME_PAGE_ID;
import static com.corpit.standard.share.ParameterNames.HOME_PAGE_MAIN_TITLE;
import static com.corpit.standard.share.ParameterNames.HOME_PAGE_SUB_TITLE;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class HomePage {

    @Id
    @SerializedName(HOME_PAGE_ID)
    private String id;

    @SerializedName(HOME_PAGE_MAIN_TITLE)
    private String mainTitle;

    @SerializedName(HOME_PAGE_SUB_TITLE)
    private String subTitle;

    @SerializedName(HOME_PAGE_CONTENT)
    private String content;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1545828349)
    public HomePage(String id, String mainTitle, String subTitle, String content,
            String deleteFlag, String updatedDate) {
        this.id = id;
        this.mainTitle = mainTitle;
        this.subTitle = subTitle;
        this.content = content;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1911342170)
    public HomePage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMainTitle() {
        return mainTitle;
    }

    public void setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
