package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.PRODUCT_CAT_ID;
import static com.corpit.standard.share.ParameterNames.PRODUCT_CAT_NAME;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class ProductCategory {

    @Id
    @SerializedName(PRODUCT_CAT_ID)
    private String id;

    @SerializedName(PRODUCT_CAT_NAME)
    private String name;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 732968982)
    public ProductCategory(String id, String name, String deleteFlag,
            String updatedDate) {
        this.id = id;
        this.name = name;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 587827635)
    public ProductCategory() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
