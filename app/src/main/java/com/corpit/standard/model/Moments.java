package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.MOMENT_ADDED_TIME;
import static com.corpit.standard.share.ParameterNames.MOMENT_ID;
import static com.corpit.standard.share.ParameterNames.MOMENT_PHOTO_COMMENT;
import static com.corpit.standard.share.ParameterNames.MOMENT_STATUS;
import static com.corpit.standard.share.ParameterNames.MOMENT_TEXT_COMMENT;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Moments {

    @Id
    @SerializedName(MOMENT_ID)
    private String id;

    @SerializedName(MOMENT_TEXT_COMMENT)
    private String textComment;

    @SerializedName(MOMENT_PHOTO_COMMENT)
    private String photoComment;

    @SerializedName(MOMENT_STATUS)
    private String status;

    @SerializedName(MOMENT_ADDED_TIME)
    private String addedTime;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 2015589121)
    public Moments(String id, String textComment, String photoComment,
            String status, String addedTime, String deleteFlag,
            String updatedDate) {
        this.id = id;
        this.textComment = textComment;
        this.photoComment = photoComment;
        this.status = status;
        this.addedTime = addedTime;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1604767730)
    public Moments() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTextComment() {
        return textComment;
    }

    public void setTextComment(String textComment) {
        this.textComment = textComment;
    }

    public String getPhotoComment() {
        return photoComment;
    }

    public void setPhotoComment(String photoComment) {
        this.photoComment = photoComment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(String addedTime) {
        this.addedTime = addedTime;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
