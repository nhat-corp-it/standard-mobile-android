package com.corpit.standard.model;

import com.corpit.standard.BuildConfig;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.util.ArrayList;
import java.util.Arrays;

import static com.corpit.standard.share.ParameterNames.CONFIG_ANDROID_LINK;
import static com.corpit.standard.share.ParameterNames.CONFIG_ANDROID_VERSION;
import static com.corpit.standard.share.ParameterNames.CONFIG_APP_ICON;
import static com.corpit.standard.share.ParameterNames.CONFIG_ENABLE_ACCOUNT;
import static com.corpit.standard.share.ParameterNames.CONFIG_ID;
import static com.corpit.standard.share.ParameterNames.CONFIG_IOS_LINK;
import static com.corpit.standard.share.ParameterNames.CONFIG_IOS_VERSION;
import static com.corpit.standard.share.ParameterNames.CONFIG_LANGUAGE;
import static com.corpit.standard.share.ParameterNames.CONFIG_REG_URL;
import static com.corpit.standard.share.ParameterNames.CONFIG_SHOW_END_DATE;
import static com.corpit.standard.share.ParameterNames.CONFIG_SHOW_NAME;
import static com.corpit.standard.share.ParameterNames.CONFIG_SHOW_START_DATE;
import static com.corpit.standard.share.ParameterNames.CONFIG_SURVEY_END_DATE;
import static com.corpit.standard.share.ParameterNames.CONFIG_SURVEY_START_DATE;
import static com.corpit.standard.share.ParameterNames.CONFIG_TEMPLATE;
import static com.corpit.standard.share.ParameterNames.CONFIG_TIME_STAMP;
import static com.corpit.standard.share.ParameterNames.CONFIG_VENUE;

@Entity
public class Configuration {

    @Id
    @SerializedName(CONFIG_ID)
    private String id;

    @SerializedName(CONFIG_TIME_STAMP)
    private String timestamp;

    @SerializedName(CONFIG_SHOW_NAME)
    private String showName;

    @SerializedName(CONFIG_VENUE)
    private String showVenue;

    @SerializedName(CONFIG_SHOW_START_DATE)
    private String showStartDate;

    @SerializedName(CONFIG_SHOW_END_DATE)
    private String showEndDate;

    @SerializedName(CONFIG_SURVEY_START_DATE)
    private String surveyStartDate;

    @SerializedName(CONFIG_SURVEY_END_DATE)
    private String surveyEndDate;

    @SerializedName(CONFIG_REG_URL)
    private String visitorRegUrl;

    @SerializedName(CONFIG_LANGUAGE)
    private String systemLanguage;

    @SerializedName(CONFIG_TEMPLATE)
    private String homePageTemplate;

    @SerializedName(CONFIG_APP_ICON)
    private String appIcon;

    @SerializedName(CONFIG_ANDROID_LINK)
    private String androidDownloadLink;

    @SerializedName(CONFIG_ANDROID_VERSION)
    private String androidVersion;

    @SerializedName(CONFIG_IOS_VERSION)
    private String iOSVersion;

    @SerializedName(CONFIG_IOS_LINK)
    private String iOSDownloadLink;

    @SerializedName(CONFIG_ENABLE_ACCOUNT)
    private String enableAccount;

    @Generated(hash = 275730378)
    public Configuration(String id, String timestamp, String showName,
                         String showVenue, String showStartDate, String showEndDate,
                         String surveyStartDate, String surveyEndDate, String visitorRegUrl,
                         String systemLanguage, String homePageTemplate, String appIcon,
                         String androidDownloadLink, String androidVersion, String iOSVersion,
                         String iOSDownloadLink, String enableAccount) {
        this.id = id;
        this.timestamp = timestamp;
        this.showName = showName;
        this.showVenue = showVenue;
        this.showStartDate = showStartDate;
        this.showEndDate = showEndDate;
        this.surveyStartDate = surveyStartDate;
        this.surveyEndDate = surveyEndDate;
        this.visitorRegUrl = visitorRegUrl;
        this.systemLanguage = systemLanguage;
        this.homePageTemplate = homePageTemplate;
        this.appIcon = appIcon;
        this.androidDownloadLink = androidDownloadLink;
        this.androidVersion = androidVersion;
        this.iOSVersion = iOSVersion;
        this.iOSDownloadLink = iOSDownloadLink;
        this.enableAccount = enableAccount;
    }

    @Generated(hash = 365253574)
    public Configuration() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getShowName() {
        return this.showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

    public String getShowVenue() {
        return this.showVenue;
    }

    public void setShowVenue(String showVenue) {
        this.showVenue = showVenue;
    }

    public String getShowStartDate() {
        return this.showStartDate;
    }

    public void setShowStartDate(String showStartDate) {
        this.showStartDate = showStartDate;
    }

    public String getShowEndDate() {
        return this.showEndDate;
    }

    public void setShowEndDate(String showEndDate) {
        this.showEndDate = showEndDate;
    }

    public String getSurveyStartDate() {
        return this.surveyStartDate;
    }

    public void setSurveyStartDate(String surveyStartDate) {
        this.surveyStartDate = surveyStartDate;
    }

    public String getSurveyEndDate() {
        return this.surveyEndDate;
    }

    public void setSurveyEndDate(String surveyEndDate) {
        this.surveyEndDate = surveyEndDate;
    }

    public String getVisitorRegUrl() {
        return this.visitorRegUrl;
    }

    public void setVisitorRegUrl(String visitorRegUrl) {
        this.visitorRegUrl = visitorRegUrl;
    }

    public String getSystemLanguage() {
        return this.systemLanguage;
    }

    public void setSystemLanguage(String systemLanguage) {
        this.systemLanguage = systemLanguage;
    }

    public String getHomePageTemplate() {
        return this.homePageTemplate;
    }

    public void setHomePageTemplate(String homePageTemplate) {
        this.homePageTemplate = homePageTemplate;
    }

    public String getAppIcon() {
        return this.appIcon;
    }

    public void setAppIcon(String appIcon) {
        this.appIcon = appIcon;
    }

    public String getAndroidDownloadLink() {
        return this.androidDownloadLink;
    }

    public void setAndroidDownloadLink(String androidDownloadLink) {
        this.androidDownloadLink = androidDownloadLink;
    }

    public String getAndroidVersion() {
        return this.androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getIOSVersion() {
        return this.iOSVersion;
    }

    public void setIOSVersion(String iOSVersion) {
        this.iOSVersion = iOSVersion;
    }

    public String getIOSDownloadLink() {
        return this.iOSDownloadLink;
    }

    public void setIOSDownloadLink(String iOSDownloadLink) {
        this.iOSDownloadLink = iOSDownloadLink;
    }

    public String getEnableAccount() {
        return this.enableAccount;
    }

    public void setEnableAccount(String enableAccount) {
        this.enableAccount = enableAccount;
    }

    public boolean isNewerVersion() {
        ArrayList<String> currents = new ArrayList<>(Arrays.asList(BuildConfig.VERSION_NAME.split("\\.")));
        ArrayList<String> latests = new ArrayList<>(Arrays.asList(androidVersion.split("\\.")));

        while (currents.size() < 3) {
            currents.add("0");
        }
        while (latests.size() < 3) {
            latests.add("0");
        }

        for (int i = 0; i < 3; i++) {
            int compareTo = currents.get(i).compareTo(latests.get(i));
            if (compareTo != 0) {
                return compareTo < 0;
            }
        }

        return false;
    }
}
