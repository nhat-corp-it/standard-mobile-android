package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.BOOTH_HEIGHT;
import static com.corpit.standard.share.ParameterNames.BOOTH_ID;
import static com.corpit.standard.share.ParameterNames.BOOTH_LOCATION;
import static com.corpit.standard.share.ParameterNames.BOOTH_NAME;
import static com.corpit.standard.share.ParameterNames.BOOTH_WIDTH;
import static com.corpit.standard.share.ParameterNames.BOOTH_X;
import static com.corpit.standard.share.ParameterNames.BOOTH_Y;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Booth {

    @Id
    @SerializedName(BOOTH_ID)
    private String id;

    @SerializedName(BOOTH_NAME)
    private String name;

    @SerializedName(BOOTH_LOCATION)
    private String locationId;

    @SerializedName(BOOTH_WIDTH)
    private String width;

    @SerializedName(BOOTH_HEIGHT)
    private String height;

    @SerializedName(BOOTH_X)
    private String x;

    @SerializedName(BOOTH_Y)
    private String y;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 817417479)
    public Booth(String id, String name, String locationId, String width,
            String height, String x, String y, String deleteFlag,
            String updatedDate) {
        this.id = id;
        this.name = name;
        this.locationId = locationId;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1922492776)
    public Booth() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
