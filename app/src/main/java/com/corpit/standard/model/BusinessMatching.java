package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_DATE;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_EXHIBITOR_COMMENT;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_EXHIBITOR_ID;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_ID;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_INTERESTED_PRODUCT;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_REQUESTED_TIME;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_STATUS;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_TIME;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_VISITOR_COMMENT;
import static com.corpit.standard.share.ParameterNames.BUSINESS_MATCHING_VISITOR_ID;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class BusinessMatching {

    @Id
    @SerializedName(BUSINESS_MATCHING_ID)
    private String id;

    @SerializedName(BUSINESS_MATCHING_VISITOR_ID)
    private String visitorId;

    @SerializedName(BUSINESS_MATCHING_EXHIBITOR_ID)
    private String exhibitorId;

    @SerializedName(BUSINESS_MATCHING_DATE)
    private String date;

    @SerializedName(BUSINESS_MATCHING_TIME)
    private String time;

    @SerializedName(BUSINESS_MATCHING_STATUS)
    private String status;

    @SerializedName(BUSINESS_MATCHING_EXHIBITOR_COMMENT)
    private String exhibitorComment;

    @SerializedName(BUSINESS_MATCHING_REQUESTED_TIME)
    private String requestedTime;

    @SerializedName(BUSINESS_MATCHING_VISITOR_COMMENT)
    private String visitorComment;

    @SerializedName(BUSINESS_MATCHING_INTERESTED_PRODUCT)
    private String interestedProduct;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1495996146)
    public BusinessMatching(String id, String visitorId, String exhibitorId, String date,
            String time, String status, String exhibitorComment, String requestedTime,
            String visitorComment, String interestedProduct, String deleteFlag,
            String updatedDate) {
        this.id = id;
        this.visitorId = visitorId;
        this.exhibitorId = exhibitorId;
        this.date = date;
        this.time = time;
        this.status = status;
        this.exhibitorComment = exhibitorComment;
        this.requestedTime = requestedTime;
        this.visitorComment = visitorComment;
        this.interestedProduct = interestedProduct;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1995949496)
    public BusinessMatching() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    public String getExhibitorId() {
        return exhibitorId;
    }

    public void setExhibitorId(String exhibitorId) {
        this.exhibitorId = exhibitorId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExhibitorComment() {
        return exhibitorComment;
    }

    public void setExhibitorComment(String exhibitorComment) {
        this.exhibitorComment = exhibitorComment;
    }

    public String getRequestedTime() {
        return requestedTime;
    }

    public void setRequestedTime(String requestedTime) {
        this.requestedTime = requestedTime;
    }

    public String getVisitorComment() {
        return visitorComment;
    }

    public void setVisitorComment(String visitorComment) {
        this.visitorComment = visitorComment;
    }

    public String getInterestedProduct() {
        return interestedProduct;
    }

    public void setInterestedProduct(String interestedProduct) {
        this.interestedProduct = interestedProduct;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
