package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.corpit.standard.ui.base.Expandable;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.PRODUCT_CAT;
import static com.corpit.standard.share.ParameterNames.PRODUCT_ID;
import static com.corpit.standard.share.ParameterNames.PRODUCT_MAIN_PRODUCT;
import static com.corpit.standard.share.ParameterNames.PRODUCT_NAME;
import static com.corpit.standard.share.ParameterNames.PRODUCT_ORDER;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Product implements Comparable<Product>, Expandable {

    @Id
    @SerializedName(PRODUCT_ID)
    private String id;

    @SerializedName(PRODUCT_ORDER)
    private String order;

    @SerializedName(PRODUCT_NAME)
    private String name;

    @SerializedName(PRODUCT_CAT)
    private String category;

    @SerializedName(PRODUCT_MAIN_PRODUCT)
    private String mainProductId;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1500930937)
    public Product(String id, String order, String name, String category,
                   String mainProductId, String deleteFlag, String updatedDate) {
        this.id = id;
        this.order = order;
        this.name = name;
        this.category = category;
        this.mainProductId = mainProductId;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1890278724)
    public Product() {
    }

    public String getId() {
        return this.id;
    }

    @Override
    public String getText() {
        return name;
    }

    @Override
    public String getIconUrl() {
        return null;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMainProductId() {
        return this.mainProductId;
    }

    public void setMainProductId(String mainProductId) {
        this.mainProductId = mainProductId;
    }

    public String getDeleteFlag() {
        return this.deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return this.updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public int compareTo(@NonNull Product o) {
        if (category.equals(o.category)) {
            return mainProductId.compareTo(o.mainProductId);
        } else {
            return id.compareTo(o.id);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != null ? !id.equals(product.id) : product.id != null) return false;
        if (order != null ? !order.equals(product.order) : product.order != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        if (category != null ? !category.equals(product.category) : product.category != null)
            return false;
        if (mainProductId != null ? !mainProductId.equals(product.mainProductId) : product.mainProductId != null)
            return false;
        if (deleteFlag != null ? !deleteFlag.equals(product.deleteFlag) : product.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(product.updatedDate) : product.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (order != null ? order.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (mainProductId != null ? mainProductId.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
