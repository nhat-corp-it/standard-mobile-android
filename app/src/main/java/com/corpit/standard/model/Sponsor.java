package com.corpit.standard.model;

import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;

import com.corpit.standard.data.remote.WithImage;
import com.corpit.standard.ui.base.Expandable;
import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.SPONSOR_CAT;
import static com.corpit.standard.share.ParameterNames.SPONSOR_CONTENT;
import static com.corpit.standard.share.ParameterNames.SPONSOR_EXHIBITOR_ID;
import static com.corpit.standard.share.ParameterNames.SPONSOR_ID;
import static com.corpit.standard.share.ParameterNames.SPONSOR_LOGO;
import static com.corpit.standard.share.ParameterNames.SPONSOR_MENU_ID;
import static com.corpit.standard.share.ParameterNames.SPONSOR_NAME;
import static com.corpit.standard.share.ParameterNames.SPONSOR_SEQ;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Sponsor implements Expandable, ListItem<Sponsor>, WithImage {

    @Id
    @SerializedName(SPONSOR_ID)
    private String id;

    @SerializedName(SPONSOR_MENU_ID)
    private String menuId;

    @SerializedName(SPONSOR_SEQ)
    private String sequence;

    @SerializedName(SPONSOR_CAT)
    private String category;

    @SerializedName(SPONSOR_NAME)
    private String name;

    @SerializedName(SPONSOR_LOGO)
    private String logo;

    @SerializedName(SPONSOR_CONTENT)
    private String content;

    @SerializedName(SPONSOR_EXHIBITOR_ID)
    private String exhibitorId;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1199630540)
    public Sponsor(String id, String menuId, String sequence, String category, String name, String logo,
                   String content, String exhibitorId, String deleteFlag, String updatedDate) {
        this.id = id;
        this.menuId = menuId;
        this.sequence = sequence;
        this.category = category;
        this.name = name;
        this.logo = logo;
        this.content = content;
        this.exhibitorId = exhibitorId;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1466319185)
    public Sponsor() {
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return name;
    }

    @Override
    public String getIconUrl() {
        return null;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Spanned getContentToDisplay() {
        StringBuilder html = new StringBuilder();
        html.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
        html.append("</head><body>");
        if (content != null && !content.isEmpty()) {
            content = content.replaceAll(" width: [0-9]*px;", "");
            content = content.replaceAll("font-family:[^;]*;", "");
            content = content.replaceAll("14px", "18px");
            content = content.replaceAll("13px", "18px");
            content = content.replaceAll("12px", "18px");
            content = content.replaceAll("background-color:[^;]*;", "");
        }
        html.append(content);
        html.append("</body>");
        html.append("</html>");

        return Html.fromHtml(html.toString());
    }

    public String getExhibitorId() {
        return this.exhibitorId;
    }

    public void setExhibitorId(String exhibitorId) {
        this.exhibitorId = exhibitorId;
    }

    @Override
    public int compareTo(@NonNull Sponsor o) {
        return name.compareTo(o.name);
    }

    @Override
    public String getUrl() {
        return logo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sponsor sponsor = (Sponsor) o;

        if (id != null ? !id.equals(sponsor.id) : sponsor.id != null) return false;
        if (menuId != null ? !menuId.equals(sponsor.menuId) : sponsor.menuId != null) return false;
        if (sequence != null ? !sequence.equals(sponsor.sequence) : sponsor.sequence != null)
            return false;
        if (category != null ? !category.equals(sponsor.category) : sponsor.category != null)
            return false;
        if (name != null ? !name.equals(sponsor.name) : sponsor.name != null) return false;
        if (logo != null ? !logo.equals(sponsor.logo) : sponsor.logo != null) return false;
        if (content != null ? !content.equals(sponsor.content) : sponsor.content != null)
            return false;
        if (exhibitorId != null ? !exhibitorId.equals(sponsor.exhibitorId) : sponsor.exhibitorId != null)
            return false;
        if (deleteFlag != null ? !deleteFlag.equals(sponsor.deleteFlag) : sponsor.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(sponsor.updatedDate) : sponsor.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (menuId != null ? menuId.hashCode() : 0);
        result = 31 * result + (sequence != null ? sequence.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (exhibitorId != null ? exhibitorId.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
