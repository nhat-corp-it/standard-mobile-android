package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.SPECIAL_EVENT_BRIEF;
import static com.corpit.standard.share.ParameterNames.SPECIAL_EVENT_DATE;
import static com.corpit.standard.share.ParameterNames.SPECIAL_EVENT_ID;
import static com.corpit.standard.share.ParameterNames.SPECIAL_EVENT_IMAGE;
import static com.corpit.standard.share.ParameterNames.SPECIAL_EVENT_TIME;
import static com.corpit.standard.share.ParameterNames.SPECIAL_EVENT_TITLE;
import static com.corpit.standard.share.ParameterNames.SPECIAL_EVENT_VENUE;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class SpecialEvent {

    @Id
    @SerializedName(SPECIAL_EVENT_ID)
    private String id;

    @SerializedName(SPECIAL_EVENT_DATE)
    private String date;

    @SerializedName(SPECIAL_EVENT_TIME)
    private String time;

    @SerializedName(SPECIAL_EVENT_TITLE)
    private String title;

    @SerializedName(SPECIAL_EVENT_VENUE)
    private String venue;

    @SerializedName(SPECIAL_EVENT_BRIEF)
    private String brief;

    @SerializedName(SPECIAL_EVENT_IMAGE)
    private String image;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 168775867)
    public SpecialEvent(String id, String date, String time, String title,
            String venue, String brief, String image, String deleteFlag,
            String updatedDate) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.title = title;
        this.venue = venue;
        this.brief = brief;
        this.image = image;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 515866224)
    public SpecialEvent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
