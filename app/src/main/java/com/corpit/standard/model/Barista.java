package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.corpit.standard.data.remote.WithImage;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.SPEAKER_ADDRESS;
import static com.corpit.standard.share.ParameterNames.SPEAKER_BIO;
import static com.corpit.standard.share.ParameterNames.SPEAKER_COMPANY;
import static com.corpit.standard.share.ParameterNames.SPEAKER_COUNTRY;
import static com.corpit.standard.share.ParameterNames.SPEAKER_EMAIL;
import static com.corpit.standard.share.ParameterNames.SPEAKER_FULL_NAME;
import static com.corpit.standard.share.ParameterNames.SPEAKER_ID;
import static com.corpit.standard.share.ParameterNames.SPEAKER_JOB;
import static com.corpit.standard.share.ParameterNames.SPEAKER_MOBILE;
import static com.corpit.standard.share.ParameterNames.SPEAKER_PROFILE_PIC;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

@Entity
public class Barista implements WithImage, Comparable<Barista> {

    @Id
    @SerializedName(SPEAKER_ID)
    private String id;

    @SerializedName(SPEAKER_FULL_NAME)
    private String fullName;

    @SerializedName(SPEAKER_JOB)
    private String job;

    @SerializedName(SPEAKER_COMPANY)
    private String company;

    @SerializedName(SPEAKER_EMAIL)
    private String email;

    @SerializedName(SPEAKER_MOBILE)
    private String mobile;

    @SerializedName(SPEAKER_ADDRESS)
    private String address;

    @SerializedName(SPEAKER_COUNTRY)
    private String country;

    @SerializedName(SPEAKER_BIO)
    private String bio;

    @SerializedName(SPEAKER_PROFILE_PIC)
    private String profilePic;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1025581969)
    public Barista(String id, String fullName, String job, String company,
            String email, String mobile, String address, String country, String bio,
            String profilePic, String deleteFlag, String updatedDate) {
        this.id = id;
        this.fullName = fullName;
        this.job = job;
        this.company = company;
        this.email = email;
        this.mobile = mobile;
        this.address = address;
        this.country = country;
        this.bio = bio;
        this.profilePic = profilePic;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1790218245)
    public Barista() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJob() {
        return this.job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBio() {
        return this.bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getProfilePic() {
        return this.profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getDeleteFlag() {
        return this.deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return this.updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getSectionName() {
        String s = fullName.substring(0, 1).toUpperCase();

        return s.matches("[A-Za-z]")
                ? s
                : "#";
    }

    @Override
    public String getUrl() {
        return profilePic;
    }

    @Override
    public int compareTo(@NonNull Barista barista) {
        return fullName.toUpperCase().compareTo(barista.fullName.toUpperCase());
    }
}
