package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.NEWSLETTER_COVER;
import static com.corpit.standard.share.ParameterNames.NEWSLETTER_DOWNLOAD_LINK;
import static com.corpit.standard.share.ParameterNames.NEWSLETTER_ID;
import static com.corpit.standard.share.ParameterNames.NEWSLETTER_ISSUE;
import static com.corpit.standard.share.ParameterNames.NEWSLETTER_ISSUE_DATE;
import static com.corpit.standard.share.ParameterNames.NEWSLETTER_TITLE;
import static com.corpit.standard.share.ParameterNames.NEWSLETTER_TYPE_ID;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Newsletter {

    @Id
    @SerializedName(NEWSLETTER_ID)
    private String id;

    @SerializedName(NEWSLETTER_TITLE)
    private String title;

    @SerializedName(NEWSLETTER_ISSUE)
    private String issue;

    @SerializedName(NEWSLETTER_ISSUE_DATE)
    private String issueDate;

    @SerializedName(NEWSLETTER_COVER)
    private String cover;

    @SerializedName(NEWSLETTER_DOWNLOAD_LINK)
    private String downloadLink;

    @SerializedName(NEWSLETTER_TYPE_ID)
    private String newsletterTypeId;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 300080004)
    public Newsletter(String id, String title, String issue, String issueDate,
            String cover, String downloadLink, String newsletterTypeId,
            String deleteFlag, String updatedDate) {
        this.id = id;
        this.title = title;
        this.issue = issue;
        this.issueDate = issueDate;
        this.cover = cover;
        this.downloadLink = downloadLink;
        this.newsletterTypeId = newsletterTypeId;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 1883329478)
    public Newsletter() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public String getNewsletterTypeId() {
        return newsletterTypeId;
    }

    public void setNewsletterTypeId(String newsletterTypeId) {
        this.newsletterTypeId = newsletterTypeId;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
