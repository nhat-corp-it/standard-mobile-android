package com.corpit.standard.model;

import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

import com.corpit.standard.data.remote.WithImage;
import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.corpit.standard.util.DateConverter;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.util.Calendar;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.EVENT_BRIEF;
import static com.corpit.standard.share.ParameterNames.EVENT_CAT;
import static com.corpit.standard.share.ParameterNames.EVENT_DATE;
import static com.corpit.standard.share.ParameterNames.EVENT_ID;
import static com.corpit.standard.share.ParameterNames.EVENT_IMAGE;
import static com.corpit.standard.share.ParameterNames.EVENT_TIME;
import static com.corpit.standard.share.ParameterNames.EVENT_TITLE;
import static com.corpit.standard.share.ParameterNames.EVENT_VENUE;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Event implements ListItem<Event>, WithImage {

    @Id
    @SerializedName(EVENT_ID)
    private String id;

    @SerializedName(EVENT_IMAGE)
    private String image;

    @SerializedName(EVENT_DATE)
    private String date;

    @SerializedName(EVENT_TIME)
    private String time;

    @SerializedName(EVENT_TITLE)
    private String title;

    @SerializedName(EVENT_CAT)
    private String category;

    @SerializedName(EVENT_VENUE)
    private String boothId;

    @SerializedName(EVENT_BRIEF)
    private String brief;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 816396123)
    public Event(String id, String image, String date, String time, String title, String category, String boothId,
                 String brief, String deleteFlag, String updatedDate) {
        this.id = id;
        this.image = image;
        this.date = date;
        this.time = time;
        this.title = title;
        this.category = category;
        this.boothId = boothId;
        this.brief = brief;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 344677835)
    public Event() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBoothId() {
        return boothId;
    }

    public void setBoothId(String boothId) {
        this.boothId = boothId;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    private String getTimeToCalculate() {
        return time.trim().replace(" ", "")
                .replace(":", "")
                .replace("-", "");
    }

    public Calendar getCalendarStartTime() {
        String realTime = getTimeToCalculate();
        String startTime = "0000";
        if (!TextUtils.isEmpty(realTime) && realTime.length() == 8) {
            startTime = realTime.substring(0, 4);
        }

        return DateConverter.toCalendar(startTime, date);
    }

    public Calendar getCalendarEndTime() {
        String realTime = getTimeToCalculate();
        String endTime = "0000";
        if (!TextUtils.isEmpty(realTime) && realTime.length() == 8) {
            endTime = realTime.substring(4, 8);
        }

        return DateConverter.toCalendar(endTime, date);
    }

    public String getFormattedStartTime() {
        if (time.isEmpty() || time.length() < 9) return "";

        return time.substring(0, 2) + ":" + time.substring(2, 4);
    }

    public String getFormattedEndTime() {
        if (time.isEmpty() || time.length() < 9) return "";

        return time.substring(5, 7) + ":" + time.substring(7, 9);
    }

    public Calendar getDateObject() {
        return DateConverter.toCalendar(date);
    }

    public Spanned getBriefToDisplay() {
        StringBuilder html = new StringBuilder();
        html.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
        html.append("</head><body>");
        if (brief != null && !brief.isEmpty()) {
            brief = brief.replaceAll(" width: [0-9]*px;", "");
            brief = brief.replaceAll("font-family:[^;]*;", "");
            brief = brief.replaceAll("14px", "18px");
            brief = brief.replaceAll("13px", "18px");
            brief = brief.replaceAll("12px", "18px");
            brief = brief.replaceAll("background-color:[^;]*;", "");
        }
        html.append(brief);
        html.append("</body>");
        html.append("</html>");

        return Html.fromHtml(html.toString());
    }

    @Override
    public int compareTo(@NonNull Event e) {
        if (!getDateObject().equals(e.getDateObject())) {
            return getDateObject().compareTo(e.getDateObject());
        } else if (!getCalendarStartTime().equals(e.getCalendarStartTime())) {
            return getCalendarStartTime().compareTo(e.getCalendarStartTime());
        } else {
            return getCalendarEndTime().compareTo(e.getCalendarEndTime());
        }
    }

    @Override
    public String getUrl() {
        return image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (id != null ? !id.equals(event.id) : event.id != null) return false;
        if (image != null ? !image.equals(event.image) : event.image != null) return false;
        if (date != null ? !date.equals(event.date) : event.date != null) return false;
        if (time != null ? !time.equals(event.time) : event.time != null) return false;
        if (title != null ? !title.equals(event.title) : event.title != null) return false;
        if (category != null ? !category.equals(event.category) : event.category != null)
            return false;
        if (boothId != null ? !boothId.equals(event.boothId) : event.boothId != null) return false;
        if (brief != null ? !brief.equals(event.brief) : event.brief != null) return false;
        if (deleteFlag != null ? !deleteFlag.equals(event.deleteFlag) : event.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(event.updatedDate) : event.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (boothId != null ? boothId.hashCode() : 0);
        result = 31 * result + (brief != null ? brief.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
