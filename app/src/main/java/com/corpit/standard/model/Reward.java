package com.corpit.standard.model;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.REWARD_CONTENT;
import static com.corpit.standard.share.ParameterNames.REWARD_CONTENT_OTHER;
import static com.corpit.standard.share.ParameterNames.REWARD_ID;
import static com.corpit.standard.share.ParameterNames.REWARD_IMAGE;
import static com.corpit.standard.share.ParameterNames.REWARD_IMAGE_OTHER;
import static com.corpit.standard.share.ParameterNames.REWARD_PASSWORD;
import static com.corpit.standard.share.ParameterNames.REWARD_TITLE;
import static com.corpit.standard.share.ParameterNames.REWARD_TITLE_OTHER;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Reward {

    @Id
    @SerializedName(REWARD_ID)
    private String id;

    @SerializedName(REWARD_TITLE)
    private String title;

    @SerializedName(REWARD_TITLE_OTHER)
    private String otherLangTitle;

    @SerializedName(REWARD_IMAGE)
    private String image;

    @SerializedName(REWARD_IMAGE_OTHER)
    private String otherLangImage;

    @SerializedName(REWARD_PASSWORD)
    private String password;

    @SerializedName(REWARD_CONTENT)
    private String content;

    @SerializedName(REWARD_CONTENT_OTHER)
    private String otherLangContent;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 607657537)
    public Reward(String id, String title, String otherLangTitle, String image,
            String otherLangImage, String password, String content,
            String otherLangContent, String deleteFlag, String updatedDate) {
        this.id = id;
        this.title = title;
        this.otherLangTitle = otherLangTitle;
        this.image = image;
        this.otherLangImage = otherLangImage;
        this.password = password;
        this.content = content;
        this.otherLangContent = otherLangContent;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 896879816)
    public Reward() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOtherLangTitle() {
        return otherLangTitle;
    }

    public void setOtherLangTitle(String otherLangTitle) {
        this.otherLangTitle = otherLangTitle;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOtherLangImage() {
        return otherLangImage;
    }

    public void setOtherLangImage(String otherLangImage) {
        this.otherLangImage = otherLangImage;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOtherLangContent() {
        return otherLangContent;
    }

    public void setOtherLangContent(String otherLangContent) {
        this.otherLangContent = otherLangContent;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
