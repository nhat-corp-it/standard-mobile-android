package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.corpit.standard.util.DateConverter;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.util.Calendar;

import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.INBOX_CONTENT;
import static com.corpit.standard.share.ParameterNames.INBOX_DATE;
import static com.corpit.standard.share.ParameterNames.INBOX_ID;
import static com.corpit.standard.share.ParameterNames.INBOX_IS_PUSH;
import static com.corpit.standard.share.ParameterNames.INBOX_TIME;
import static com.corpit.standard.share.ParameterNames.INBOX_TITLE;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class Inbox implements ListItem<Inbox> {

    @Id
    @SerializedName(INBOX_ID)
    private String id;

    @SerializedName(INBOX_TITLE)
    private String title;

    @SerializedName(INBOX_CONTENT)
    private String content;

    @SerializedName(INBOX_DATE)
    private String date; //(dd/mm/yyyy)

    @SerializedName(INBOX_TIME)
    private String time; //(hh:mm) 24 hour format

    @SerializedName(INBOX_IS_PUSH)
    private String isPush;

    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    @Generated(hash = 1911813898)
    public Inbox(String id, String title, String content, String date, String time,
                 String isPush, String deleteFlag, String updatedDate) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.date = date;
        this.time = time;
        this.isPush = isPush;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 951426812)
    public Inbox() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIsPush() {
        return isPush;
    }

    public void setIsPush(String isPush) {
        this.isPush = isPush;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Calendar getCalendar() {
        return DateConverter.toCalendar(time, date);
    }

    @Override
    public int compareTo(@NonNull Inbox o) {
        return getCalendar().compareTo(o.getCalendar());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inbox inbox = (Inbox) o;

        if (id != null ? !id.equals(inbox.id) : inbox.id != null) return false;
        if (title != null ? !title.equals(inbox.title) : inbox.title != null) return false;
        if (content != null ? !content.equals(inbox.content) : inbox.content != null) return false;
        if (date != null ? !date.equals(inbox.date) : inbox.date != null) return false;
        if (time != null ? !time.equals(inbox.time) : inbox.time != null) return false;
        if (isPush != null ? !isPush.equals(inbox.isPush) : inbox.isPush != null) return false;
        if (deleteFlag != null ? !deleteFlag.equals(inbox.deleteFlag) : inbox.deleteFlag != null)
            return false;
        return updatedDate != null ? updatedDate.equals(inbox.updatedDate) : inbox.updatedDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (isPush != null ? isPush.hashCode() : 0);
        result = 31 * result + (deleteFlag != null ? deleteFlag.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        return result;
    }
}
