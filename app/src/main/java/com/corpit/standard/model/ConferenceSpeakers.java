package com.corpit.standard.model;

import android.support.annotation.NonNull;

import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;

import static com.corpit.standard.share.ParameterNames.CONF_SPEAKER_CONF_CAT;
import static com.corpit.standard.share.ParameterNames.CONF_SPEAKER_CONF_ID;
import static com.corpit.standard.share.ParameterNames.CONF_SPEAKER_ID;
import static com.corpit.standard.share.ParameterNames.CONF_SPEAKER_SPEAKER_CAT;
import static com.corpit.standard.share.ParameterNames.CONF_SPEAKER_SPEAKER_CAT_SEQ;
import static com.corpit.standard.share.ParameterNames.CONF_SPEAKER_SPEAKER_ID;
import static com.corpit.standard.share.ParameterNames.DELETE_FLAG;
import static com.corpit.standard.share.ParameterNames.UPDATED_DATE;

/**
 * Created by nhatton on 23/3/18.
 */

@Entity
public class ConferenceSpeakers implements ListItem<ConferenceSpeakers> {

    @Id
    @Expose
    @SerializedName(CONF_SPEAKER_ID)
    private String id;

    @Expose
    @SerializedName(CONF_SPEAKER_SPEAKER_ID)
    private String speakerId;

    @ToOne(joinProperty = "speakerId")
    private Speaker speaker;

    @Expose
    @SerializedName(CONF_SPEAKER_SPEAKER_CAT)
    private String speakerCategory;

    @Expose
    @SerializedName(CONF_SPEAKER_SPEAKER_CAT_SEQ)
    private String speakerCategorySeq;

    @Expose
    @SerializedName(CONF_SPEAKER_CONF_ID)
    private String conferenceId;

    @ToOne(joinProperty = "conferenceId")
    private Conference conference;

    @Expose
    @SerializedName(CONF_SPEAKER_CONF_CAT)
    private String conferenceCategory;

    @Expose
    @SerializedName(DELETE_FLAG)
    private String deleteFlag;

    @Expose
    @SerializedName(UPDATED_DATE)
    private String updatedDate;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 957712793)
    private transient ConferenceSpeakersDao myDao;

    @Generated(hash = 1754804583)
    public ConferenceSpeakers(String id, String speakerId, String speakerCategory,
                              String speakerCategorySeq, String conferenceId,
                              String conferenceCategory, String deleteFlag, String updatedDate) {
        this.id = id;
        this.speakerId = speakerId;
        this.speakerCategory = speakerCategory;
        this.speakerCategorySeq = speakerCategorySeq;
        this.conferenceId = conferenceId;
        this.conferenceCategory = conferenceCategory;
        this.deleteFlag = deleteFlag;
        this.updatedDate = updatedDate;
    }

    @Generated(hash = 766121286)
    public ConferenceSpeakers() {
    }

    @Generated(hash = 1310970078)
    private transient String speaker__resolvedKey;

    @Generated(hash = 671115196)
    private transient String conference__resolvedKey;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpeakerId() {
        return speakerId;
    }

    public void setSpeakerId(String speakerId) {
        this.speakerId = speakerId;
    }

    public String getSpeakerCategory() {
        return speakerCategory;
    }

    public void setSpeakerCategory(String speakerCategory) {
        this.speakerCategory = speakerCategory;
    }

    public String getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(String conferenceId) {
        this.conferenceId = conferenceId;
    }

    public String getConferenceCategory() {
        return conferenceCategory;
    }

    public void setConferenceCategory(String conferenceCategory) {
        this.conferenceCategory = conferenceCategory;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public int compareTo(@NonNull ConferenceSpeakers o) {
        int seq1 = Integer.parseInt(speakerCategorySeq);
        int seq2 = Integer.parseInt(o.speakerCategorySeq);

        if (seq1 != seq2) {
            return seq1 - seq2;
        } else if (!conferenceCategory.equals(o.conferenceCategory)) {
            return conferenceCategory.compareTo(o.conferenceCategory);
        } else if (!conferenceId.equals(o.conferenceId)) {
            return conferenceId.compareTo(o.conferenceId);
        } else if (!speakerCategory.equals(o.speakerCategory)) {
            return speakerCategory.compareTo(o.speakerCategory);
        } else {
            return speakerId.compareTo(o.speakerId);
        }
    }

    public String getSpeakerCategorySeq() {
        return this.speakerCategorySeq;
    }

    public void setSpeakerCategorySeq(String speakerCategorySeq) {
        this.speakerCategorySeq = speakerCategorySeq;
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1048524662)
    public Speaker getSpeaker() {
        String __key = this.speakerId;
        if (speaker__resolvedKey == null || speaker__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            SpeakerDao targetDao = daoSession.getSpeakerDao();
            Speaker speakerNew = targetDao.load(__key);
            synchronized (this) {
                speaker = speakerNew;
                speaker__resolvedKey = __key;
            }
        }
        return speaker;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1833418296)
    public void setSpeaker(Speaker speaker) {
        synchronized (this) {
            this.speaker = speaker;
            speakerId = speaker == null ? null : speaker.getId();
            speaker__resolvedKey = speakerId;
        }
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1549026057)
    public Conference getConference() {
        String __key = this.conferenceId;
        if (conference__resolvedKey == null || conference__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ConferenceDao targetDao = daoSession.getConferenceDao();
            Conference conferenceNew = targetDao.load(__key);
            synchronized (this) {
                conference = conferenceNew;
                conference__resolvedKey = __key;
            }
        }
        return conference;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 119012166)
    public void setConference(Conference conference) {
        synchronized (this) {
            this.conference = conference;
            conferenceId = conference == null ? null : conference.getId();
            conference__resolvedKey = conferenceId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1824395467)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getConferenceSpeakersDao() : null;
    }
}
