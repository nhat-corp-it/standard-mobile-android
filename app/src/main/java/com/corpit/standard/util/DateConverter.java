package com.corpit.standard.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static java.lang.System.currentTimeMillis;

public final class DateConverter {

    private static final DateFormat dateFormatter = new SimpleDateFormat("d MMM", Locale.UK);
    private static final DateFormat oldDateFormatter = new SimpleDateFormat("dd/mm/yy", Locale.UK);

    private static final long secondsInMillis = 1000;
    private static final long minutesInMillis = secondsInMillis * 60;
    private static final long hoursInMillis = minutesInMillis * 60;
    private static final long daysInMillis = hoursInMillis * 24;
    private static final long weeksInMillis = daysInMillis * 7;

    private DateConverter() {
    }

    public static Calendar toCalendar(String date) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFormatter.parse(date));
            return cal;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Calendar toCalendar(String time, String date) {
        time = time.replace(":", "");
        time = time.replace(" ", "");
        try {
            int hour = Integer.parseInt(time.substring(0, 2));
            int min = Integer.parseInt(time.substring(2, 4));
            Calendar cal = Calendar.getInstance();
            if (date.length() == 10) {
                cal.setTime(oldDateFormatter.parse(date));
            } else {
                cal.setTime(dateFormatter.parse(date));
            }
            cal.set(Calendar.YEAR, 2018);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, min);

            return cal;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String formatTimeElapsed(String updatedDate, long currentTime) {
        long timeMillis = (long) Float.parseFloat(updatedDate);
        long differentInMillis = (currentTime - timeMillis) * 1000;

        long elapsedWeeks = differentInMillis / weeksInMillis;
        differentInMillis = differentInMillis % weeksInMillis;

        long elapsedDays = differentInMillis / daysInMillis;
        differentInMillis = differentInMillis % daysInMillis;

        long elapsedHours = differentInMillis / hoursInMillis;
        differentInMillis = differentInMillis % hoursInMillis;

        long elapsedMinutes = differentInMillis / minutesInMillis;
        differentInMillis = differentInMillis % minutesInMillis;

        long elapsedSeconds = differentInMillis / secondsInMillis;

        if (elapsedWeeks > 4) {
            Date date = new Date(timeMillis * 1000);
            return dateFormatter.format(date);
        } else if (elapsedWeeks > 0) {
            String week = elapsedWeeks > 1 ? " weeks" : " week";
            return elapsedWeeks + week;
        } else if (elapsedDays > 0) {
            String day = elapsedDays > 1 ? " days" : " day";
            return elapsedDays + day;
        } else if (elapsedHours > 0) {
            String hour = elapsedHours > 1 ? " hours" : " hour";
            return elapsedHours + hour;
        } else if (elapsedMinutes > 0) {
            String min = elapsedMinutes > 1 ? " mins" : " min";
            return elapsedMinutes + min;
        } else {
            String second = elapsedSeconds > 1 ? " seconds" : " second";
            return elapsedSeconds + second;
        }
    }

    public static String formatTimeElapsed(String updatedDate) {
        return formatTimeElapsed(updatedDate, currentTimeMillis() / 1000);
    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentTimeMillis());
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);

        return day + " " + month;
    }

}
