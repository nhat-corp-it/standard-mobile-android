package com.corpit.standard.util;

import android.content.Context;
import android.support.annotation.RawRes;

import com.corpit.standard.data.remote.RetrofitGenericResponse;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.model.Menu;
import com.corpit.standard.model.Menu.MenuType;
import com.corpit.standard.model.Menu.PageType;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class BootstrapHelper {

    private BootstrapHelper() {
    }

    public static <T> void addBootstrap(Context context, DaoSession daoSession, Class<T> cls, @RawRes int resLocation) {
        try {
            String json = JSONHandler.parseResource(context, resLocation);

            Gson gson = new Gson();

            RetrofitGenericResponse response = gson.fromJson(json, RetrofitGenericResponse.class);

            JsonArray array = response.getData();

            ArrayList<T> list = new ArrayList<>();
            for (JsonElement element : array) {
                T item = gson.fromJson(element, cls);
                list.add(item);
            }

            daoSession.startAsyncSession().insertInTx(cls, list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Menu> debugMenus() {
        ArrayList<Menu> menus = new ArrayList<>();

        Menu newsMenu = new Menu();
        newsMenu.setId("MU10090");
        newsMenu.setEnTitle("News");
        newsMenu.setMenuType(MenuType.SIDE_MENU);
        newsMenu.setPageType(PageType.NEWS);
        newsMenu.setPermission("VIP/Exhibitor/Delegate/Visitor/Guest");
        newsMenu.setIconUrl("MU10026");
        newsMenu.setOrderId("12");
        newsMenu.setParentId("");

        menus.add(newsMenu);

        Menu inboxMenu = new Menu();
        inboxMenu.setId("MU10091");
        inboxMenu.setEnTitle("Inbox");
        inboxMenu.setMenuType(MenuType.SIDE_MENU);
        inboxMenu.setPageType(PageType.INBOX);
        inboxMenu.setPermission("VIP/Exhibitor/Delegate/Visitor/Guest");
        inboxMenu.setIconUrl("MU10091");
        inboxMenu.setOrderId("13");
        inboxMenu.setParentId("");

        menus.add(inboxMenu);

        Menu pollingMenu = new Menu();
        pollingMenu.setId("MU10092");
        pollingMenu.setEnTitle("Live Polling");
        pollingMenu.setMenuType(MenuType.SIDE_MENU);
        pollingMenu.setPageType(PageType.POLLING);
        pollingMenu.setPermission("VIP/Exhibitor/Delegate/Visitor/Guest");
        pollingMenu.setIconUrl("MU10092");
        pollingMenu.setOrderId("14");
        pollingMenu.setParentId("");

        menus.add(pollingMenu);

        Menu liveQAMenu = new Menu();
        liveQAMenu.setId("MU10093");
        liveQAMenu.setEnTitle("Live QA");
        liveQAMenu.setMenuType(MenuType.SIDE_MENU);
        liveQAMenu.setPageType(PageType.LIVE_QA);
        liveQAMenu.setPermission("VIP/Exhibitor/Delegate/Visitor/Guest");
        liveQAMenu.setIconUrl("MU10093");
        liveQAMenu.setOrderId("15");
        liveQAMenu.setParentId("");

        menus.add(liveQAMenu);

        Menu ePosterMenu = new Menu();
        ePosterMenu.setId("MU10094");
        ePosterMenu.setEnTitle("E-poster");
        ePosterMenu.setMenuType(MenuType.SIDE_MENU);
        ePosterMenu.setPageType(PageType.E_POSTER);
        ePosterMenu.setPermission("VIP/Exhibitor/Delegate/Visitor/Guest");
        ePosterMenu.setIconUrl("MU10094");
        ePosterMenu.setOrderId("16");
        ePosterMenu.setParentId("");

        menus.add(ePosterMenu);

        Menu logoutMenu = new Menu();
        logoutMenu.setId("MU10099");
        logoutMenu.setEnTitle("Logout");
        logoutMenu.setMenuType(MenuType.SIDE_MENU);
        logoutMenu.setPageType(PageType.LOGOUT);
        logoutMenu.setPermission("VIP/Exhibitor/Delegate/Visitor/Guest");
        logoutMenu.setIconUrl("MU10028");
        logoutMenu.setOrderId("99");
        logoutMenu.setParentId("");

        menus.add(logoutMenu);

        return menus;
    }
}
