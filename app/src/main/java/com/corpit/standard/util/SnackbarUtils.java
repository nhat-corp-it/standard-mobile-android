package com.corpit.standard.util;

import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import java.text.MessageFormat;

public final class SnackbarUtils {

    private SnackbarUtils() {
    }

    public static void showSnackbar(View v, String snackbarText) {
        if (v == null || TextUtils.isEmpty(snackbarText)) {
            return;
        }
        String text = MessageFormat.format("<font color=\"#ffffff\">{0}</font>", snackbarText);
        Snackbar.make(v, Html.fromHtml(text), Snackbar.LENGTH_SHORT)
                .setActionTextColor(v.getResources().getColor(android.R.color.white))
                .show();
    }
}
