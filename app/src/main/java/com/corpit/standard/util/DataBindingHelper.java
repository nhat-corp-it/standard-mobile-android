package com.corpit.standard.util;

import android.databinding.BindingAdapter;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.corpit.standard.R;
import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

/**
 * Helper class to use custom attribute in layout
 */
public final class DataBindingHelper {

    private DataBindingHelper() {
    }

    @BindingAdapter("imageUrl")
    public static void imageUrl(ImageView imageView, String url) {
        Glide.with(imageView)
                .load(url)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageView);
    }

    @BindingAdapter("iconUrl")
    public static void iconUrl(ImageView imageView, String url) {
        Glide.with(imageView)
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_menu_item_default)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageView);
    }

    @BindingAdapter("avatarUrl")
    public static void avatarUrl(ImageView imageView, String url) {
        Glide.with(imageView)
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.speaker_placeholder)
                        .centerCrop().circleCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageView);
    }

    @BindingAdapter("displayTime")
    public static void displayTime(TextView textView, String updatedDate) {
        textView.setText(DateConverter.formatTimeElapsed(updatedDate));
    }

    @BindingAdapter("displayHtml")
    public static void displayHtml(TextView textView, String content) {
        StringBuilder html = new StringBuilder();
        html.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />")
                .append("</head><body>");
        if (content != null && !content.isEmpty()) {
            content = content.replaceAll(" width: [0-9]*px;", "")
                    .replaceAll("font-family:[^;]*;", "")
                    .replaceAll("14px", "18px")
                    .replaceAll("13px", "18px")
                    .replaceAll("12px", "18px")
                    .replaceAll("background-color:[^;]*;", "");
        }
        html.append(content)
                .append("</body>")
                .append("</html>");

        textView.setText(Html.fromHtml(html.toString()));
    }

    @BindingAdapter("isVisible")
    public static void setVisible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter("list")
    public static <T extends ListItem<T>> void setList(RecyclerView listView, List<T> list) {
        StandardBoundAdapter<T, ViewDataBinding> adapter = (StandardBoundAdapter<T, ViewDataBinding>) listView.getAdapter();
        if (adapter != null) {
            adapter.setList(list);
        }
    }
}
