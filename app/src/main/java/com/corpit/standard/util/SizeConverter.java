package com.corpit.standard.util;

import android.content.Context;
import android.util.TypedValue;

public final class SizeConverter {

    private SizeConverter() {
    }

    public static int dipToPixel(int dip, Context context) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dip, context.getResources().getDisplayMetrics());
    }
}
