package com.corpit.standard.util;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.model.Conference;
import com.corpit.standard.model.Event;

import static android.Manifest.permission.WRITE_CALENDAR;

public final class CalendarProvider {

    private CalendarProvider() {
    }

    public static final int MY_CURRENT_REQUEST_WRITE_CALENDAR = 20;

    public static void addEvent(Event event, String location, Context context) {

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event.getCalendarStartTime().getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, event.getCalendarEndTime().getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, event.getTitle())
                .putExtra(CalendarContract.Events.DESCRIPTION, event.getBriefToDisplay())
                .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        context.startActivity(intent);
    }

    public static void addConference(Conference conference, String location, Context context) {

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, conference.getCalendarStartTime().getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, conference.getCalendarEndTime().getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, conference.getTitle())
                .putExtra(CalendarContract.Events.DESCRIPTION, conference.getBriefToDisplay())
                .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        context.startActivity(intent);
    }

    public static long addConferenceToCalendar(Activity activity, Conference conference, String location) {
        long calID = 3;

        ContentResolver cr = activity.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, conference.getCalendarStartTime().getTimeInMillis());
        values.put(CalendarContract.Events.DTEND, conference.getCalendarEndTime().getTimeInMillis());
        values.put(CalendarContract.Events.TITLE, conference.getTitle());
        values.put(CalendarContract.Events.DESCRIPTION, conference.getBriefToDisplay().toString());
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_LOCATION, location);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, BuildConfig.EVENT_TIMEZONE);
        values.put(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{WRITE_CALENDAR},
                    MY_CURRENT_REQUEST_WRITE_CALENDAR);
            return -1;
        }
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        // get the event ID that is the last element in the Uri
        if (uri != null) {
            return Long.parseLong(uri.getLastPathSegment());
        }
        return -1;
    }

    public static int deleteConferenceFromCalendar(Context context, long eventId) {
        ContentResolver cr = context.getContentResolver();
        Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventId);
        return cr.delete(deleteUri, null, null);
    }
}
