package com.corpit.standard.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.content.res.AppCompatResources;

public final class VectorUtil {

    private VectorUtil() {
    }

    @Nullable
    public static Drawable getVector(@DrawableRes int vectorRes, Context context) {
        return AppCompatResources.getDrawable(context, vectorRes);
    }
}
