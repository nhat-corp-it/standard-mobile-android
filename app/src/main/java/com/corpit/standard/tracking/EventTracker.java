package com.corpit.standard.tracking;

import com.corpit.standard.model.Conference;
import com.corpit.standard.model.Event;
import com.corpit.standard.model.ExhibitingCompany;
import com.corpit.standard.model.GeneralInfo;
import com.corpit.standard.model.Location;
import com.corpit.standard.model.Menu;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.model.Sponsor;

public interface EventTracker {
    String SPEAKER_EVENT_TYPE = "Speaker";
    String SPEAKER_NAME = "Name";
    String SPEAKER_ID = "ID";

    String SPONSOR_EVENT_TYPE = "Sponsor";
    String SPONSOR_ATTR_NAME = "Name";
    String SPONSOR_ATTR_ID = "ID";

    String CONFERENCE_EVENT_TYPE = "Conference";
    String CONFERENCE_TITLE = "FullTitle";
    String CONFERENCE_ID = "ID";

    String EVENT_EVENT_TYPE = "EventSchedule";
    String EVENT_TITLE = "FullTitle";
    String EVENT_ID = "ID";

    String EXHIBITOR_EVENT_TYPE = "Exhibitor";
    String EXHIBITOR_COMPANY_NAME = "CompanyName";
    String EXHIBITOR_HALL = "Hall";
    String EXHIBITOR_BOOTH = "Booth";
    String EXHIBITOR_ID = "ID";

    String FLOORPLAN_EVENT_TYPE = "FloorPlan";
    String FLOORPLAN_HALL_NAME = "HallName";
    String FLOORPLAN_ID = "ID";

    String INFO_EVENT_TYPE = "GeneralInformation";
    String INFO_TITLE = "Title";
    String INFO_ID = "ID";

    String MENU_EVENT_TYPE = "Menu";
    String MENU_TITLE = "Title";
    String MENU_ID = "ID";

    void trackSpeaker(Speaker speaker);

    void trackSponsor(Sponsor sponsor);

    void trackConference(Conference conference);

    void trackEventSchedule(Event event);

    void trackExhibitor(ExhibitingCompany exhibitor);

    void trackFloorplan(Location floorplan);

    void trackGeneralInformation(GeneralInfo info);

    void trackMenuClick(Menu menu);

    void onPageStart(String tag);

    void onPageEnd(String tag);
}
