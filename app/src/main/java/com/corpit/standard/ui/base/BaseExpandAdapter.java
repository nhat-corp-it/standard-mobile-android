package com.corpit.standard.ui.base;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.widget.AnimatedExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;

public class BaseExpandAdapter<T extends Expandable> extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

    private Context mContext;
    private ItemClickListener<T> mListener;
    private ArrayList<String> groups;
    private HashMap<String, ArrayList<T>> children;
    private ColorScheme colorScheme;

    public BaseExpandAdapter(Context context, ArrayList<String> groups, HashMap<String,
            ArrayList<T>> children, ItemClickListener<T> listener) {
        this.mContext = context;
        this.mListener = listener;
        this.groups = groups;
        this.children = children;
        this.colorScheme = Injector.get().colorScheme();
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        String parentName = groups.get(groupPosition);
        ArrayList<T> child = children.get(parentName);
        return child == null ? 0 : child.size();
    }

    @Override
    public String getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public T getChild(int groupPosition, int childPosition) {
        String parentName = groups.get(groupPosition);
        ArrayList<T> childList = children.get(parentName);
        return childList.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return getGroup(groupPosition).hashCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).hashCode();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_sub_category, parent, false);
            holder = new ViewHolder();
            holder.mText = convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        GradientDrawable background = (GradientDrawable) convertView.getBackground();
        background.setColor(colorScheme.getColorButton1Code());

        holder.mText.setText(getGroup(groupPosition));

        return convertView;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_simple_item, parent, false);
            holder = new ViewHolder();
            holder.mText = convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final T item = getChild(groupPosition, childPosition);
        holder.mText.setText(item.getText());
        holder.mText.setTextColor(colorScheme.getColorFont1Code());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(item);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolder {
        TextView mText;
    }
}
