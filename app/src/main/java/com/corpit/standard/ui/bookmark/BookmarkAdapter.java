package com.corpit.standard.ui.bookmark;

import android.databinding.ObservableBoolean;

import com.corpit.standard.databinding.RowBookmarkBinding;
import com.corpit.standard.model.Bookmark;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

public class BookmarkAdapter extends StandardBoundAdapter<Bookmark, RowBookmarkBinding> {
    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId The layout to be used for items. It must use data binding.
     * @param listener Basic item click listener
     */
    BookmarkAdapter(int layoutId, ItemClickListener<Bookmark> listener) {
        super(layoutId, listener);
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowBookmarkBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);

        ObservableBoolean hideCategory = new ObservableBoolean();
        hideCategory.set(holder.getAdapterPosition() > 0
                && getList().get(holder.getAdapterPosition() - 1).getBookmarkType()
                .equals(getList().get(holder.getAdapterPosition()).getBookmarkType()));
        holder.binding.setHideCategory(hideCategory);

    }
}
