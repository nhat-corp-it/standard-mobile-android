package com.corpit.standard.ui.floorplans;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Booth;
import com.corpit.standard.model.BoothDao;
import com.corpit.standard.model.ExhibitingCompany;
import com.corpit.standard.model.ExhibitingCompanyDao;
import com.corpit.standard.model.Location;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.exhibitors.ExhibitorFragment;
import com.corpit.standard.ui.exhibitors.ExhibitorListFragment;
import com.corpit.standard.util.VectorUtil;
import com.corpit.standard.widget.PinView;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link IndividualHallFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IndividualHallFragment extends BaseFragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String locationId;
    private String boothId;

    private PinView mapView;

    private Booth selectedBooth;

    private ArrayList<Booth> booths;

    private PointF sCoord = new PointF();
    private Location floorplan;

    public IndividualHallFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param locationId Parameter 1.
     * @param boothId    Parameter 2.
     * @return A new instance of fragment IndividualHallFragment.
     */
    public static IndividualHallFragment newInstance(String locationId, String boothId) {
        IndividualHallFragment fragment = new IndividualHallFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, locationId);
        args.putString(ARG_PARAM2, boothId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            locationId = getArguments().getString(ARG_PARAM1);
            boothId = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_individual_hall, container, false);

        sCoord.x = 0;
        sCoord.y = 0;

        floorplan = daoSession.getLocationDao().load(locationId);

        booths = (ArrayList<Booth>) daoSession.getBoothDao().queryBuilder()
                .where(BoothDao.Properties.LocationId.eq(locationId)).list();

        TextView title = root.findViewById(R.id.title);
        title.setText(floorplan.getName());

        title.setTextColor(colorScheme.getColorFont1Code());

        selectedBooth = daoSession.getBoothDao().load(boothId);

        initMapView(root);

        Button searchButton = root.findViewById(R.id.search_button);

        ViewCompat.setBackgroundTintList(searchButton,
                ColorStateList.valueOf(colorScheme.getColorButton1Code()));
        Drawable drawable = VectorUtil.getVector(R.drawable.ic_search_white, getContext());
        searchButton.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(ExhibitorListFragment.newInstance(ExhibitorListFragment.FILTER_BY_HALL,
                        "Overview"));
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Injector.get().eventTracker().trackFloorplan(floorplan);
    }

    private void initMapView(View root) {
        mapView = root.findViewById(R.id.hall_map);
        mapView.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_START);

        String url = floorplan.getUrl();
        Glide.with(getActivity()).downloadOnly().listener(new RequestListener<File>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                        Target<File> target, boolean isFirstResource) {
                if (e != null) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onResourceReady(File file, Object model, Target<File> target,
                                           DataSource dataSource, boolean isFirstResource) {
                String uri = file.getPath();
                mapView.setImage(ImageSource.uri(uri));
                if (selectedBooth != null) {
                    try {
                        float sourceX = Float.parseFloat(selectedBooth.getX());
                        float sourceY = Float.parseFloat(selectedBooth.getY());
                        float boothWidth = Float.parseFloat(selectedBooth.getWidth());
                        float boothHeight = Float.parseFloat(selectedBooth.getHeight());

                        mapView.setScaleAndCenter(1,
                                new PointF(sourceX + boothWidth / 2, sourceY + boothHeight / 2));

                        mapView.setPin(sourceX + boothWidth / 2, sourceY + boothHeight / 2);

//                        mapView.drawSelectedBooth(selectedBooth);


                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                }
                return true;
            }
        }).load(url).submit();

        mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mapView.isReady()) return;

                for (Booth booth : booths) {
                    try {
                        float boothX = Float.parseFloat(booth.getX());
                        float boothY = Float.parseFloat(booth.getY());
                        float boothWidth = Float.parseFloat(booth.getWidth());
                        float boothHeight = Float.parseFloat(booth.getHeight());

                        if (sCoord.x >= boothX
                                && sCoord.x <= (boothX + boothWidth)
                                && sCoord.y >= boothY
                                && sCoord.y <= (boothY + boothHeight)) {

                            mapView.drawSelectedBooth(booth);
                            displayWindow(booth);

                            break;
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        final GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (mapView.isReady()) {
                    sCoord = mapView.viewToSourceCoord(e.getX(), e.getY());
                }
                return true;
            }
        });

        mapView.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

    }

    private void displayWindow(final Booth booth) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        final List<ExhibitingCompany> list = daoSession.getExhibitingCompanyDao().queryBuilder()
                .where(ExhibitingCompanyDao.Properties.BoothId.eq(booth.getId())).list();

        String exhibitorName = "";
        if (!list.isEmpty()) {
            exhibitorName = list.get(0).getName();
        }

        builder.setTitle("Exhibitor")
                .setMessage(exhibitorName)
                .setPositiveButton(R.string.btn_go_to, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (!list.isEmpty()) {
                            gotoFragment(ExhibitorFragment.newInstance(
                                    list.get(0).getId(), false));
                        }

                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create()
                .show();

    }

}
