package com.corpit.standard.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentExhibitorLoginBinding;
import com.corpit.standard.ui.base.BaseFragment;

public class ExhibitorLoginFragment extends BaseFragment {

    private EditText userIdInput;
    private EditText passwordInput;

    public ExhibitorLoginFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        FragmentExhibitorLoginBinding binding = FragmentExhibitorLoginBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        userIdInput = binding.userIdInput;
        passwordInput = binding.passwordInput;

        binding.exhibitorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    Toast.makeText(getContext(), "Ok", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.visitorLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VisitorLoginFragment fragment = new VisitorLoginFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out,
                                R.anim.push_right_in, R.anim.push_right_out)
                        .replace(R.id.fragment_container, fragment, fragment.getClass().toString())
                        .addToBackStack(ExhibitorLoginFragment.class.toString())
                        .commit();
            }
        });

        return binding.getRoot();
    }

    private boolean isValid() {
        userIdInput.setError(null);
        passwordInput.setError(null);

        String id = userIdInput.getText().toString();
        String pass = passwordInput.getText().toString();

        boolean isValid = true;

        if (id.isEmpty()) {
            userIdInput.setError(getString(R.string.error_empty_user_id));
            isValid = false;
        }

        if (pass.isEmpty()) {
            passwordInput.setError(getString(R.string.error_empty_password));
            isValid = false;
        }

        return isValid;
    }
}
