package com.corpit.standard.ui.download;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.corpit.standard.BuildConfig;
import com.corpit.standard.R;
import com.corpit.standard.data.remote.DownloadProgressCallBack;
import com.corpit.standard.data.remote.DownloadTaskManager;
import com.corpit.standard.data.remote.RetrofitResponse;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.ConfigurationImages;
import com.corpit.standard.model.ConfigurationImages.ImageKey;
import com.corpit.standard.model.ConfigurationImagesDao;
import com.corpit.standard.model.DataVersion;
import com.corpit.standard.ui.base.ParentActivity;
import com.corpit.standard.util.NetworkUtil;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.corpit.standard.ui.splash.SplashActivity.KEY_DATABASE_VERSION;
import static com.corpit.standard.ui.splash.SplashActivity.KEY_IS_FIRST_TIME;
import static com.corpit.standard.ui.splash.SplashActivity.KEY_UPDATED_AT;
import static java.lang.System.currentTimeMillis;


public class DownloadActivity extends ParentActivity implements DownloadProgressCallBack {

    private ProgressBar progressBar;
    private TextView progressTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        List<ConfigurationImages> list = daoSession.getConfigurationImagesDao().queryBuilder()
                .where(ConfigurationImagesDao.Properties.Key.eq(ImageKey.LOADING)).list();

        if (!list.isEmpty()) {
            Glide.with(this)
                    .load(list.get(0).getFile())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model,
                                                       Target<Drawable> target, DataSource dataSource,
                                                       boolean isFirstResource) {
                            findViewById(R.id.logo).setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into((ImageView) findViewById(R.id.background));
        }

        progressBar = findViewById(R.id.progress_bar);
        ColorScheme colorScheme = Injector.get().colorScheme();
        LayerDrawable progressDrawable = (LayerDrawable) progressBar.getProgressDrawable();
        ClipDrawable drawable = (ClipDrawable) progressDrawable.findDrawableByLayerId(android.R.id.progress);
        drawable.setColorFilter(colorScheme.getColorMargin1Code(), PorterDuff.Mode.MULTIPLY);

        progressTextView = findViewById(R.id.progress);

        if (NetworkUtil.isNetworkWorking(this)) {
            download();
        } else {
            Toast.makeText(this, R.string.error_no_internet, Toast.LENGTH_SHORT).show();
            SharedPreferences sharedPreferences = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
            boolean isFirstTime = sharedPreferences.getBoolean(KEY_IS_FIRST_TIME, true);
            if (isFirstTime) {
                setResult(RESULT_CANCELED);
                finish();
            } else {
                finishDownload();
            }
        }
    }

    private void download() {
        this.getLifecycle().addObserver(new DownloadTaskManager.DownloadLifecycleObserver());
        Call<RetrofitResponse<DataVersion>> call = Injector.get().apiService().getDataVersions("en");
        call.enqueue(new Callback<RetrofitResponse<DataVersion>>() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onResponse(@NonNull Call<RetrofitResponse<DataVersion>> call,
                                   @NonNull Response<RetrofitResponse<DataVersion>> response) {
                if (response.isSuccessful() && response.body().getStatus().equals("200")) {
                    ArrayList<DataVersion> dataVersions = response.body().getData();

                    Map<String, String> dataVersionMap = new HashMap<>();
                    for (DataVersion dataVersion : dataVersions) {
                        dataVersionMap.put(dataVersion.getPage(), dataVersion.getDataVersion());
                    }

                    DownloadTaskManager.getInstance()
                            .startDownloadTasks(dataVersionMap, DownloadActivity.this, false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<RetrofitResponse<DataVersion>> call,
                                  @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void finishDownload() {
        SharedPreferences sharedPreferences = Injector.get().sharedPreferences();
        sharedPreferences.edit().putInt(KEY_DATABASE_VERSION, BuildConfig.SCHEMA_VERSION).apply();
        sharedPreferences.edit().putBoolean(KEY_IS_FIRST_TIME, false).apply();
        sharedPreferences.edit().putLong(KEY_UPDATED_AT, currentTimeMillis()).apply();

        setResult(RESULT_OK);
        finish();
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onProgressUpdate(int currentProgress) {
        progressBar.setProgress(currentProgress);
        progressTextView.setText(MessageFormat.format("{0}%", currentProgress));
    }

    @Override
    public void onProgressComplete() {
        finishDownload();
    }

    @Override
    public void onError() {
        Toast.makeText(this, R.string.error_download_failed, Toast.LENGTH_SHORT).show();
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_cancel_download)
                .setPositiveButton(R.string.btn_download_cancel_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                })
                .setNegativeButton(R.string.btn_download_cancel_no, null);
        builder.create().show();
    }

}
