package com.corpit.standard.ui.exhibitors;


import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.corpit.standard.databinding.FragmentProductCategoriesBinding;
import com.corpit.standard.model.Product;
import com.corpit.standard.model.ProductDao;
import com.corpit.standard.ui.base.BaseExpandAdapter;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.widget.AnimatedExpandableListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ProductCategoriesFragment extends BaseFragment implements ItemClickListener<Product> {

    private int lastExpandedGroup = -1;

    private AnimatedExpandableListView categoryListView;

    public ProductCategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentProductCategoriesBinding binding = FragmentProductCategoriesBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        categoryListView = binding.productCategoryList;

        categoryListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // We call collapseGroupWithAnimation(int) and
                // expandGroupWithAnimation(int) to animate group
                // expansion/collapse.
                if (categoryListView.isGroupExpanded(groupPosition)) {
                    categoryListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    categoryListView.expandGroupWithAnimation(groupPosition);
                    if (lastExpandedGroup != -1 && groupPosition != lastExpandedGroup) {
                        categoryListView.collapseGroup(lastExpandedGroup);
                    }
                    lastExpandedGroup = groupPosition;
                }

                return true;
            }

        });

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        loadFromDb();
    }

    private void loadFromDb() {
        ArrayList<String> groups = new ArrayList<>();
        Cursor cursor = daoSession.getDatabase()
                .rawQuery("SELECT DISTINCT " + ProductDao.Properties.Category.columnName +
                " FROM " + ProductDao.TABLENAME +
                " WHERE " + ProductDao.Properties.Category.columnName + " != ''", null);
        while (cursor.moveToNext()) {
            groups.add(cursor.getString(0));
        }
        cursor.close();

        HashMap<String, ArrayList<Product>> children = new HashMap<>();
        for (String category : groups) {
            ArrayList<Product> list = (ArrayList<Product>) daoSession.getProductDao().queryBuilder()
                    .where(ProductDao.Properties.Category.eq(category)).list();
            Collections.sort(list);
            children.put(category, list);
        }

        BaseExpandAdapter<Product> adapter = new BaseExpandAdapter<>(getContext(), groups, children, this);

        categoryListView.setAdapter(adapter);
    }

    @Override
    public void onClick(Product item) {
        gotoFragment(ProductListFragment.newInstance(item.getId()));
    }
}
