package com.corpit.standard.ui.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.share.UserManager;
import com.corpit.standard.ui.home.MainActivity;
import com.corpit.standard.ui.splash.SplashActivity;

import java.util.Locale;

/**
 * Created by nhatton on 24/3/18.
 */

public abstract class ParentActivity extends AppCompatActivity {

    protected String TAG = this.getClass().getSimpleName();
    protected DaoSession daoSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        daoSession = Injector.get().daoSession();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    @Override
    public void finish() {
        super.finish();
        if (!(this instanceof SplashActivity) && !(this instanceof MainActivity)) {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }
    }

    private Context updateBaseContextLocale(Context context) {
        String language = UserManager.getInstance().getLanguage().locale;
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

}
