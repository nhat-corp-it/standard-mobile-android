package com.corpit.standard.ui.conference;


import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentConferenceBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Bookmark;
import com.corpit.standard.model.BookmarkDao;
import com.corpit.standard.model.Booth;
import com.corpit.standard.model.Conference;
import com.corpit.standard.model.ConferenceSpeakers;
import com.corpit.standard.model.ConferenceSpeakersDao;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.speakers.SpeakerFragment;
import com.corpit.standard.util.CalendarProvider;
import com.corpit.standard.util.SnackbarUtils;

import java.util.ArrayList;
import java.util.Collections;

import static com.corpit.standard.util.CalendarProvider.MY_CURRENT_REQUEST_WRITE_CALENDAR;

public class ConferenceFragment extends BaseFragment implements ItemClickListener<Speaker> {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String conferenceId;
    private Conference conference;

    private SmallSpeakerAdapter speakerAdapter;

    public final ObservableBoolean isBookmarked = new ObservableBoolean(false);

    public ConferenceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param conferenceId Parameter 1.
     * @return A new instance of fragment ConferenceFragment.
     */
    public static ConferenceFragment newInstance(String conferenceId) {
        ConferenceFragment fragment = new ConferenceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, conferenceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            conferenceId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentConferenceBinding binding = FragmentConferenceBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        conference = daoSession.getConferenceDao().load(conferenceId);
        binding.setConference(conference);

        Booth booth = daoSession.getBoothDao().load(conference.getBoothId());
        binding.setBooth(booth);

        binding.setHandlers(new Handlers());

        ArrayList<Bookmark> bookmarks = (ArrayList<Bookmark>) daoSession.getBookmarkDao().queryBuilder()
                .where(BookmarkDao.Properties.ItemId.eq(conferenceId)).list();
        isBookmarked.set(!bookmarks.isEmpty());
        binding.setIsBookmarked(isBookmarked);

        speakerAdapter = new SmallSpeakerAdapter(R.layout.row_speaker_small, this);
        binding.speakerList.setAdapter(speakerAdapter);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadDataFromDb();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_CURRENT_REQUEST_WRITE_CALENDAR:
                new Handlers().addBookmark();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Injector.get().eventTracker().trackConference(conference);
    }

    private void loadDataFromDb() {
        final ArrayList<ConferenceSpeakers> confSpeakers = (ArrayList<ConferenceSpeakers>) daoSession
                .getConferenceSpeakersDao()
                .queryBuilder()
                .where(ConferenceSpeakersDao.Properties.ConferenceId.eq(conferenceId)).list();
        Collections.sort(confSpeakers);

        ArrayList<Speaker> speakers = new ArrayList<>();
        SparseArray<String> speakerCategories = new SparseArray<>();
        for (int i = 0; i < confSpeakers.size(); i++) {
            ConferenceSpeakers conferenceSpeaker = confSpeakers.get(i);
            speakers.add(conferenceSpeaker.getSpeaker());
            speakerCategories.put(i, conferenceSpeaker.getSpeakerCategory());
        }

        speakerAdapter.setList(speakers, speakerCategories);
    }

    @Override
    public void onClick(Speaker item) {
        gotoFragment(SpeakerFragment.newInstance(item.getId()));
    }

    public class Handlers {

        public void removeBookmark() {
            Bookmark bookmark = daoSession.getBookmarkDao().queryBuilder()
                    .where(BookmarkDao.Properties.ItemId.eq(conferenceId)).list().get(0);
            CalendarProvider.deleteConferenceFromCalendar(getContext(), bookmark.getCalendarEventId());
            daoSession.getBookmarkDao().deleteInTx(bookmark);

            SnackbarUtils.showSnackbar(getView(),
                    getResources().getString(R.string.toast_bookmark_removed));

            isBookmarked.set(false);
        }

        public void addBookmark() {
            Bookmark bookmark = Bookmark.create(Bookmark.Type.CONFERENCE, conferenceId, daoSession);

            String location = daoSession.getBoothDao().load(conference.getBoothId()).getName();
            long eventId = CalendarProvider.addConferenceToCalendar(getActivity(), conference, location);
            if (eventId == -1) return;
            bookmark.setCalendarEventId(eventId);

            daoSession.getBookmarkDao().insert(bookmark);

            SnackbarUtils.showSnackbar(getView(),
                    getResources().getString(R.string.toast_bookmark_added));

            isBookmarked.set(true);
        }
    }

}
