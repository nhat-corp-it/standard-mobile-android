package com.corpit.standard.ui.base.databoundrecyclerview;

public interface ListItem<T> extends Comparable<T> {

    String getId();
}
