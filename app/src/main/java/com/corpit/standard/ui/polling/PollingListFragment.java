package com.corpit.standard.ui.polling;


import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentPollingListBinding;
import com.corpit.standard.model.PollingConference;
import com.corpit.standard.model.PollingConferenceDao;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.WebViewDetailFragment;
import com.corpit.standard.ui.base.WebViewDetailFragment.WebType;
import com.corpit.standard.util.DateConverter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 */
public class PollingListFragment extends BaseFragment implements ItemClickListener<PollingConference> {

    private FragmentPollingListBinding binding;

    private PollingAdapter pollingAdapter;

    private final ObservableBoolean isDataEmpty = new ObservableBoolean(true);

    public PollingListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPollingListBinding.inflate(LayoutInflater.from(getContext()),
                container, false);

        binding.setColorScheme(colorScheme);
        binding.setIsDataEmpty(isDataEmpty);

        pollingAdapter = new PollingAdapter(R.layout.row_polling, this);

        pollingAdapter.setHasStableIds(true);

        binding.pollingList.setAdapter(pollingAdapter);

        binding.title.setText(getCurrentFragmentName());

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        loadFromDb();
        checkListEmpty();
    }

    private void loadFromDb() {
        String currentDate;
        if (BuildConfig.DEBUG) {
            currentDate = "22 March";
        } else {
            currentDate = DateConverter.getCurrentDate();
        }

        ArrayList<PollingConference> list = (ArrayList<PollingConference>) daoSession.getPollingConferenceDao()
                .queryBuilder().where(PollingConferenceDao.Properties.Date.eq(currentDate)).list();

        Collections.sort(list);

        pollingAdapter.setList(list);
    }

    private void checkListEmpty() {
        isDataEmpty.set(binding.pollingList.getAdapter().getItemCount() == 0);
    }

    @Override
    public void onClick(PollingConference item) {
        gotoFragment(WebViewDetailFragment.newInstance(item.getId(), WebType.POLLING));
    }
}
