package com.corpit.standard.ui.exhibitors;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.corpit.standard.data.local.DaoHelper;
import com.corpit.standard.databinding.FragmentExhibitorCountryListBinding;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.StringArrayAdapter;

import java.util.ArrayList;

import static com.corpit.standard.ui.exhibitors.ExhibitorListFragment.FILTER_BY_COUNTRY;

/**
 * A simple {@link Fragment} subclass.
 */
public class CountryListFragment extends BaseFragment {

    public CountryListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentExhibitorCountryListBinding binding = FragmentExhibitorCountryListBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        ArrayList<String> countries = DaoHelper.getCountries();

        final StringArrayAdapter adapter = new StringArrayAdapter(getContext(), countries);

        binding.countryList.setAdapter(adapter);

        binding.countryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoFragment(ExhibitorListFragment.newInstance(FILTER_BY_COUNTRY, adapter.getItem(position)));
            }
        });

        return binding.getRoot();
    }

}
