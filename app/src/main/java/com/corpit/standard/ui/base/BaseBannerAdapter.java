package com.corpit.standard.ui.base;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.RestrictTo;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.corpit.standard.R;
import com.corpit.standard.data.remote.WithImage;

import java.util.ArrayList;

import static android.widget.Adapter.IGNORE_ITEM_VIEW_TYPE;

/**
 * Adapter class to create a scrollable list of banners.
 *
 * @param <T> Model class that have banner url.
 */
public class BaseBannerAdapter<T extends WithImage> extends PagerAdapter {

    private Context mContext;
    private ArrayList<T> mValues;

    private final RecycleBin recycleBin;

    private int size;
    private boolean isInfiniteLoop;

    public BaseBannerAdapter(Context context, ArrayList<T> values) {
        mContext = context;
        mValues = values;
        size = mValues.size();
        isInfiniteLoop = false;

        recycleBin = new RecycleBin();
        recycleBin.setViewTypeCount(getViewTypeCount());
    }

    @Override
    public void notifyDataSetChanged() {
        recycleBin.scrapActiveViews();
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return isInfiniteLoop ? Integer.MAX_VALUE : mValues.size();
    }

    private int getPosition(int position) {
        return isInfiniteLoop ? position % size : position;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int viewType = getItemViewType(position);
        View view = null;
        if (viewType != IGNORE_ITEM_VIEW_TYPE) {
            view = recycleBin.getScrapView(position, viewType);
        }
        view = getView(position, view, container);
        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        int viewType = getItemViewType(position);
        if (viewType != IGNORE_ITEM_VIEW_TYPE) {
            recycleBin.addScrapView((View) object, position, viewType);
        }
    }

    @SuppressWarnings("unchecked")
    private View getView(int position, View convertView, ViewGroup container) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.row_banner, container, false);
        }

        RequestOptions options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL);
        if (getPlaceHolder() != 0) {
            options = options.placeholder(getPlaceHolder());
        }

        Glide.with(mContext)
                .load(mValues.get(getPosition(position)).getUrl())
                .apply(options)
                .into((ImageView) convertView);

        return convertView;
    }

    private int getViewTypeCount() {
        return 1;
    }

    private int getItemViewType(int position) {
        return 0;
    }

    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    public void setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
    }

    @DrawableRes
    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected int getPlaceHolder() {
        return 0;
    }
}
