package com.corpit.standard.ui.bookmark;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentBookmarkListBinding;
import com.corpit.standard.model.Bookmark;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.conference.ConferenceFragment;
import com.corpit.standard.ui.exhibitors.ExhibitorFragment;
import com.corpit.standard.ui.speakers.SpeakerFragment;

public class BookmarkListFragment extends BaseFragment implements ItemClickListener<Bookmark> {

    private BookmarkViewModel viewModel;

    public BookmarkListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentBookmarkListBinding binding = FragmentBookmarkListBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        viewModel = ViewModelProviders.of(this).get(BookmarkViewModel.class);

        binding.setViewModel(viewModel);

        binding.title.setText(getCurrentFragmentName());

        BookmarkAdapter bookmarkAdapter = new BookmarkAdapter(R.layout.row_bookmark, this);
        bookmarkAdapter.setHasStableIds(true);
        binding.bookmarkList.setAdapter(bookmarkAdapter);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        viewModel.loadBookmarks();
    }

    @Override
    public void onClick(Bookmark item) {
        @Bookmark.Type String type = item.getBookmarkType();
        switch (type) {
            case Bookmark.Type.EXHIBITOR:
                gotoFragment(ExhibitorFragment.newInstance(item.getItemId(), false));
                break;
            case Bookmark.Type.SPEAKER:
                gotoFragment(SpeakerFragment.newInstance(item.getItemId()));
                break;
            case Bookmark.Type.CONFERENCE:
                gotoFragment(ConferenceFragment.newInstance(item.getItemId()));
                break;
            default:
                break;
        }
    }
}
