package com.corpit.standard.ui.eposter;


import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.databinding.TabCommentsBinding;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommentsTab extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";

    private String eposterId;

    public CommentsTab() {
        // Required empty public constructor
    }

    public static CommentsTab newInstance(String eposterId) {
        CommentsTab fragment = new CommentsTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, eposterId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eposterId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TabCommentsBinding binding = TabCommentsBinding.inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        LayerDrawable stars = (LayerDrawable) binding.ratingBar.getProgressDrawable();
        stars.getDrawable(1).setColorFilter(getResources()
                .getColor(android.R.color.holo_orange_light), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(getResources()
                .getColor(android.R.color.holo_orange_light), PorterDuff.Mode.SRC_ATOP);

        return binding.getRoot();
    }

}
