package com.corpit.standard.ui.home;

import android.content.Context;

import com.corpit.standard.R;
import com.corpit.standard.model.ScrollingBanner;
import com.corpit.standard.ui.base.BaseBannerAdapter;

import java.util.ArrayList;

public class ScrollingBannerAdapter extends BaseBannerAdapter<ScrollingBanner> {

    ScrollingBannerAdapter(Context context, ArrayList<ScrollingBanner> values) {
        super(context, values);
    }

    @Override
    protected int getPlaceHolder() {
        return R.drawable.holder_scrolling_banner;
    }
}
