package com.corpit.standard.ui.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.corpit.standard.BuildConfig;
import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.Configuration;
import com.corpit.standard.model.ConfigurationImages;
import com.corpit.standard.model.ConfigurationImages.ImageKey;
import com.corpit.standard.model.ConfigurationImagesDao;
import com.corpit.standard.share.Role;
import com.corpit.standard.share.UserManager;
import com.corpit.standard.ui.base.ParentActivity;
import com.corpit.standard.ui.download.DownloadActivity;
import com.corpit.standard.ui.home.MainActivity;
import com.corpit.standard.ui.login.LoginActivity;

import java.util.List;
import java.util.UUID;

import static android.Manifest.permission.READ_PHONE_STATE;

public class SplashActivity extends ParentActivity {

    private static final int SPLASH_TIME_OUT = 2000;

    private static final int REQUEST_DOWNLOAD = 1338;

    public static final String KEY_IS_FIRST_TIME = "isFirstTime";
    public static final String KEY_DATABASE_VERSION = "databaseVersion";
    public static final String KEY_UPDATED_AT = "updatedAt";

    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 11;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserManager.getInstance().loadUser();

        setContentView(R.layout.activity_splash);

        initBackground();

        //Immersive mode
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        if (checkPermission()) {
            splash();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE:
                splash();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_DOWNLOAD) {
            if (resultCode == RESULT_OK) finishSplash();
            else finish();
        }
    }

    private void initBackground() {
        List<ColorScheme> colorSchemes = daoSession.getColorSchemeDao().loadAll();
        if (!colorSchemes.isEmpty()) {
            ColorScheme colorScheme = colorSchemes.get(0);
            findViewById(R.id.root).setBackgroundColor(colorScheme.getColorBackground1Code());
        }

        ConfigurationImages configurationImage = daoSession.getConfigurationImagesDao().queryBuilder()
                .where(ConfigurationImagesDao.Properties.Key.eq(ImageKey.SPLASH))
                .unique();

        if (configurationImage != null) {
            Glide.with(this)
                    .load(configurationImage.getFile())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model,
                                                       Target<Drawable> target, DataSource dataSource,
                                                       boolean isFirstResource) {
                            findViewById(R.id.logo).setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into((ImageView) findViewById(R.id.splash));
        }
    }

    private void finishSplash() {
        Intent intent;
        if (shouldLogin()) {
            intent = new Intent(SplashActivity.this, LoginActivity.class);
        } else {
            intent = new Intent(SplashActivity.this, MainActivity.class);
        }
        startActivity(intent);
        finish();
    }

    private void splash() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //Check if is the first time open the app. Only download data on the first time.
                if (shouldDownload()) {
                    Intent intent = new Intent(SplashActivity.this, DownloadActivity.class);
                    startActivityForResult(intent, REQUEST_DOWNLOAD);
                } else {
                    finishSplash();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    @SuppressWarnings("RedundantIfStatement")
    private boolean shouldDownload() {
        SharedPreferences sharedPreferences = Injector.get().sharedPreferences();

        int databaseVersion = sharedPreferences.getInt(KEY_DATABASE_VERSION, 1);
        if (databaseVersion != BuildConfig.SCHEMA_VERSION) return true;

        boolean isFirstTime = sharedPreferences.getBoolean(KEY_IS_FIRST_TIME, true);
        if (isFirstTime) {
            saveGuid();
            return true;
        }

        if (daoSession.getColorSchemeDao().loadAll().isEmpty()) return true;

        return false;
    }

    private void saveGuid() {
        UserManager.getInstance().setUserId(UUID.randomUUID().toString());
    }

    private boolean shouldLogin() {
        Configuration configuration = Injector.get().configuration();

        if (configuration != null) {
            String accounts = configuration.getEnableAccount();
            return !accounts.equals((Role.GUEST)) && UserManager.getInstance().getRole().equals(Role.GUEST);
        }

        return false;
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            return false;
        }

        return true;
    }

}
