package com.corpit.standard.ui.speakers;

import android.support.v4.view.ViewCompat;

import com.corpit.standard.databinding.RowSpeakerBinding;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

public class SpeakerAdapter extends StandardBoundAdapter<Speaker, RowSpeakerBinding> {

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId The layout to be used for items. It must use data binding.
     * @param listener Basic item click listener
     */
    SpeakerAdapter(int layoutId, ItemClickListener<Speaker> listener) {
        super(layoutId, listener);
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowSpeakerBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);

        Speaker speaker = getList().get(holder.getAdapterPosition());

        boolean hideCategory = holder.getAdapterPosition() > 0
                && getList().get(holder.getAdapterPosition() - 1).getSectionName()
                .equals(speaker.getSectionName());
        holder.binding.setHideCategory(hideCategory);

        ViewCompat.setTransitionName(holder.binding.speakerPicture, speaker.getFullName());
    }
}
