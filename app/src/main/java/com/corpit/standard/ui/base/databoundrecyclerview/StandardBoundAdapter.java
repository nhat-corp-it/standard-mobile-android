package com.corpit.standard.ui.base.databoundrecyclerview;

import android.databinding.ViewDataBinding;
import android.support.annotation.CallSuper;
import android.support.v7.util.DiffUtil;

import com.corpit.standard.BR;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.ui.base.ItemClickListener;

import java.util.List;

/**
 * Standard adapter used for {@link android.support.v7.widget.RecyclerView} with databinding.
 * The layout of a view holder should contain at least 3 variables as followed:
 * <pre>
 *  {@code
 *
 *  <data>
 *      <variable
 *          name="data"
 *          type="com.corpit.standard.model.Conference" />
 *
 *      <variable
 *          name="listener"
 *          type="com.corpit.standard.ui.base.ItemClickListener&lt;com.corpit.standard.model.Conference>" />
 *
 *       <variable
 *           name="colorScheme"
 *           type="com.corpit.standard.model.ColorScheme" />
 *  </data>
 *  }
 * </pre>
 * @param <M> The model class
 * @param <B> The type of the binding class
 */
public class StandardBoundAdapter<M extends ListItem<M>, B extends ViewDataBinding>
        extends DataBoundAdapter<B> {

    private static final int DIFF_DATA_SIZE_LIMIT = 1000;

    private ColorScheme colorScheme;
    private ItemClickListener<M> mListener;
    private List<M> mData;

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId    The layout to be used for items. It must use data binding.
     * @param listener    Basic item click listener
     */
    public StandardBoundAdapter(int layoutId, ItemClickListener<M> listener) {
        super(layoutId);
        this.colorScheme = Injector.get().colorScheme();
        this.mListener = listener;
    }

    public List<M> getList() {
        return mData;
    }

    public void setList(final List<M> data) {
        if (mData == null) {
            mData = data;
            notifyItemRangeInserted(0, data.size());
        } else if (data.size() > DIFF_DATA_SIZE_LIMIT) {
            //if the data set is too large, don't compare
            mData = data;
            notifyDataSetChanged();
        } else {
            final DiffUtil.DiffResult result = DiffUtil.calculateDiff(new BaseDiffCallback<>(mData, data));
            mData = data;
            result.dispatchUpdatesTo(StandardBoundAdapter.this);
        }
    }

    public ColorScheme getColorScheme() {
        return colorScheme;
    }

    @CallSuper
    @Override
    protected void bindItem(DataBoundViewHolder<B> holder, int position, List<Object> payloads) {
        holder.binding.setVariable(BR.colorScheme, colorScheme);
        holder.binding.setVariable(BR.data, mData.get(position));
        holder.binding.setVariable(BR.listener, mListener);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public long getItemId(int position) {
        return mData.get(position).hashCode();
    }
}
