package com.corpit.standard.ui.liveqa;


import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentLiveQaListBinding;
import com.corpit.standard.databinding.RowConferenceBinding;
import com.corpit.standard.model.Conference;
import com.corpit.standard.model.ConferenceDao;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.WebViewDetailFragment;
import com.corpit.standard.ui.base.WebViewDetailFragment.WebType;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;
import com.corpit.standard.util.DateConverter;

import java.util.ArrayList;
import java.util.Collections;

public class LiveQAListFragment extends BaseFragment implements ItemClickListener<Conference> {

    private StandardBoundAdapter<Conference, RowConferenceBinding> qaAdapter;

    private final ObservableBoolean isDataEmpty = new ObservableBoolean(true);

    public LiveQAListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentLiveQaListBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_live_qa_list, container, false);

        binding.setColorScheme(colorScheme);

        binding.title.setText(getCurrentFragmentName());

        qaAdapter = new StandardBoundAdapter<>(R.layout.row_conference, this);
        qaAdapter.setHasStableIds(true);
        binding.qaList.setAdapter(qaAdapter);

        binding.setIsDataEmpty(isDataEmpty);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        loadDataFromDb();
        checkListEmpty();
    }

    private void loadDataFromDb() {
        String currentDate = DateConverter.getCurrentDate();
        ArrayList<Conference> list = (ArrayList<Conference>) daoSession.getConferenceDao().queryBuilder()
                .where(ConferenceDao.Properties.LiveQA.eq("Yes"))
                .where(ConferenceDao.Properties.Date.eq(currentDate))
                .list();
        Collections.sort(list);

        qaAdapter.setList(list);
    }

    private void checkListEmpty() {
        isDataEmpty.set(qaAdapter.getItemCount() == 0);
    }

    @Override
    public void onClick(Conference conference) {
        gotoFragment(WebViewDetailFragment.newInstance(conference.getId(), WebType.LIVE_QA));
    }
}
