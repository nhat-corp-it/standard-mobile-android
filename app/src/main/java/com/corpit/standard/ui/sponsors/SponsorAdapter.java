package com.corpit.standard.ui.sponsors;

import com.corpit.standard.databinding.RowSponsorBinding;
import com.corpit.standard.model.Sponsor;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

public class SponsorAdapter extends StandardBoundAdapter<Sponsor, RowSponsorBinding> {

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId The layout to be used for items. It must use data binding.
     * @param listener Basic item click listener
     */
    SponsorAdapter(int layoutId, ItemClickListener<Sponsor> listener) {
        super(layoutId, listener);
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowSponsorBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);
        boolean hideCategory = holder.getAdapterPosition() > 0
                && getList().get(holder.getAdapterPosition() - 1).getCategory()
                .equals(getList().get(holder.getAdapterPosition()).getCategory());
        holder.binding.setHideCategory(hideCategory);
    }
}
