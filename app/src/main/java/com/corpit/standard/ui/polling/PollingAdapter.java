package com.corpit.standard.ui.polling;

import android.graphics.drawable.GradientDrawable;

import com.corpit.standard.databinding.RowPollingBinding;
import com.corpit.standard.model.PollingConference;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

public class PollingAdapter extends StandardBoundAdapter<PollingConference, RowPollingBinding> {
    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId The layout to be used for items. It must use data binding.
     * @param listener Basic item click listener
     */
    PollingAdapter(int layoutId, ItemClickListener<PollingConference> listener) {
        super(layoutId, listener);
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowPollingBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);
        holder.binding.setPosition(holder.getAdapterPosition());
        GradientDrawable drawable = (GradientDrawable) holder.binding.divider.getBackground();
        drawable.setColor(getColorScheme().getColorFont1Code());
    }
}
