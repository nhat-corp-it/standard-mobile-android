package com.corpit.standard.ui.home;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer.DrawableContainerState;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.Menu;
import com.corpit.standard.widget.AnimatedExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nhatton on 23/3/18.
 */

public class MenuAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

    private Context mContext;
    private ArrayList<Menu> groups;
    private HashMap<String, ArrayList<Menu>> children;
    private ColorScheme colorScheme;

    private final ColorStateList textColorStateList;

    MenuAdapter(Context context, ArrayList<Menu> menus) {
        mContext = context;
        groups = new ArrayList<>();
        children = new HashMap<>();
        for (int i = 0; i < menus.size(); i++) {
            if (menus.get(i).isGroup()) {
                groups.add(menus.get(i));
            } else {
                String parentId = menus.get(i).getParentId();
                ArrayList<Menu> child = children.get(parentId);
                if (child == null) {
                    child = new ArrayList<>();
                }
                child.add(menus.get(i));
                children.put(parentId, child);
            }
        }

        this.colorScheme = Injector.get().colorScheme();

        textColorStateList = new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_activated}, new int[]{}},
                new int[]{Color.WHITE, colorScheme.getColorFont1Code()});
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        String parentId = groups.get(groupPosition).getId();
        ArrayList<Menu> child = children.get(parentId);
        return child == null ? 0 : child.size();
    }

    @Override
    public Menu getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Menu getChild(int groupPosition, int childPosition) {
        String parentId = groups.get(groupPosition).getId();
        ArrayList<Menu> childList = children.get(parentId);
        return childList.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return getGroup(groupPosition).hashCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).hashCode();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_menu_item, parent, false);
            holder = new ViewHolder();
            holder.mView = convertView;
            holder.mIcon = convertView.findViewById(R.id.menu_icon);
            holder.mText = convertView.findViewById(R.id.menu_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mItem = getGroup(groupPosition);

        StateListDrawable background = (StateListDrawable) holder.mView.getBackground();
        DrawableContainerState constantState = (DrawableContainerState) background.getConstantState();
        Drawable[] children = new Drawable[0];
        if (constantState != null) {
            children = constantState.getChildren();
        }
        ColorDrawable selectedItem = (ColorDrawable) children[0];
        selectedItem.setColor(colorScheme.getColorButton1Code());

        Glide.with(mContext)
                .load(holder.mItem.getIconUrl())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_menu_item_default)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.mIcon);

        holder.mText.setText(holder.mItem.getEnTitle());
        holder.mText.setTextColor(textColorStateList);

        return convertView;
    }

    @Override
    public View getRealChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_menu_item, parent, false);
            holder = new ViewHolder();
            holder.mView = convertView;
            holder.mIcon = convertView.findViewById(R.id.menu_icon);
            holder.mText = convertView.findViewById(R.id.menu_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        StateListDrawable background = (StateListDrawable) holder.mView.getBackground();
        DrawableContainerState constantState = (DrawableContainerState) background.getConstantState();
        Drawable[] children = new Drawable[0];
        if (constantState != null) {
            children = constantState.getChildren();
        }
        ColorDrawable selectedItem = (ColorDrawable) children[0];
        selectedItem.setColor(colorScheme.getColorButton1Code());

        holder.mItem = getChild(groupPosition, childPosition);

        holder.mText.setText(holder.mItem.getEnTitle());
        holder.mText.setTextColor(textColorStateList);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        String parentId = groups.get(groupPosition).getId();
        return children.get(parentId) != null;
    }

    public int getGroupPosition(Menu menu) {
        return groups.indexOf(menu);
    }

    public boolean groupHasChildren(int groupPosition) {
        return getRealChildrenCount(groupPosition) > 0;
    }

    public boolean groupHasChildren(Menu menu) {
        return menu.isGroup() && groupHasChildren(getGroupPosition(menu));
    }

    public int getChildPosition(Menu menu) {
        if (!menu.isGroup()) {
            return children.get(menu.getParentId()).indexOf(menu);
        } else {
            return -1;
        }
    }

    static class ViewHolder {
        View mView;
        ImageView mIcon;
        TextView mText;
        Menu mItem;
    }
}
