package com.corpit.standard.ui.conference;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.RowConferenceBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.Conference;
import com.corpit.standard.widget.AnimatedExpandableListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConferenceAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

    private Context mContext;
    private ColorScheme colorScheme;
    private List<Conference> mGroup = new ArrayList<>();
    private HashMap<String, List<Conference>> mChildMap = new HashMap<>();

    ConferenceAdapter(Context context) {
        this.mContext = context;
        this.colorScheme = Injector.get().colorScheme();
    }

    @Override
    public int getGroupCount() {
        return mGroup.size();
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        String mainConferenceId = mGroup.get(groupPosition).getId();

        return mChildMap.get(mainConferenceId).size();
    }

    @Override
    public Conference getGroup(int groupPosition) {
        return mGroup.get(groupPosition);
    }

    @Override
    public Conference getChild(int groupPosition, int childPosition) {
        String mainConferenceId = mGroup.get(groupPosition).getId();
        return mChildMap.get(mainConferenceId).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return getGroup(groupPosition).hashCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).hashCode();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        RowConferenceBinding binding;

        if (convertView == null) {
            binding = RowConferenceBinding.inflate(LayoutInflater.from(mContext), parent, false);
            convertView = binding.getRoot();
            convertView.setTag(binding);
        } else {
            binding = (RowConferenceBinding) convertView.getTag();
        }

        binding.setColorScheme(colorScheme);

        Conference conference = mGroup.get(groupPosition);
        binding.setData(conference);

        binding.getRoot().setBackgroundColor(colorScheme.getColorColumn2Code());

        if (groupHasChildren(groupPosition)) {
            binding.groupIndicator.setVisibility(View.VISIBLE);
            binding.groupIndicator.setImageResource(isExpanded ? R.drawable.ic_arrow_up : R.drawable.ic_arrow_down);
        } else {
            binding.groupIndicator.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        RowConferenceBinding binding;

        if (convertView == null) {
            binding = RowConferenceBinding.inflate(LayoutInflater.from(mContext), parent, false);
            convertView = binding.getRoot();
            convertView.setTag(binding);
        } else {
            binding = (RowConferenceBinding) convertView.getTag();
        }

        binding.setColorScheme(colorScheme);

        String mainConferenceId = mGroup.get(groupPosition).getId();
        Conference conference = mChildMap.get(mainConferenceId).get(childPosition);
        binding.setData(conference);

        binding.getRoot().setBackgroundColor(colorScheme.getColorColumn1Code());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setList(final List<Conference> data) {
        clear();

        for (Conference conference : data) {
            if (TextUtils.isEmpty(conference.getMainConferenceId())) {
                mGroup.add(conference);
                initialChildren(conference.getId());
            } else {
                initialChildren(conference.getMainConferenceId()).add(conference);
            }
        }

        sort();

        notifyDataSetChanged();
    }

    public boolean groupHasChildren(int groupPosition) {
        return getRealChildrenCount(groupPosition) > 0;
    }

    private void sort() {
        Collections.sort(mGroup);

        Set<Map.Entry<String, List<Conference>>> entries = mChildMap.entrySet();
        for (Map.Entry<String, List<Conference>> entry : entries) {
            Collections.sort(entry.getValue());
        }
    }

    private List<Conference> initialChildren(String mainConferenceId) {
        List<Conference> children = mChildMap.get(mainConferenceId);
        if (children == null) {
            children = new ArrayList<>();
            mChildMap.put(mainConferenceId, children);
        }

        return children;
    }

    private void clear() {
        mGroup = new ArrayList<>();
        mChildMap = new HashMap<>();
    }
}
