package com.corpit.standard.ui.exhibitors;

import android.databinding.ObservableBoolean;

import com.corpit.standard.databinding.RowExhibitorBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Booth;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.model.ExhibitingCompany;
import com.corpit.standard.model.Location;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

public class ExhibitorAdapter extends StandardBoundAdapter<ExhibitingCompany, RowExhibitorBinding> {

    private DaoSession daoSession;

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId The layout to be used for items. It must use data binding.
     * @param listener Basic item click listener
     */
    ExhibitorAdapter(int layoutId, ItemClickListener<ExhibitingCompany> listener) {
        super(layoutId, listener);
        this.daoSession = Injector.get().daoSession();
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowExhibitorBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);
        ExhibitingCompany exhibitor = getList().get(holder.getAdapterPosition());

        Booth booth = daoSession.getBoothDao().load(exhibitor.getBoothId());
        holder.binding.setBooth(booth);

        Location location = null;
        if (booth != null) {
            location = daoSession.getLocationDao().load(booth.getLocationId());
        }
        holder.binding.setLocation(location);

        ObservableBoolean hideCategory = new ObservableBoolean(holder.getAdapterPosition() > 0 && getList().get(holder.getAdapterPosition() - 1).getSectionName()
                .equals(exhibitor.getSectionName()));
        holder.binding.setHideCategory(hideCategory);
    }
}
