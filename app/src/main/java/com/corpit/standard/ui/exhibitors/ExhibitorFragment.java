package com.corpit.standard.ui.exhibitors;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentExhibitorBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Bookmark;
import com.corpit.standard.model.BookmarkDao;
import com.corpit.standard.model.Booth;
import com.corpit.standard.model.ExhibitingCompany;
import com.corpit.standard.model.Sponsor;
import com.corpit.standard.model.SponsorDao;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.floorplans.IndividualHallFragment;
import com.corpit.standard.util.SnackbarUtils;

import java.util.ArrayList;

import static com.corpit.standard.model.Bookmark.create;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExhibitorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExhibitorFragment extends BaseFragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String exhibitorId;
    private boolean isFromSponsor;
    private ExhibitingCompany exhibitor;

    private ImageButton bookmarkButton;
    private Booth booth;

    public ExhibitorFragment() {
        // Required empty public constructor
    }

    public static ExhibitorFragment newInstance(String exhibitorId) {
        return newInstance(exhibitorId, false);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ExhibitorFragment.
     */
    public static ExhibitorFragment newInstance(String exhibitorId, boolean isFromSponsor) {
        ExhibitorFragment fragment = new ExhibitorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, exhibitorId);
        args.putBoolean(ARG_PARAM2, isFromSponsor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            exhibitorId = getArguments().getString(ARG_PARAM1);
            isFromSponsor = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentExhibitorBinding binding = FragmentExhibitorBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        exhibitor = daoSession.getExhibitingCompanyDao().load(exhibitorId);

        binding.setExhibitor(exhibitor);

        booth = daoSession.getBoothDao().load(exhibitor.getBoothId());
        binding.setBooth(booth);

        bookmarkButton = binding.exhibitorBookmark;
        initBookmarkButton();

        ImageButton floorplanButton = binding.exhibitorFloorPlan;

        if (booth != null) {
            floorplanButton.setActivated(true);
            floorplanButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    gotoFragment(IndividualHallFragment.newInstance(booth.getLocationId(), booth.getId()));

                }
            });
        }

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isFromSponsor) {
            Sponsor sponsor = daoSession.getSponsorDao().queryBuilder()
                    .where(SponsorDao.Properties.ExhibitorId.eq(exhibitorId))
                    .list().get(0);

            Injector.get().eventTracker().trackSponsor(sponsor);
        } else {
            Injector.get().eventTracker().trackExhibitor(exhibitor);
        }

    }

    private void initBookmarkButton() {

        ArrayList<Bookmark> bookmarks = (ArrayList<Bookmark>) daoSession.getBookmarkDao().queryBuilder()
                .where(BookmarkDao.Properties.ItemId.eq(exhibitorId)).list();
        boolean bookmarkExisted = !bookmarks.isEmpty();
        bookmarkButton.setActivated(bookmarkExisted);

        bookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bookmarkButton.isActivated()) {
                    Bookmark bookmark = daoSession.getBookmarkDao().queryBuilder()
                            .where(BookmarkDao.Properties.ItemId.eq(exhibitorId)).list().get(0);
                    daoSession.getBookmarkDao().deleteInTx(bookmark);

                    SnackbarUtils.showSnackbar(getView(),
                            getResources().getString(R.string.toast_bookmark_removed));

                    bookmarkButton.setActivated(false);

                } else {
                    Bookmark bookmark = create(Bookmark.Type.EXHIBITOR, exhibitorId, daoSession);

                    daoSession.getBookmarkDao().insert(bookmark);

                    SnackbarUtils.showSnackbar(getView(),
                            getResources().getString(R.string.toast_bookmark_added));

                    bookmarkButton.setActivated(true);
                }
            }
        });
    }

}
