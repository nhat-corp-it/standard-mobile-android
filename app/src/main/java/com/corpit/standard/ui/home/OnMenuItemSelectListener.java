package com.corpit.standard.ui.home;

import android.support.annotation.NonNull;

import com.corpit.standard.model.Menu;

/**
 * Created by nhatton on 24/3/18.
 */

public interface OnMenuItemSelectListener {

    void onMenuItemSelected(@NonNull Menu item, int flatPosition);
}
