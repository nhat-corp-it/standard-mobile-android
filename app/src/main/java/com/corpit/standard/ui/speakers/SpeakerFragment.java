package com.corpit.standard.ui.speakers;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentSpeakerBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Bookmark;
import com.corpit.standard.model.BookmarkDao;
import com.corpit.standard.model.Conference;
import com.corpit.standard.model.ConferenceSpeakers;
import com.corpit.standard.model.ConferenceSpeakersDao;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.conference.ConferenceFragment;
import com.corpit.standard.util.SnackbarUtils;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.Collections;

import static com.corpit.standard.model.Bookmark.create;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SpeakerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpeakerFragment extends BaseFragment implements ItemClickListener<Conference> {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String speakerId;
    private Speaker speaker;

    private FragmentSpeakerBinding binding;

    private SmallConferenceAdapter conferenceAdapter;

    public SpeakerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param speakerId Parameter 1.
     * @return A new instance of fragment SpeakerFragment.
     */
    public static SpeakerFragment newInstance(String speakerId) {
        SpeakerFragment fragment = new SpeakerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, speakerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            speakerId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSpeakerBinding.inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        speaker = daoSession.getSpeakerDao().load(speakerId);

        binding.setSpeaker(speaker);

        conferenceAdapter = new SmallConferenceAdapter(R.layout.row_conference_small, this);

        conferenceAdapter.setHasStableIds(true);

        binding.conferenceList.setAdapter(conferenceAdapter);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View root, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(root, savedInstanceState);

        Glide.with(getContext())
                .load(speaker.getUrl())
                .apply(new RequestOptions()
                        .dontAnimate()
                        .placeholder(R.drawable.speaker_placeholder)
                        .centerCrop().circleCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(binding.speakerPicture);
    }

    @Override
    public void onStart() {
        super.onStart();
        initBookmarkButton();
        loadDataFromDb();
        checkListEmpty();
    }

    @Override
    public void onResume() {
        super.onResume();
        Injector.get().eventTracker().trackSpeaker(speaker);
    }

    private void loadDataFromDb() {
        QueryBuilder<Conference> queryBuilder = daoSession.getConferenceDao().queryBuilder();
        queryBuilder.join(ConferenceSpeakers.class, ConferenceSpeakersDao.Properties.ConferenceId)
                .where(ConferenceSpeakersDao.Properties.SpeakerId.eq(speakerId));
        final ArrayList<Conference> conferences = (ArrayList<Conference>) queryBuilder.list();

        Collections.sort(conferences);

        conferenceAdapter.setList(conferences);
    }

    private void checkListEmpty() {
        if (conferenceAdapter.getItemCount() != 0) {
            binding.sessionText.setVisibility(View.VISIBLE);
        } else {
            binding.sessionText.setVisibility(View.GONE);
        }
    }

    private void initBookmarkButton() {

        ArrayList<Bookmark> bookmarks = (ArrayList<Bookmark>) daoSession.getBookmarkDao().queryBuilder()
                .where(BookmarkDao.Properties.ItemId.eq(speakerId)).list();
        boolean bookmarkExisted = !bookmarks.isEmpty();
        binding.speakerBookmark.setActivated(bookmarkExisted);

        binding.speakerBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.speakerBookmark.isActivated()) {
                    Bookmark bookmark = daoSession.getBookmarkDao().queryBuilder()
                            .where(BookmarkDao.Properties.ItemId.eq(speakerId)).unique();
                    daoSession.getBookmarkDao().deleteInTx(bookmark);

                    SnackbarUtils.showSnackbar(getView(),
                            getResources().getString(R.string.toast_bookmark_removed));

                    binding.speakerBookmark.setActivated(false);

                } else {
                    Bookmark bookmark = create(Bookmark.Type.SPEAKER, speakerId, daoSession);

                    daoSession.getBookmarkDao().insert(bookmark);

                    SnackbarUtils.showSnackbar(getView(),
                            getResources().getString(R.string.toast_bookmark_added));

                    binding.speakerBookmark.setActivated(true);
                }
            }
        });
    }

    @Override
    public void onClick(Conference item) {
        gotoFragment(ConferenceFragment.newInstance(item.getId()));
    }
}
