package com.corpit.standard.ui.conference;


import android.animation.LayoutTransition;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.corpit.standard.databinding.FragmentConferenceCategoriesBinding;
import com.corpit.standard.model.ConferenceDao;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.widget.FixListView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConferenceCategoriesFragment extends BaseFragment {

    private FixListView categoryListView;

    public ConferenceCategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentConferenceCategoriesBinding binding = FragmentConferenceCategoriesBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        binding.title.setText(getCurrentFragmentName());

        binding.buttonList.getLayoutTransition()
                .enableTransitionType(LayoutTransition.CHANGING);

        categoryListView = binding.categoryList;
        populateCategoryList();

        binding.byDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(ConferenceListFragment.newInstance(
                        ConferenceListFragment.FILTER_BY_DATE, ""));
            }
        });

        binding.byCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isVisible = categoryListView.getVisibility() == View.VISIBLE;
                categoryListView.setVisibility(isVisible ? View.GONE : View.VISIBLE);
            }
        });

        return binding.getRoot();
    }

    private void populateCategoryList() {
        ArrayList<String> categories = new ArrayList<>();
        Cursor cursor = daoSession.getDatabase().rawQuery(
                "SELECT DISTINCT " + ConferenceDao.Properties.Category.columnName +
                        " FROM " + ConferenceDao.TABLENAME +
                        " WHERE " + ConferenceDao.Properties.Category.columnName + " != ''", null);
        while (cursor.moveToNext()) {
            categories.add(cursor.getString(0));
        }
        cursor.close();
        Collections.sort(categories);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, categories);

        categoryListView.setAdapter(adapter);

        categoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoFragment(ConferenceListFragment.newInstance(
                        ConferenceListFragment.FILTER_BY_CATEGORY, adapter.getItem(position)));
            }
        });
    }

}
