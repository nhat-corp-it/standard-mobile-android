package com.corpit.standard.ui.conference;


import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.corpit.standard.R;
import com.corpit.standard.data.local.DaoHelper;
import com.corpit.standard.model.Conference;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.events.DayAdapter;
import com.corpit.standard.widget.AnimatedExpandableListView;

import java.util.ArrayList;

public class ConferenceListFragment extends BaseFragment implements DayAdapter.OnDaySelectListener {

    public static final int FILTER_BY_DATE = 0;
    public static final int FILTER_BY_CATEGORY = 1;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int filterBy;
    private String param;

    private AnimatedExpandableListView conferenceListView;
    private ConferenceAdapter conferenceAdapter;

    private RecyclerView dayListView;
    private int lastExpandedGroup = -1;

    public ConferenceListFragment() {
        // Required empty public constructor
    }

    public static ConferenceListFragment newInstance(int filterBy, String param) {
        ConferenceListFragment fragment = new ConferenceListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, filterBy);
        args.putString(ARG_PARAM2, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            filterBy = getArguments().getInt(ARG_PARAM1);
            param = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_conference_list, container, false);

        conferenceListView = root.findViewById(R.id.conference_list);

        dayListView = root.findViewById(R.id.day_list);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(dayListView.getContext(),
                DividerItemDecoration.HORIZONTAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        dayListView.addItemDecoration(dividerItemDecoration);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(dayListView);

        conferenceListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Conference conference = conferenceAdapter.getGroup(groupPosition);

                if (conferenceAdapter.groupHasChildren(groupPosition)) {
                    if (conferenceListView.isGroupExpanded(groupPosition)) {
                        conferenceListView.collapseGroupWithAnimation(groupPosition);
                    } else {
                        conferenceListView.expandGroupWithAnimation(groupPosition);
                        if (lastExpandedGroup != -1 && groupPosition != lastExpandedGroup) {
                            conferenceListView.collapseGroup(lastExpandedGroup);
                        }
                        lastExpandedGroup = groupPosition;
                    }
                } else {
                    gotoFragment(ConferenceFragment.newInstance(conference.getId()));
                }

                return true;
            }
        });

        conferenceListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Conference conference = conferenceAdapter.getChild(groupPosition, childPosition);
                gotoFragment(ConferenceFragment.newInstance(conference.getId()));
                return true;
            }
        });

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadFromDb();
    }

    private void loadFromDb() {
        ArrayList<String> days = new ArrayList<>();
        switch (filterBy) {
            case FILTER_BY_DATE:
                days = DaoHelper.getConferenceDays();
                break;
            case FILTER_BY_CATEGORY:
                days = DaoHelper.getConferenceDays(param);
                break;
            default:
                break;
        }
        DayAdapter dayAdapter = new DayAdapter(days, this);
        dayListView.setAdapter(dayAdapter);

        conferenceAdapter = new ConferenceAdapter(getContext());

        conferenceListView.setAdapter(conferenceAdapter);

        if (!days.isEmpty()) {
            onDaySelected(days.get(0));
        }

    }

    @Override
    public void onDaySelected(String day) {
        switch (filterBy) {
            case FILTER_BY_DATE:
                conferenceAdapter.setList(DaoHelper.getConferences(day));
                break;
            case FILTER_BY_CATEGORY:
                conferenceAdapter.setList(DaoHelper.getConferences(param, day));
                break;
        }

    }
}
