package com.corpit.standard.ui.sponsors;


import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.model.Sponsor;
import com.corpit.standard.model.SponsorDao;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.exhibitors.ExhibitorFragment;

import java.util.ArrayList;


public class SponsorListFragment extends BaseFragment implements ItemClickListener<Sponsor> {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String menuId;

    private SponsorAdapter sponsorAdapter;

    public SponsorListFragment() {
        // Required empty public constructor
    }

    public static SponsorListFragment newInstance(String menuId) {
        SponsorListFragment fragment = new SponsorListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, menuId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menuId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sponsor_list, container, false);

        TextView titleTextView = root.findViewById(R.id.title);
        titleTextView.setText(getCurrentFragmentName());
        int colorCode = colorScheme.getColorFont1Code();
        titleTextView.setTextColor(colorCode);

        sponsorAdapter = new SponsorAdapter(R.layout.row_sponsor, this);

        sponsorAdapter.setHasStableIds(true);

        RecyclerView sponsorListView = root.findViewById(R.id.sponsor_list);
        sponsorListView.setAdapter(sponsorAdapter);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadFromDb();
    }

    private void loadFromDb() {
        final ArrayList<Sponsor> sponsors = (ArrayList<Sponsor>) daoSession.getSponsorDao().queryBuilder()
                .where(SponsorDao.Properties.MenuId.eq(menuId)).list();

        sponsorAdapter.setList(sponsors);
    }

    @Override
    public void onClick(Sponsor sponsor) {
        if (!sponsor.getExhibitorId().isEmpty()
                && daoSession.getExhibitingCompanyDao().load(sponsor.getExhibitorId()) != null) {
            gotoFragment(ExhibitorFragment.newInstance(
                    sponsor.getExhibitorId(), true));
        } else {
            gotoFragment(SponsorFragment.newInstance(sponsor.getId()));
        }
    }
}
