package com.corpit.standard.ui.home;

import android.content.Context;

import com.corpit.standard.R;
import com.corpit.standard.model.BottomBanner;
import com.corpit.standard.ui.base.BaseBannerAdapter;

import java.util.ArrayList;

public class BottomBannerAdapter extends BaseBannerAdapter<BottomBanner> {

    public BottomBannerAdapter(Context context, ArrayList<BottomBanner> values) {
        super(context, values);
    }

    @Override
    protected int getPlaceHolder() {
        return R.drawable.holder_bottom_banner;
    }
}
