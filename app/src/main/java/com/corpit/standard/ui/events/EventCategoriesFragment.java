package com.corpit.standard.ui.events;


import android.animation.LayoutTransition;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.corpit.standard.R;
import com.corpit.standard.data.local.DaoHelper;
import com.corpit.standard.databinding.FragmentEventCategoriesBinding;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.widget.FixListView;

import java.util.ArrayList;


public class EventCategoriesFragment extends BaseFragment {

    private FixListView trackListView;

    public EventCategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentEventCategoriesBinding binding = FragmentEventCategoriesBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        binding.title.setText(getCurrentFragmentName());

        binding.buttonList.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);

        trackListView = binding.trackList;
        initTrackList();

        binding.byDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(EventListFragment.newInstance(EventListFragment.FILTER_BY_DATE, ""));
            }
        });

        binding.byTrackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isVisible = trackListView.getVisibility() == View.VISIBLE;
                trackListView.setVisibility(isVisible ? View.GONE : View.VISIBLE);

            }
        });

        return binding.getRoot();
    }

    private void initTrackList() {

        ArrayList<String> tracks = DaoHelper.getEventTracks();

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                R.layout.row_simple_item, android.R.id.text1, tracks);

        trackListView.setAdapter(adapter);

        trackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoFragment(EventListFragment.newInstance(EventListFragment.FILTER_BY_TRACK, adapter.getItem(position)));
            }
        });

    }

}
