package com.corpit.standard.ui.base;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;

import java.util.ArrayList;

public class StringArrayAdapter extends BaseAdapter {

    private ArrayList<String> mValues;
    private ColorScheme colorScheme;
    private Context mContext;

    public StringArrayAdapter(Context context, ArrayList<String> values) {
        mValues = values;
        this.colorScheme = Injector.get().colorScheme();
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    @Override
    public String getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_sub_category, parent, false);
            holder = new ViewHolder();
            holder.textView = convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(mValues.get(position));

        GradientDrawable background = (GradientDrawable) convertView.getBackground();
        background.setColor(colorScheme.getColorButton1Code());

        return convertView;
    }

    class ViewHolder {
        TextView textView;
    }
}
