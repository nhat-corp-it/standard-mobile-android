package com.corpit.standard.ui.sponsors;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.databinding.FragmentSponsorBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Sponsor;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SponsorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SponsorFragment extends BaseFragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String sponsorId;
    private Sponsor sponsor;

    public SponsorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sponsorId Parameter 1.
     * @return A new instance of fragment SponsorFragment.
     */
    public static SponsorFragment newInstance(String sponsorId) {
        SponsorFragment fragment = new SponsorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, sponsorId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sponsorId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentSponsorBinding binding = FragmentSponsorBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        sponsor = daoSession.getSponsorDao().load(sponsorId);

        binding.setSponsor(sponsor);

        binding.sponsorDetails.setMovementMethod(LinkMovementMethod.getInstance());

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Injector.get().eventTracker().trackSponsor(sponsor);
    }
}
