package com.corpit.standard.ui.home;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.corpit.standard.BuildConfig;
import com.corpit.standard.R;
import com.corpit.standard.data.remote.DownloadProgressCallBack;
import com.corpit.standard.data.remote.DownloadTaskManager;
import com.corpit.standard.data.remote.RetrofitResponse;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.BottomBanner;
import com.corpit.standard.model.Configuration;
import com.corpit.standard.model.ConfigurationImages;
import com.corpit.standard.model.ConfigurationImages.ImageKey;
import com.corpit.standard.model.ConfigurationImagesDao;
import com.corpit.standard.model.DataVersion;
import com.corpit.standard.model.Location;
import com.corpit.standard.model.LocationDao;
import com.corpit.standard.model.Menu;
import com.corpit.standard.model.Menu.MenuType;
import com.corpit.standard.model.Menu.PageType;
import com.corpit.standard.model.MenuDao;
import com.corpit.standard.share.UserManager;
import com.corpit.standard.share.UserManager.Language;
import com.corpit.standard.ui.base.BaseBannerAdapter;
import com.corpit.standard.ui.base.ParentActivity;
import com.corpit.standard.ui.base.WebActivity;
import com.corpit.standard.ui.bookmark.BookmarkListFragment;
import com.corpit.standard.ui.conference.ConferenceCategoriesFragment;
import com.corpit.standard.ui.download.DownloadActivity;
import com.corpit.standard.ui.eposter.EPosterCategoriesFragment;
import com.corpit.standard.ui.events.EventCategoriesFragment;
import com.corpit.standard.ui.events.EventFragment;
import com.corpit.standard.ui.exhibitors.ExhibitorCategoriesFragment;
import com.corpit.standard.ui.exhibitors.ExhibitorListFragment;
import com.corpit.standard.ui.floorplans.IndividualHallFragment;
import com.corpit.standard.ui.floorplans.OverallMapFragment;
import com.corpit.standard.ui.generalinfo.GeneralInfoCategoriesFragment;
import com.corpit.standard.ui.inbox.InboxListFragment;
import com.corpit.standard.ui.liveqa.LiveQAListFragment;
import com.corpit.standard.ui.login.LoginActivity;
import com.corpit.standard.ui.news.NewsListFragment;
import com.corpit.standard.ui.polling.PollingListFragment;
import com.corpit.standard.ui.products.ProductFragment;
import com.corpit.standard.ui.speakers.SpeakerListFragment;
import com.corpit.standard.ui.sponsors.SponsorListFragment;
import com.corpit.standard.ui.survey.SurveyFragment;
import com.corpit.standard.util.BootstrapHelper;
import com.corpit.standard.util.SizeConverter;
import com.corpit.standard.util.VectorUtil;
import com.corpit.standard.widget.AnimatedExpandableListView;
import com.corpit.standard.widget.ScrollViewPager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.jpush.android.api.JPushInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE;
import static android.support.v4.view.GravityCompat.START;
import static com.corpit.standard.ui.splash.SplashActivity.KEY_UPDATED_AT;
import static java.lang.System.currentTimeMillis;

public class MainActivity extends ParentActivity implements OnMenuItemSelectListener,
        HomeFragment.OnHomeInteractListener, DownloadProgressCallBack {

    private static final long ONE_HOUR = 3600000;

    private static final int REQUEST_DOWNLOAD = 1337;

    public static final String MESSAGE_RECEIVED_ACTION = "com.corpit.standard.MESSAGE_RECEIVED_ACTION";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";

    private static final String KEY_SELECTED_POSITION = "selectedPosition";
    private static final String KEY_SELECTED_MENU = "selectedMenu";
    private static final String KEY_LAST_EXPANDED_POSITION = "lastExpandedPosition";
    private static final String KEY_CURRENT_FRAGMENT_NAME = "currentFragmentName";
    public static final int TOGGLE_ICON_SIZE = 30;

    private Toolbar toolbar;
    public static boolean isForeground = false;
    private DrawerLayout drawer;
    private AnimatedExpandableListView menuListView;

    private ImageView navLogo;
    private ImageView topBanner;
    private ScrollViewPager bottomBanner;

    private HomeFragment homeFragment;

    private FragmentManager fragmentManager;
    private MenuAdapter menuAdapter;
    private int selectedPosition = -1;
    private int lastExpandedPosition = -1;

    public String currentFragmentName = "";

    private Menu selectedMenu;

    // To receive broadcast from JPush
    private MessageReceiver mMessageReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        updateData();

        initNavLogo();

        initMenuList();

        initTopBanner();

        initToolbar();

        initDrawer();

        if (BuildConfig.MULTIPLE_LANGUAGES) {
            initSpinner();
        }

        initBottomBanner();

        fragmentManager = getSupportFragmentManager();

        //To prevent lost save instance when change orientation
        if (savedInstanceState == null) {
            homeFragment = new HomeFragment();

            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment_container, homeFragment, null)
                    .commit();

        }

        registerMessageReceiver();

        if (!BuildConfig.DEBUG) {
            checkVersion();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_SELECTED_POSITION, selectedPosition);
        outState.putParcelable(KEY_SELECTED_MENU, selectedMenu);
        outState.putInt(KEY_LAST_EXPANDED_POSITION, lastExpandedPosition);
        outState.putString(KEY_CURRENT_FRAGMENT_NAME, currentFragmentName);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            selectedPosition = savedInstanceState.getInt(KEY_SELECTED_POSITION);
            menuListView.setItemChecked(selectedPosition, true);

            selectedMenu = savedInstanceState.getParcelable(KEY_SELECTED_MENU);

            lastExpandedPosition = savedInstanceState.getInt(KEY_LAST_EXPANDED_POSITION);
            if (lastExpandedPosition != -1) {
                menuListView.expandGroup(lastExpandedPosition);
            }

            currentFragmentName = savedInstanceState.getString(KEY_CURRENT_FRAGMENT_NAME);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Drawable drawable = topBanner.getDrawable();
        if (drawable != null) {
            setHeights(drawable.getIntrinsicHeight());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        bottomBanner.stopAutoScroll();
        JPushInterface.onPause(this);
        isForeground = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (daoSession.getBottomBannerDao().count() > 1) {
            bottomBanner.startAutoScroll();
        }
        JPushInterface.onResume(this);
        isForeground = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(START)) {
            drawer.closeDrawer(START);
        } else {
            super.onBackPressed();
            //Returned to home, remove checked in drawer menu
            if (fragmentManager.getBackStackEntryCount() == 0) {
                returnHome();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_DOWNLOAD && resultCode == RESULT_OK) {
            recreate();
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onMenuItemSelected(@NonNull Menu menuItem, int flatPosition) {
        // Send Custom Event to Amazon Pinpoint
        Injector.get().eventTracker().trackMenuClick(menuItem);

        @Menu.PageType String pageType = menuItem.getPageType();
        Intent intent;
        Bundle translateBundle = ActivityOptions.makeCustomAnimation(MainActivity.this,
                R.anim.slide_in_left, R.anim.slide_out_left).toBundle();

        switch (pageType) {
            case PageType.MORE_INFORMATION:
                displayFragment(GeneralInfoCategoriesFragment.newInstance(menuItem.getId()));
                break;
            case PageType.EXHIBITORS:
                displayFragment(new ExhibitorCategoriesFragment());
                break;
            case PageType.FLOORPLAN:
                if (menuItem.getEnTitle().equals("Overall map")) {
                    displayFragment(new OverallMapFragment());
                } else {
                    List<Location> list = daoSession.getLocationDao().queryBuilder()
                            .where(LocationDao.Properties.MenuId.eq(menuItem.getId())).list();
                    if (list.size() > 0) {
                        Location location = list.get(0);
                        displayFragment(IndividualHallFragment.newInstance(
                                location.getId(), ""));
                    }
                }
                break;
            case PageType.SPONSOR:
                displayFragment(SponsorListFragment.newInstance(menuItem.getId()));
                break;
            case PageType.CONFERENCE:
                displayFragment(new ConferenceCategoriesFragment());
                break;
            case PageType.EVENT:
                displayFragment(new EventCategoriesFragment());
                break;
            case "Innovation Show":
                displayFragment(new ProductFragment());
                break;
            case "Partner Country":
                displayFragment(ExhibitorListFragment
                        .newInstance(ExhibitorListFragment.FILTER_BY_COUNTRY, "ARGENTINA"));
                break;
            case PageType.SPEAKER:
                displayFragment(new SpeakerListFragment());
                break;
            case PageType.WEBLINK:
                intent = new Intent(MainActivity.this, WebActivity.class);
                intent.putExtra("url", menuItem.getWebLink());
                startActivity(intent, translateBundle);
                break;
            case PageType.INBOX:
                displayFragment(new InboxListFragment());
                break;
            case PageType.BOOKMARK:
                displayFragment(new BookmarkListFragment());
                break;
            case PageType.LIVE_QA:
                displayFragment(new LiveQAListFragment());
                break;
            case PageType.POLLING:
                displayFragment(new PollingListFragment());
                break;
            case PageType.NEWS:
                displayFragment(new NewsListFragment());
                break;
            case PageType.NEWSLETTERS:
                break;
            case PageType.MOMENTS:
                break;
            case PageType.E_POSTER:
                displayFragment(new EPosterCategoriesFragment());
                break;
            case PageType.SURVEY:
                displayFragment(new SurveyFragment());
                break;
            case PageType.LOGOUT:
                logout();
                break;
            case PageType.BLANK:
                break;
            case PageType.UPDATE:
                intent = new Intent(MainActivity.this, DownloadActivity.class);
                startActivityForResult(intent, REQUEST_DOWNLOAD, translateBundle);
                break;
            default:
                break;
        }

        if (isPageTypeForDisplay(pageType)) {
            currentFragmentName = menuItem.getEnTitle();
        }

        // if is a group with children, open drawer to expand child list, else close drawer
        if (menuAdapter.groupHasChildren(menuItem)) {
            drawer.openDrawer(START);

            collapseMenu();

            flatPosition = menuAdapter.getGroupPosition(menuItem);

            menuListView.smoothScrollToPosition(flatPosition);

            int groupPosition = menuAdapter.getGroupPosition(menuItem);
            expandMenu(groupPosition);

        } else {
            drawer.closeDrawer(START);
        }

        selectedPosition = flatPosition;
        menuListView.setItemChecked(selectedPosition, true);

    }

    private boolean isPageTypeForDisplay(@PageType String pageType) {
        return !pageType.equals(PageType.WEBLINK) && !pageType.equals(PageType.UPDATE)
                && !pageType.equals(PageType.LOGOUT) && !pageType.equals(PageType.BLANK);
    }

    private void updateData() {
        SharedPreferences sharedPreferences = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
        long updatedAt = sharedPreferences.getLong(KEY_UPDATED_AT, 0);
        if (currentTimeMillis() - updatedAt > ONE_HOUR) {
            this.getLifecycle().addObserver(new DownloadTaskManager.DownloadLifecycleObserver());

            Call<RetrofitResponse<DataVersion>> call = Injector.get().apiService().getDataVersions("en");
            call.enqueue(new Callback<RetrofitResponse<DataVersion>>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<RetrofitResponse<DataVersion>> call,
                                       @NonNull Response<RetrofitResponse<DataVersion>> response) {
                    if (response.isSuccessful() && response.body().getStatus().equals("200")) {
                        ArrayList<DataVersion> dataVersions = response.body().getData();

                        Map<String, String> dataVersionMap = new HashMap<>();
                        for (DataVersion dataVersion : dataVersions) {
                            dataVersionMap.put(dataVersion.getPage(), dataVersion.getDataVersion());
                        }

                        DownloadTaskManager.getInstance()
                                .startDownloadTasks(dataVersionMap, MainActivity.this, false);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RetrofitResponse<DataVersion>> call,
                                      @NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onProgressUpdate(int currentProgress) {
        //do nothing
    }

    @Override
    public void onProgressComplete() {
        SharedPreferences sharedPreferences = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
        sharedPreferences.edit().putLong(KEY_UPDATED_AT, currentTimeMillis()).apply();
    }

    @Override
    public void onError() {
        //do nothing
    }

    private void initTopBanner() {
        topBanner = findViewById(R.id.top_banner);
        ConfigurationImages image = daoSession.getConfigurationImagesDao().queryBuilder()
                .where(ConfigurationImagesDao.Properties.Key.eq(ImageKey.TOP_BANNER))
                .unique();
        if (image == null) return;
        Glide.with(this)
                .load(image.getFile())
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.holder_top_banner))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        int bannerHeight = SizeConverter.dipToPixel(120, MainActivity.this);
                        setHeights(bannerHeight);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model,
                                                   Target<Drawable> target, DataSource dataSource,
                                                   boolean isFirstResource) {
                        int bannerHeight = resource.getIntrinsicHeight();
                        setHeights(bannerHeight);
                        return false;
                    }
                })
                .into(topBanner);
        topBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnHome();
            }
        });
    }

    private void initNavLogo() {
        navLogo = findViewById(R.id.nav_logo);
        navLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnHome();
                drawer.closeDrawer(Gravity.START);
            }
        });
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            Drawable drawable = VectorUtil
                    .getVector(R.drawable.ic_menu, MainActivity.this);
            getSupportActionBar().setHomeAsUpIndicator(drawable);

            ConfigurationImages configurationImage = daoSession.getConfigurationImagesDao()
                    .queryBuilder()
                    .where(ConfigurationImagesDao.Properties.Key.eq(ImageKey.SIDE_MENU_ICON))
                    .unique();
            if (configurationImage != null) {
                String sideIconUrl = configurationImage.getUrl();
                final int dimension = SizeConverter.dipToPixel(TOGGLE_ICON_SIZE, this);
                Glide.with(this)
                        .asDrawable()
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                        Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable drawable, Object model,
                                                           Target<Drawable> target,
                                                           DataSource dataSource,
                                                           boolean isFirstResource) {
                                getSupportActionBar().setHomeAsUpIndicator(drawable);
                                return true;
                            }
                        })
                        .load(sideIconUrl)
                        .submit(dimension, dimension);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private void initDrawer() {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
//        toggle.syncState();

    }

    private void initMenuList() {
        menuListView = findViewById(R.id.nav_menu);
        addVersionMenu();

        String role = UserManager.getInstance().getRole();
        ArrayList<Menu> menus = (ArrayList<Menu>) daoSession.getMenuDao().queryBuilder()
                .where(MenuDao.Properties.MenuType.like("%" + MenuType.SIDE_MENU))
                .where(MenuDao.Properties.Permission.like("%" + role + "%"))
                .list();

        Collections.sort(menus);

        if (BuildConfig.DEBUG) {
            menus.addAll(BootstrapHelper.debugMenus());
        }

        menuAdapter = new MenuAdapter(this, menus);

        menuListView.setAdapter(menuAdapter);

        menuListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // if is group with children, then expand or collapse, else select
                if (menuAdapter.groupHasChildren(groupPosition) && menuListView.isGroupExpanded(groupPosition)) {
                    menuListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    int flatPosition = menuListView.getFlatListPosition(
                            ExpandableListView.getPackedPositionForGroup(groupPosition));
                    selectedMenu = menuAdapter.getGroup(groupPosition);
                    onMenuItemSelected(selectedMenu, flatPosition);
                }

                return true;
            }
        });

        menuListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {
                int flatPosition = menuListView.getFlatListPosition(
                        ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
                selectedMenu = menuAdapter.getChild(groupPosition, childPosition);
                onMenuItemSelected(selectedMenu, flatPosition);
                return true;
            }
        });
    }

    private void initSpinner() {
        final ArrayList<Language> langs = new ArrayList<>();
        langs.add(Language.English);
        langs.add(Language.Chinese);

        Spinner langSpinner = findViewById(R.id.lang_spinner);
        langSpinner.setVisibility(View.VISIBLE);
        langSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!UserManager.getInstance().getLanguage().equals(langs.get(position))) {
                    UserManager.getInstance().setLanguage(langs.get(position), MainActivity.this);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerAdapter spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, langs);
        langSpinner.setAdapter(spinnerAdapter);

        if (UserManager.getInstance().getLanguage().equals(Language.English)) {
            langSpinner.setSelection(0);
        } else {
            langSpinner.setSelection(1);
        }
    }

    private void initBottomBanner() {
        bottomBanner = findViewById(R.id.bottom_banner);

        ArrayList<BottomBanner> banners = (ArrayList<BottomBanner>) daoSession.getBottomBannerDao().loadAll();

        BaseBannerAdapter<BottomBanner> adapter = new BottomBannerAdapter(this, banners);

        if (banners.size() > 0) {
            adapter.setInfiniteLoop(true);
            bottomBanner.setVisibility(View.VISIBLE);
        }

        bottomBanner.setAdapter(adapter);
        if (banners.size() > 1) {
            bottomBanner.setInterval(5000);
            bottomBanner.startAutoScroll();
        }
    }

    /**
     * Display fragment by replace the current one, if same fragment then do nothing
     *
     * @param fragment fragment to change to
     */
    public void displayFragment(Fragment fragment) {
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.fragment_container);
        // if same fragment or is a child menu, then change
        if (!fragment.getClass().toString().equals(currentFragment.getTag())
                || !selectedMenu.getParentId().equals("")) {
            if (homeFragment == null) homeFragment = new HomeFragment();
            fragmentManager.popBackStack(null, POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out,
                            R.anim.push_right_in, R.anim.push_right_out)
                    .replace(R.id.fragment_container, fragment, fragment.getClass().toString())
                    .addToBackStack(currentFragment.getClass().toString())
                    .commit();
        }
    }

    private void setHeights(int bannerHeight) {
        ViewGroup.LayoutParams layoutParams = navLogo.getLayoutParams();
        layoutParams.height = bannerHeight;
        navLogo.setLayoutParams(layoutParams);

        ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) menuListView.getLayoutParams();
        marginParams.topMargin = bannerHeight;
        menuListView.setLayoutParams(marginParams);
    }

    private void returnHome() {
        if (homeFragment == null) {
            homeFragment = new HomeFragment();
        }

        fragmentManager.popBackStack(null, POP_BACK_STACK_INCLUSIVE);

        int checkedPosition = menuListView.getCheckedItemPosition();
        if (checkedPosition != -1) {
            menuListView.setItemChecked(checkedPosition, false);
        }

        collapseMenu();

        selectedMenu = null;
        selectedPosition = -1;
    }

    private void addVersionMenu() {
        String version = "V " + BuildConfig.VERSION_NAME;

        View footer = LayoutInflater.from(this).inflate(R.layout.footer_version, menuListView, false);

        TextView versionTextView = footer.findViewById(R.id.version_text);
        versionTextView.setText(version);

        menuListView.addFooterView(footer);
    }

    public void collapseMenu() {
        if (lastExpandedPosition != -1) {
            menuListView.collapseGroup(lastExpandedPosition);
            lastExpandedPosition = -1;
        }
    }

    public void expandMenu(int groupPosition) {
        menuListView.expandGroupWithAnimation(groupPosition);
        lastExpandedPosition = groupPosition;
    }

    private void logout() {
        UserManager.getInstance().clear();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkVersion() {
        Call<RetrofitResponse<Configuration>> call = Injector.get().apiService().getConfigurations("en");
        call.enqueue(new Callback<RetrofitResponse<Configuration>>() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onResponse(@NonNull Call<RetrofitResponse<Configuration>> call,
                                   @NonNull Response<RetrofitResponse<Configuration>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getData() != null) {
                    Configuration configuration = response.body().getData().get(0);
                    if (configuration.isNewerVersion()) {
                        showCheckVersionDialog(configuration);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RetrofitResponse<Configuration>> call, @NonNull Throwable t) {
            }
        });
    }

    private void showCheckVersionDialog(final Configuration configuration) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog alertDialog = builder
                .setMessage(R.string.dialog_title_new_version)
                .setPositiveButton(R.string.btn_update, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!TextUtils.isEmpty(configuration.getAndroidDownloadLink())) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(configuration.getAndroidDownloadLink()));
                            startActivity(intent);
                        }
                        finish();
                    }
                })
                .create();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onMenuTap(Menu item) {
        collapseMenu();

        int groupPosition = menuAdapter.getGroupPosition(item);
        int flatPosition = menuListView.getFlatListPosition(ExpandableListView.
                getPackedPositionForGroup(groupPosition));

        selectedMenu = item;
        onMenuItemSelected(item, flatPosition);
    }

    public void registerMessageReceiver() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(MESSAGE_RECEIVED_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);
    }

    public class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                    String message = intent.getStringExtra(KEY_MESSAGE);
                    String extras = intent.getStringExtra(KEY_EXTRAS);

                    if (BuildConfig.DEBUG) Log.e(TAG, "onReceive: " + message + "\n" + extras);
                    Gson gson = new Gson();
                    JsonObject object = gson.fromJson(extras, JsonObject.class);

                    String type = object.get("type").getAsString();
                    String id = object.get("id").getAsString();

                    if (type.equals("event")) {
                        displayFragment(EventFragment.newInstance(id));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
