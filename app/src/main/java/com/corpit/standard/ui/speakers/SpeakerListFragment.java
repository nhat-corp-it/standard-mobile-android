package com.corpit.standard.ui.speakers;


import android.databinding.ViewDataBinding;

import com.corpit.standard.R;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.model.SpeakerDao;
import com.corpit.standard.ui.base.BaseListFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import org.greenrobot.greendao.query.Query;

public class SpeakerListFragment extends BaseListFragment<Speaker>
        implements ItemClickListener<Speaker> {

    public SpeakerListFragment() {
        // Required empty public constructor
    }

    @Override
    protected String getTitle() {
        return getCurrentFragmentName();
    }

    @Override
    protected StandardBoundAdapter<Speaker, ? extends ViewDataBinding> getAdapter() {
        return new SpeakerAdapter(R.layout.row_speaker, this);
    }

    @Override
    protected Query<Speaker> fetchData(String query) {
        return daoSession.getSpeakerDao()
                .queryBuilder()
                .where(SpeakerDao.Properties.FullName.like("%" + query + "%"))
                .orderAsc(SpeakerDao.Properties.FullName)
                .build();
    }

    @Override
    protected Query<Speaker> fetchData() {
        return daoSession.getSpeakerDao()
                .queryBuilder()
                .orderAsc(SpeakerDao.Properties.FullName)
                .build();
    }

    @Override
    public void onClick(Speaker speaker) {
        gotoFragment(SpeakerFragment.newInstance(speaker.getId()));
    }

}
