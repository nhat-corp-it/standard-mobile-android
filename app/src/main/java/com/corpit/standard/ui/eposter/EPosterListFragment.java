package com.corpit.standard.ui.eposter;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.corpit.standard.R;
import com.corpit.standard.model.EPoster;
import com.corpit.standard.model.EPosterDao;
import com.corpit.standard.ui.base.BaseListFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EPosterListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EPosterListFragment extends BaseListFragment<EPoster>
        implements ItemClickListener<EPoster> {

    public static final int FILTER_BY_ALL = 0;
    public static final int FILTER_BY_CATEGORY = 1;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int filterBy;
    private String param;


    public EPosterListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param filterBy Parameter 1.
     * @param param    Parameter 2.
     * @return A new instance of fragment EPosterListFragment.
     */
    public static EPosterListFragment newInstance(int filterBy, String param) {
        EPosterListFragment fragment = new EPosterListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, filterBy);
        args.putString(ARG_PARAM2, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            filterBy = getArguments().getInt(ARG_PARAM1);
            param = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    protected String getTitle() {
        return getCurrentFragmentName();
    }

    @Override
    protected StandardBoundAdapter<EPoster, ? extends ViewDataBinding> getAdapter() {
        return new EPosterAdapter(R.layout.row_eposter, this);
    }

    @Override
    protected Query<EPoster> fetchData(String query) {
        QueryBuilder<EPoster> builder = daoSession.getEPosterDao().queryBuilder();

        if (filterBy == FILTER_BY_CATEGORY) {
            builder.where(EPosterDao.Properties.Category.eq(param));
        }

        if (!TextUtils.isEmpty(query)) {
            builder.where(EPosterDao.Properties.Title.like("%" + query + "&"));
        }

        builder.orderAsc(EPosterDao.Properties.Author);

        return builder.build();
    }

    @Override
    protected Query<EPoster> fetchData() {
        return fetchData("");
    }

    @Override
    public void onClick(EPoster item) {
        gotoFragment(EPosterFragment.newInstance(item.getId()));
    }

}
