package com.corpit.standard.ui.eposter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.databinding.TabAbstractBinding;
import com.corpit.standard.model.EPoster;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class AbstractTab extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";

    private String eposterId;

    public AbstractTab() {
        // Required empty public constructor
    }

    public static AbstractTab newInstance(String eposterId) {
        AbstractTab fragment = new AbstractTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, eposterId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eposterId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TabAbstractBinding binding = TabAbstractBinding.inflate(inflater,
                container, false);

        EPoster ePoster = daoSession.getEPosterDao().load(eposterId);

        binding.setEposter(ePoster);

        return binding.getRoot();
    }

}
