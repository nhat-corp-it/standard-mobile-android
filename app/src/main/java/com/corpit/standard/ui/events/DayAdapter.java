package com.corpit.standard.ui.events;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.DaoSession;

import java.util.ArrayList;

public class DayAdapter extends RecyclerView.Adapter<DayAdapter.DayHolder> {

    private ArrayList<String> mValues;
    private OnDaySelectListener mListener;
    private DaoSession daoSession;

    private int selectedPosition = 0;

    public DayAdapter(ArrayList<String> values, OnDaySelectListener listener) {
        mValues = values;
        mListener = listener;
        this.daoSession = Injector.get().daoSession();
    }

    @Override
    public DayHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_day, parent, false);
        return new DayHolder(view);
    }

    @Override
    public void onBindViewHolder(DayHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mDay.setText(holder.mItem);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity(holder.mView.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int tabsOnScreen = mValues.size() > 3 ? 3 : mValues.size();
        holder.mDay.setWidth(displayMetrics.widthPixels / tabsOnScreen);

        ColorScheme colorScheme = daoSession.getColorSchemeDao().loadAll().get(0);

        holder.mView.setBackgroundColor(selectedPosition == position
                ? colorScheme.getColorButton1Code()
                : colorScheme.getColorButton2Code());
    }

    private Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class DayHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final View mView;
        final TextView mDay;
        String mItem;

        DayHolder(View view) {
            super(view);
            mView = view;
            mDay = mView.findViewById(android.R.id.text1);
            mView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selectedPosition);
            selectedPosition = getAdapterPosition();
            notifyItemChanged(selectedPosition);
            mListener.onDaySelected(mItem);
        }
    }

    public interface OnDaySelectListener {
        void onDaySelected(String day);
    }
}
