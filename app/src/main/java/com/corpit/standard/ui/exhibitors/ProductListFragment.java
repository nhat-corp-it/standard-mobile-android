package com.corpit.standard.ui.exhibitors;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.model.Product;
import com.corpit.standard.model.ProductDao;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.StringArrayAdapter;

import java.util.ArrayList;
import java.util.Collections;

import static com.corpit.standard.ui.exhibitors.ExhibitorListFragment.FILTER_BY_PRODUCT;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductListFragment extends BaseFragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String mainProductId;

    public ProductListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProductListFragment.
     */
    public static ProductListFragment newInstance(String subCategory) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, subCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mainProductId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_product_list, container, false);

        ((TextView) root.findViewById(R.id.title)).setTextColor(colorScheme.getColorFont3Code());

        ListView productListView = root.findViewById(R.id.product_list);

        ArrayList<Product> products = (ArrayList<Product>) daoSession.getProductDao().queryBuilder()
                .where(ProductDao.Properties.MainProductId.eq(mainProductId)).list();
        Collections.sort(products);

        ArrayList<String> productNames = new ArrayList<>();
        for (Product product : products) {
            productNames.add(product.getName());
        }

        final StringArrayAdapter adapter = new StringArrayAdapter(getContext(), productNames);

        productListView.setAdapter(adapter);

        productListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoFragment(ExhibitorListFragment.newInstance(
                        FILTER_BY_PRODUCT, adapter.getItem(position)));
            }
        });

        return root;
    }

}
