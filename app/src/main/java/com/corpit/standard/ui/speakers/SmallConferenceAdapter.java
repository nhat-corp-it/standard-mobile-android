package com.corpit.standard.ui.speakers;

import com.corpit.standard.databinding.RowConferenceSmallBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Booth;
import com.corpit.standard.model.Conference;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

public class SmallConferenceAdapter extends StandardBoundAdapter<Conference, RowConferenceSmallBinding> {

    private DaoSession daoSession;

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId The layout to be used for items. It must use data binding.
     * @param listener Basic item click listener
     */
    SmallConferenceAdapter(int layoutId, ItemClickListener<Conference> listener) {
        super(layoutId, listener);
        this.daoSession = Injector.get().daoSession();
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowConferenceSmallBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);
        Booth booth = daoSession.getBoothDao().load(getList().get(holder.getAdapterPosition()).getBoothId());
        holder.binding.setBooth(booth);
    }
}
