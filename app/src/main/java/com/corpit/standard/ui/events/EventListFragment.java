package com.corpit.standard.ui.events;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.data.local.DaoHelper;
import com.corpit.standard.model.Event;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventListFragment extends BaseFragment
        implements ItemClickListener<Event>, DayAdapter.OnDaySelectListener {

    public static final int FILTER_BY_DATE = 0;
    public static final int FILTER_BY_TRACK = 1;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int filterBy;
    private String param;


    private RecyclerView eventListView;
    private EventAdapter eventAdapter;

    private RecyclerView dayListView;


    public EventListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EventListFragment.
     */
    public static EventListFragment newInstance(int filterBy, String param) {
        EventListFragment fragment = new EventListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, filterBy);
        args.putString(ARG_PARAM2, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            filterBy = getArguments().getInt(ARG_PARAM1);
            param = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_event_list, container, false);

        eventListView = root.findViewById(R.id.event_list);

        dayListView = root.findViewById(R.id.day_list);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(dayListView.getContext(),
                DividerItemDecoration.HORIZONTAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        dayListView.addItemDecoration(dividerItemDecoration);

        loadFromDb();

        return root;
    }

    private void loadFromDb() {
        ArrayList<String> days = new ArrayList<>();
        switch (filterBy) {
            case FILTER_BY_DATE:
                days = DaoHelper.getEventDays();
                break;
            case FILTER_BY_TRACK:
                days = DaoHelper.getEventDays(param);
                break;
            default:
                break;
        }
        DayAdapter dayAdapter = new DayAdapter(days, this);
        dayListView.setAdapter(dayAdapter);

        eventAdapter = new EventAdapter(R.layout.row_event, this);
        eventListView.setAdapter(eventAdapter);

        if (!days.isEmpty()) {
            onDaySelected(days.get(0));
        }
    }

    @Override
    public void onClick(Event event) {
        gotoFragment(EventFragment.newInstance(event.getId()));
    }

    @Override
    public void onDaySelected(String day) {
        switch (filterBy) {
            case FILTER_BY_DATE:
                eventAdapter.setList(DaoHelper.getEvents(day));
                break;
            case FILTER_BY_TRACK:
                eventAdapter.setList(DaoHelper.getEvents(day, param));
                break;
        }
    }

}
