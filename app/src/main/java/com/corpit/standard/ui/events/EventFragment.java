package com.corpit.standard.ui.events;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.databinding.FragmentEventBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Booth;
import com.corpit.standard.model.Event;
import com.corpit.standard.ui.base.BaseFragment;

public class EventFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String eventId;
    private Event event;

    public EventFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param eventId Parameter 1.
     * @return A new instance of fragment EventFragment.
     */
    public static EventFragment newInstance(String eventId) {
        EventFragment fragment = new EventFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, eventId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentEventBinding binding = FragmentEventBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        event = daoSession.getEventDao().load(eventId);

        binding.setEvent(event);

        final Booth booth = daoSession.getBoothDao().load(event.getBoothId());

        binding.setBooth(booth);

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Injector.get().eventTracker().trackEventSchedule(event);
    }
}
