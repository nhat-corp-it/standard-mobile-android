package com.corpit.standard.ui.base;

import android.view.View;

public interface OnTransitionItemListener<T> {

    void onItemSelected(T item, View view);
}
