package com.corpit.standard.ui.base;

public interface ItemClickListener<T> {

    void onClick(T item);
}
