package com.corpit.standard.ui.exhibitors;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.model.BoothDao;
import com.corpit.standard.model.ExhibitingCompany;
import com.corpit.standard.model.ExhibitingCompanyDao;
import com.corpit.standard.ui.base.BaseListFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.greenrobot.greendao.query.WhereCondition.StringCondition;

import static com.corpit.standard.model.BoothDao.Properties.Id;
import static com.corpit.standard.model.BoothDao.Properties.Name;
import static com.corpit.standard.model.ExhibitingCompanyDao.Properties;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExhibitorListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExhibitorListFragment extends BaseListFragment<ExhibitingCompany>
        implements ItemClickListener<ExhibitingCompany> {

    public static final int FILTER_BY_A_Z = 0;
    public static final int FILTER_BY_COUNTRY = 1;
    public static final int FILTER_BY_HALL = 2;
    public static final int FILTER_BY_PRODUCT = 3;
    public static final int FILTER_BY_SEGMENT = 4;
    public static final int FILTER_BY_TREND = 5;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int filterBy;
    private String param;

    public ExhibitorListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ExhibitorListFragment.
     */
    public static ExhibitorListFragment newInstance(int filterBy, String param) {
        ExhibitorListFragment fragment = new ExhibitorListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, filterBy);
        args.putString(ARG_PARAM2, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            filterBy = getArguments().getInt(ARG_PARAM1);
            param = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    protected void setUpListBy(TextView listByText) {
        listByText.setVisibility(View.VISIBLE);
        listByText.setText(listBy());
    }

    private String listBy() {
        switch (filterBy) {
            case FILTER_BY_A_Z:
                return getResources().getString(R.string.category_all);
            case FILTER_BY_HALL:
                if (param.equals("Overview")) {
                    return getResources().getString(R.string.category_all);
                }
            case FILTER_BY_COUNTRY:
            case FILTER_BY_PRODUCT:
            case FILTER_BY_SEGMENT:
            case FILTER_BY_TREND:
                return param;
            default:
                return getResources().getString(R.string.category_all);
        }
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected StandardBoundAdapter<ExhibitingCompany, ? extends ViewDataBinding> getAdapter() {
        return new ExhibitorAdapter(R.layout.row_exhibitor, this);
    }

    @Override
    protected Query<ExhibitingCompany> fetchData(String query) {
        QueryBuilder<ExhibitingCompany> builder = daoSession.getExhibitingCompanyDao().queryBuilder();

        switch (filterBy) {
            case FILTER_BY_COUNTRY:
                builder.where(Properties.Country.eq(param));
                break;
            case FILTER_BY_HALL:
                if (!param.equals("Overview")) {
                    builder.where(Properties.Hall.eq(param));
                }
                break;
            case FILTER_BY_PRODUCT:
                builder.where(Properties.Product.like("%" + param + "%"));
                break;
            case FILTER_BY_SEGMENT:
                builder.where(Properties.MarketSegment.like(("%" + param + "%")));
                break;
            case FILTER_BY_TREND:
                builder.where(Properties.Trends.like(("%" + param + "%")));
                break;
            default:
                break;
        }

        if (!TextUtils.isEmpty(query)) {
            //The condition in which the booth name contains the query
            StringCondition condition = new StringCondition("T.\"BOOTH_ID\" IN " +
                    "(SELECT " + BoothDao.TABLENAME + "." + Id.columnName + " FROM " +
                    BoothDao.TABLENAME + " JOIN " + ExhibitingCompanyDao.TABLENAME +
                    " ON " + BoothDao.TABLENAME + "." + Id.columnName + " = " +
                    ExhibitingCompanyDao.TABLENAME + "." + Properties.BoothId.columnName +
                    " AND " + BoothDao.TABLENAME + "." + Name.columnName + " LIKE '%" + query + "%')");

            builder.whereOr(Properties.Name.like("%" + query + "%"), condition);
        }

        builder.orderAsc(Properties.Name);

        return builder.build();
    }

    @Override
    protected Query<ExhibitingCompany> fetchData() {
        return fetchData("");
    }

    @Override
    public void onClick(ExhibitingCompany exhibitor) {
        gotoFragment(ExhibitorFragment.newInstance(exhibitor.getId(), false));
    }

}
