package com.corpit.standard.ui.base;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.R;
import com.corpit.standard.share.UserManager;
import com.corpit.standard.widget.StandardWebViewClient;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.MessageFormat;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WebViewDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WebViewDetailFragment extends Fragment {

    @StringDef({WebType.POLLING, WebType.LIVE_QA})
    @Retention(RetentionPolicy.SOURCE)
    public @interface WebType {
        String POLLING = "Polling";
        String LIVE_QA = "LiveQA";
    }

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String conferenceId;
    @WebType
    private String type;

    private WebView webView;

    public WebViewDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param conferenceId Parameter 1.
     * @return A new instance of fragment WebViewDetailFragment.
     */
    public static WebViewDetailFragment newInstance(String conferenceId, @WebType String type) {
        WebViewDetailFragment fragment = new WebViewDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, conferenceId);
        args.putString(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            conferenceId = getArguments().getString(ARG_PARAM1);
            type = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_polling_detail, container, false);

        webView = root.findViewById(R.id.web_view);

        return root;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new StandardWebViewClient());
    }

    @Override
    public void onStart() {
        super.onStart();

        String userId;
        if (TextUtils.isEmpty(UserManager.getInstance().getUserId())) {
            userId = UUID.randomUUID().toString();
        } else {
            userId = UserManager.getInstance().getUserId();
        }
        //Eg: http://203.127.83.146/mobileotcasia2018/apiv1/Polling?SessionID=RID37690&UserID=123
        String url = MessageFormat.format("{0}{1}?SessionID={2}&UserID={3}",
                BuildConfig.API_ROOT_URL, type, conferenceId, userId);

        if (BuildConfig.DEBUG) {
            url = MessageFormat.format(
                    "http://203.127.83.146/mobileotcasia2018/apiv1/{0}?SessionID={1}&UserID={2}",
                    type, conferenceId, userId);
        }

        webView.loadUrl(url);
    }
}
