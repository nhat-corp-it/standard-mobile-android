package com.corpit.standard.ui.survey;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.databinding.FragmentSurveyBinding;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SurveyFragment extends BaseFragment {


    public SurveyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentSurveyBinding binding = FragmentSurveyBinding.inflate(inflater, container, false);
        binding.setColorScheme(colorScheme);

        binding.title.setText(getCurrentFragmentName());

        return binding.getRoot();
    }

}
