package com.corpit.standard.ui.eposter;

import com.corpit.standard.databinding.RowEposterBinding;
import com.corpit.standard.model.EPoster;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

public class EPosterAdapter extends StandardBoundAdapter<EPoster, RowEposterBinding> {

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId    The layout to be used for items. It must use data binding.
     * @param listener    Basic item click listener
     */
    EPosterAdapter(int layoutId, ItemClickListener<EPoster> listener) {
        super(layoutId, listener);
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowEposterBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);

        boolean hideCategory = holder.getAdapterPosition() > 0
                && getList().get(holder.getAdapterPosition() - 1).getSectionName()
                .equals(getList().get(holder.getAdapterPosition()).getSectionName());

        holder.binding.setHideCategory(hideCategory);
    }
}
