package com.corpit.standard.ui.generalinfo;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.GeneralInfo;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GeneralInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GeneralInfoFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String infoId;
    private GeneralInfo info;

    public GeneralInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GeneralInfoFragment.
     */
    public static GeneralInfoFragment newInstance(String infoId) {
        GeneralInfoFragment fragment = new GeneralInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, infoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            infoId = getArguments().getString(ARG_PARAM1);
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_general_info, container, false);

        info = daoSession.getGeneralInfoDao().load(infoId);

        WebView webView = root.findViewById(R.id.web_view);

        StringBuilder html = new StringBuilder();
        html.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><style type='text/css'>")
                .append("@font-face {font-family: MyFont;src: url('file:///android_asset/Belleza-Regular.ttf')}")
                .append("body {font-family: MyFont;font-size: medium;text-align: justify;}</style></head><body>");

        int colorFont3Code = colorScheme.getColorFont3Code();
        int a = (colorFont3Code >> 24) & 0xFF;
        int r = (colorFont3Code >> 16) & 0xFF;
        int g = (colorFont3Code >> 8) & 0xFF;
        int b = colorFont3Code & 0xFF;
        String hexColor = String.format("rgba(%d, %d, %d, %d)", r, g, b, a);

        String title = String.format("<p style=\"font-size:25px; color:%s\">%s</p>",
                hexColor, info.getTitle());

        String content = title + info.getContent();

        content = content
                .replaceAll(" width: [0-9]*px;", "")
                .replaceAll("font-family:[^;]*;", "")
                .replaceAll("14px", "18px")
                .replaceAll("13px", "18px")
                .replaceAll("12px", "18px")
                .replaceAll("background-color:[^;]*;", "");


        html.append(content)
                .append("</body>")
                .append("</html>");

        webView.loadDataWithBaseURL(null, html.toString(), "text/html",
                "utf-8", null);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Injector.get().eventTracker().trackGeneralInformation(info);
    }
}
