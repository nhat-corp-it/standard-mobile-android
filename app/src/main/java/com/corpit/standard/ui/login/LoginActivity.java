package com.corpit.standard.ui.login;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.R;
import com.corpit.standard.model.BottomBanner;
import com.corpit.standard.share.UserManager;
import com.corpit.standard.share.UserManager.Language;
import com.corpit.standard.ui.base.BaseBannerAdapter;
import com.corpit.standard.ui.base.ParentActivity;
import com.corpit.standard.ui.home.BottomBannerAdapter;

import java.util.ArrayList;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class LoginActivity extends ParentActivity {

    public static final String KEY_LOCALE = "locale";

    private AutoScrollViewPager bottomBanner;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (savedInstanceState != null) {
            String locale = savedInstanceState.getString(KEY_LOCALE);
            Log.e(TAG, "onCreate: locale" + locale);
        }

        initBottomBanner();

        if (BuildConfig.MULTIPLE_LANGUAGES) {
            initSpinner();
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new ExhibitorLoginFragment())
                .commit();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_LOCALE, UserManager.getInstance().getLanguage().locale);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bottomBanner.stopAutoScroll();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomBanner.startAutoScroll();
    }

    private void initBottomBanner() {
        bottomBanner = findViewById(R.id.bottom_banner);

        ArrayList<BottomBanner> banners = (ArrayList<BottomBanner>) daoSession.getBottomBannerDao().loadAll();

        BaseBannerAdapter<BottomBanner> adapter = new BottomBannerAdapter(this, banners);

        if (banners.size() != 0) {
            adapter.setInfiniteLoop(true);
        }

        bottomBanner.setAdapter(adapter);

        bottomBanner.setInterval(5000);
        bottomBanner.startAutoScroll();
    }

    private void initSpinner() {
        final ArrayList<Language> langs = new ArrayList<>();
        langs.add(Language.English);
        langs.add(Language.Chinese);

        Spinner langSpinner = findViewById(R.id.lang_spinner);
        langSpinner.setVisibility(View.VISIBLE);
        langSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!UserManager.getInstance().getLanguage().equals(langs.get(position))) {
                    UserManager.getInstance().setLanguage(langs.get(position), LoginActivity.this);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerAdapter spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, langs);
        langSpinner.setAdapter(spinnerAdapter);

        if (UserManager.getInstance().getLanguage().equals(Language.English)) {
            langSpinner.setSelection(0);
        } else {
            langSpinner.setSelection(1);
        }

    }

}
