package com.corpit.standard.ui.inbox;

import android.graphics.drawable.GradientDrawable;

import com.corpit.standard.databinding.RowInboxBinding;
import com.corpit.standard.model.Inbox;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;
import com.corpit.standard.util.SizeConverter;

import java.util.List;

public class InboxAdapter extends StandardBoundAdapter<Inbox, RowInboxBinding> {

    private static final int BORDER_SIZE_DIP = 1;

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId The layout to be used for items. It must use data binding.
     * @param listener Basic item click listener
     */
    InboxAdapter(int layoutId, ItemClickListener<Inbox> listener) {
        super(layoutId, listener);
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowInboxBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);

        GradientDrawable background = (GradientDrawable) holder.binding.getRoot().getBackground();
        int borderSize = SizeConverter.dipToPixel(BORDER_SIZE_DIP, holder.binding.getRoot().getContext());
        background.setStroke(borderSize, getColorScheme().getColorMargin1Code());
    }
}
