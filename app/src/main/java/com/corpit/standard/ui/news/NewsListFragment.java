package com.corpit.standard.ui.news;


import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentNewsListBinding;
import com.corpit.standard.databinding.RowNewsBinding;
import com.corpit.standard.model.News;
import com.corpit.standard.model.NewsDao;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.ArrayList;
import java.util.Collections;

public class NewsListFragment extends BaseFragment implements ItemClickListener<News> {

    private StandardBoundAdapter<News, RowNewsBinding> newsAdapter;

    private ObservableBoolean isEmpty = new ObservableBoolean(true);

    public NewsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentNewsListBinding binding = FragmentNewsListBinding
                .inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        binding.setIsEmpty(isEmpty);

        binding.title.setText(getCurrentFragmentName());

        newsAdapter = new StandardBoundAdapter<>(R.layout.row_news, this);

        newsAdapter.setHasStableIds(true);

        binding.newsList.setAdapter(newsAdapter);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadDataFromDb();
    }

    private void loadDataFromDb() {
        ArrayList<News> newsList = (ArrayList<News>) daoSession.getNewsDao()
                .queryBuilder()
                .orderAsc(NewsDao.Properties.UpdatedDate)
                .list();
        Collections.sort(newsList);

        newsAdapter.setList(newsList);

        if (newsList.size() > 0) {
            isEmpty.set(false);
        }
    }

    @Override
    public void onClick(News item) {
        gotoFragment(NewsFragment.newInstance(item.getId()));
    }
}
