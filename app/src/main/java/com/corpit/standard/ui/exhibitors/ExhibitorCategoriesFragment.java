package com.corpit.standard.ui.exhibitors;


import android.animation.LayoutTransition;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentExhibitorCategoriesBinding;
import com.corpit.standard.model.Location;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.widget.FixListView;

import java.util.ArrayList;
import java.util.Collections;

import static com.corpit.standard.ui.exhibitors.ExhibitorListFragment.FILTER_BY_A_Z;

/**
 * Fragment that displays categories of exhibitor, before coming to the list of exhibitors.
 */
public class ExhibitorCategoriesFragment extends BaseFragment {

    private static final String[] TRADE_SHOW_CATEGORIES = {
            "THAIFEX COFFEE & TEA", "THAIFEX DRINKS", "THAIFEX FINE FOOD", "THAIFEX FOOD SERVICE",
            "THAIFEX FOOD TECHNOLOGY", "THAIFEX FROZEN FOOD", "THAIFEX FRUITS & VEGETABLES",
            "THAIFEX RICE", "THAIFEX SEAFOOD", "THAIFEX SWEETS & CONFECTIONERY",
            "ASSOCIATIONS, ORGANISATIONS, TRADE PRESS, SERVICES, IT"
    };

    private static final String[] SPECIAL_SHOW_CATEGORIES = {
            "THAIFEX HALAL MARKET", "THAIFEX ORGANIC MARKET", "THAIFEX FRANCHISE MARKET"
    };

    private FixListView hallListView;
    private FixListView segmentListView;
    private FixListView trendListView;

    public ExhibitorCategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentExhibitorCategoriesBinding binding = FragmentExhibitorCategoriesBinding.inflate(inflater,
                container, false);
        binding.setColorScheme(colorScheme);

        binding.title.setText(getCurrentFragmentName());

        binding.buttonList.getLayoutTransition()
                .enableTransitionType(LayoutTransition.CHANGING);

        hallListView = binding.hallList;
        initHallListView();
        segmentListView = binding.segmentList;
        initSegmentList();
        trendListView = binding.trendList;
        initTrendList();

        binding.byAZButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(ExhibitorListFragment.newInstance(FILTER_BY_A_Z, ""));
            }
        });

        binding.byCountryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(new CountryListFragment());
            }
        });

        binding.byHallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isVisible = hallListView.getVisibility() == View.VISIBLE;
                if (isVisible) {
                    hallListView.setVisibility(View.GONE);
                } else {
                    trendListView.setVisibility(View.GONE);
                    segmentListView.setVisibility(View.GONE);
                    hallListView.setVisibility(View.VISIBLE);
                    hallListView.clearFocus();
                    hallListView.requestFocus();
                }
            }
        });

        binding.byProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(new ProductCategoriesFragment());
            }
        });

        binding.bySegmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isVisible = segmentListView.getVisibility() == View.VISIBLE;
                if (isVisible) {
                    segmentListView.setVisibility(View.GONE);
                } else {
                    hallListView.setVisibility(View.GONE);
                    trendListView.setVisibility(View.GONE);
                    segmentListView.setVisibility(View.VISIBLE);
                    segmentListView.clearFocus();
                    segmentListView.requestFocus();
                }
            }
        });

        binding.byTrendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isVisible = trendListView.getVisibility() == View.VISIBLE;
                if (isVisible) {
                    trendListView.setVisibility(View.GONE);
                } else {
                    hallListView.setVisibility(View.GONE);
                    segmentListView.setVisibility(View.GONE);
                    trendListView.setVisibility(View.VISIBLE);
                    trendListView.clearFocus();
                    trendListView.requestFocus();
                }
            }
        });

        return binding.getRoot();
    }

    private void initHallListView() {
        ArrayList<Location> locations = (ArrayList<Location>) daoSession.getLocationDao().loadAll();
        Collections.sort(locations);

        ArrayList<String> halls = new ArrayList<>();
        for (Location location : locations) {
            halls.add(location.getName());
        }

        final ArrayAdapter<String> hallListAdapter = new ArrayAdapter<>(getContext(),
                R.layout.row_simple_item, android.R.id.text1, halls);

        hallListView.setAdapter(hallListAdapter);

        hallListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoFragment(ExhibitorListFragment.newInstance(
                        ExhibitorListFragment.FILTER_BY_HALL, hallListAdapter.getItem(position)));
            }
        });

    }

    private void initSegmentList() {
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                R.layout.row_simple_item, android.R.id.text1, TRADE_SHOW_CATEGORIES);
        segmentListView.setAdapter(adapter);
        segmentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoFragment(ExhibitorListFragment.newInstance(
                        ExhibitorListFragment.FILTER_BY_SEGMENT, adapter.getItem(position)));
            }
        });
    }

    private void initTrendList() {
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                R.layout.row_simple_item, android.R.id.text1, SPECIAL_SHOW_CATEGORIES);
        trendListView.setAdapter(adapter);
        trendListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoFragment(ExhibitorListFragment.newInstance(
                        ExhibitorListFragment.FILTER_BY_SEGMENT, adapter.getItem(position)));
            }
        });
    }

}
