package com.corpit.standard.ui.floorplans;


import android.annotation.SuppressLint;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Location;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.widget.PinView;
import com.davemorrissey.labs.subscaleview.ImageSource;

import java.io.File;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class OverallMapFragment extends BaseFragment {

    private PinView mapView;

    private PointF sCoord = new PointF();
    private Location floorplan;

    public OverallMapFragment() {
        // Required empty public constructor
    }

    private static Venue c1 = new Venue();
    private static Venue c2 = new Venue();
    private static Venue c3 = new Venue();
    private static Venue ih14 = new Venue();
    private static Venue ih58 = new Venue();
    private static ArrayList<Venue> venues = new ArrayList<>();

    static {
        c1.x = 781;
        c1.y = 875;
        c1.width = 133;
        c1.height = 108;
        c1.locationId = "LF1001";
        c2.x = 647;
        c2.y = 875;
        c2.width = 133;
        c2.height = 108;
        c2.locationId = "LF1002";
        c3.x = 510;
        c3.y = 875;
        c3.width = 133;
        c3.height = 108;
        c3.locationId = "LF1003";
        ih14.x = 509;
        ih14.y = 623;
        ih14.width = 205;
        ih14.height = 103;
        ih14.locationId = "LF1004";
        ih58.x = 287;
        ih58.y = 604;
        ih58.width = 221;
        ih58.height = 122;
        ih58.locationId = "LF1005";

        venues.add(c1);
        venues.add(c2);
        venues.add(c3);
        venues.add(ih14);
        venues.add(ih58);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_overall_map, container, false);

        floorplan = daoSession.getLocationDao().load("LF1006");

        sCoord.x = 0;
        sCoord.y = 0;

        ((TextView) root.findViewById(R.id.title)).setText(getCurrentFragmentName());

        initMapView(root);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Injector.get().eventTracker().trackFloorplan(floorplan);
    }

    private void initMapView(View root) {
        mapView = root.findViewById(R.id.overall_map);
        String url = floorplan.getUrl();
        Glide.with(getActivity()).downloadOnly().listener(new RequestListener<File>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<File> target, boolean isFirstResource) {
                if (e != null) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onResourceReady(File file, Object model, Target<File> target, DataSource dataSource, boolean isFirstResource) {
                String uri = file.getPath();
                mapView.setImage(ImageSource.uri(uri));
                return true;
            }
        }).load(url).submit();

        mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Venue venue : venues) {
                    if (sCoord.x >= venue.x
                            && sCoord.x <= (venue.x + venue.width)
                            && sCoord.y >= venue.y
                            && sCoord.y <= (venue.y + venue.height)) {

                        gotoFragment(IndividualHallFragment.newInstance(venue.locationId, ""));
//                        mapView.drawSelectedVenue(venue);
                    }
                }
            }
        });

        final GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (mapView.isReady()) {
                    sCoord = mapView.viewToSourceCoord(e.getX(), e.getY());
                }
                return true;
            }
        });

        mapView.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
    }

    public static class Venue {
        public int x;
        public int y;
        public int width;
        public int height;
        String locationId;
    }
}
