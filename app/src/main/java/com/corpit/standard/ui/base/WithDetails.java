package com.corpit.standard.ui.base;

public interface WithDetails {

    /**
     * @return the main text of the item
     */
    String getText();

    /**
     * Used to put items in sections
     * @return section name. Usually the first letter of the main text
     */
    String getSectionName();
}
