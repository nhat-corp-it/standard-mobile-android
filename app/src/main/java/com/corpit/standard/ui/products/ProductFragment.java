package com.corpit.standard.ui.products;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.model.InnovationShow;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.StringArrayAdapter;
import com.corpit.standard.ui.exhibitors.ExhibitorFragment;

import java.util.ArrayList;
import java.util.Collections;

public class ProductFragment extends BaseFragment {

    public ProductFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_innovation_show, container, false);

        TextView titleTextView = root.findViewById(R.id.title);
        titleTextView.setText(getCurrentFragmentName());

        final ArrayList<InnovationShow> innovationShows = (ArrayList<InnovationShow>) daoSession.getInnovationShowDao().loadAll();
        Collections.sort(innovationShows);

        ArrayList<String> productNames = new ArrayList<>();
        for (InnovationShow show : innovationShows) {
            productNames.add(show.getName());
        }

        final StringArrayAdapter adapter = new StringArrayAdapter(getContext(), productNames);

        ListView listView = root.findViewById(R.id.innovation_product_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String companyId = innovationShows.get(position).getExhibitingCompanyId();
                if (daoSession.getExhibitingCompanyDao().load(companyId) != null) {
                    gotoFragment(ExhibitorFragment.newInstance(companyId, false));
                }
            }
        });

        return root;
    }

}
