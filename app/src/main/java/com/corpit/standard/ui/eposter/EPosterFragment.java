package com.corpit.standard.ui.eposter;


import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentEposterBinding;
import com.corpit.standard.model.EPoster;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EPosterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EPosterFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String ePosterId;

    public EPosterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param eposterId Parameter 1.
     * @return A new instance of fragment EPosterFragment.
     */
    public static EPosterFragment newInstance(String eposterId) {
        EPosterFragment fragment = new EPosterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, eposterId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ePosterId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentEposterBinding binding = FragmentEposterBinding.inflate(inflater,
                container, false);
        binding.setColorScheme(colorScheme);

        EPoster ePoster = daoSession.getEPosterDao().load(ePosterId);

        binding.setEposter(ePoster);

        TabPagerAdapter adapter = new TabPagerAdapter(getChildFragmentManager());
        adapter.addTab(AuthorTab.newInstance(ePosterId), getResources().getString(R.string.tab_author));
        adapter.addTab(AbstractTab.newInstance(ePosterId), getResources().getString(R.string.tab_abstract));
        adapter.addTab(CommentsTab.newInstance(ePosterId), getResources().getString(R.string.tab_comments));

        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        ViewGroup viewGroup = (ViewGroup) binding.tabLayout.getChildAt(0);
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            StateListDrawable background = (StateListDrawable) viewGroup.getChildAt(i).getBackground();
            DrawableContainer.DrawableContainerState constantState = (DrawableContainer.DrawableContainerState) background.getConstantState();
            Drawable[] children = new Drawable[0];
            if (constantState != null) {
                children = constantState.getChildren();
            }
            ColorDrawable selectedItem = (ColorDrawable) children[0];
            selectedItem.setColor(colorScheme.getColorButton1Code());
            ColorDrawable unselectedItem = (ColorDrawable) children[1];
            unselectedItem.setColor(colorScheme.getColorButton2Code());
        }

        return binding.getRoot();
    }

}
