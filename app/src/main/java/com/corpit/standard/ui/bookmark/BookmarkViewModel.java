package com.corpit.standard.ui.bookmark;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;

import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Bookmark;
import com.corpit.standard.model.BookmarkDao;

import java.util.Collections;

public class BookmarkViewModel extends AndroidViewModel {

    public final ObservableList<Bookmark> bookmarks = new ObservableArrayList<>();

    public final ObservableBoolean isEmpty = new ObservableBoolean(true);

    private BookmarkDao bookmarkDao;

    BookmarkViewModel(@NonNull Application application) {
        super(application);
        bookmarkDao = Injector.get().daoSession().getBookmarkDao();
    }

    public void loadBookmarks() {
        bookmarks.clear();
        bookmarks.addAll(bookmarkDao.loadAll());
        Collections.sort(bookmarks);
        isEmpty.set(bookmarks.size() == 0);
    }
}
