package com.corpit.standard.ui.inbox;


import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentInboxListBinding;
import com.corpit.standard.model.Inbox;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;

import java.util.ArrayList;
import java.util.Collections;

public class InboxListFragment extends BaseFragment implements ItemClickListener<Inbox> {

    private InboxAdapter inboxAdapter;

    private ObservableBoolean isEmpty = new ObservableBoolean(true);

    public InboxListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentInboxListBinding binding = FragmentInboxListBinding
                .inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        binding.setIsEmpty(isEmpty);

        binding.title.setText(getCurrentFragmentName());

        inboxAdapter = new InboxAdapter(R.layout.row_inbox, this);
        inboxAdapter.setHasStableIds(true);
        binding.inboxList.setAdapter(inboxAdapter);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadDataFromDb();
    }

    private void loadDataFromDb() {
        ArrayList<Inbox> inboxList = (ArrayList<Inbox>) daoSession.getInboxDao().loadAll();
        Collections.sort(inboxList);
        Collections.reverse(inboxList);

        inboxAdapter.setList(inboxList);

        if (inboxList.size() > 0) {
            isEmpty.set(false);
        }
    }

    @Override
    public void onClick(Inbox item) {
        gotoFragment(InboxFragment.newInstance(item.getId()));
    }
}
