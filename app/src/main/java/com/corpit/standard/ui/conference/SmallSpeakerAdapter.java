package com.corpit.standard.ui.conference;

import android.support.v4.view.ViewCompat;
import android.util.SparseArray;

import com.corpit.standard.databinding.RowSpeakerSmallBinding;
import com.corpit.standard.model.Speaker;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.List;

public class SmallSpeakerAdapter extends StandardBoundAdapter<Speaker, RowSpeakerSmallBinding> {

    private SparseArray<String> speakerCategories = new SparseArray<>();

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId    The layout to be used for items. It must use data binding.
     * @param listener    Basic item click listener
     */
    SmallSpeakerAdapter(int layoutId, ItemClickListener<Speaker> listener) {
        super(layoutId, listener);
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowSpeakerSmallBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);

        String speakerCategory = speakerCategories.get(holder.getAdapterPosition());
        holder.binding.speakerCategory.setText(speakerCategory);

        boolean hideCategory = holder.getAdapterPosition() > 0
                && speakerCategories.get(holder.getAdapterPosition() - 1).equals(speakerCategory);
        holder.binding.setHideCategory(hideCategory);

        ViewCompat.setTransitionName(holder.binding.speakerPicture, getList().get(holder.getAdapterPosition()).getFullName());
    }

    public void setList(List<Speaker> data, SparseArray<String> speakerCategories) {
        this.speakerCategories = speakerCategories;
        setList(data);
    }
}
