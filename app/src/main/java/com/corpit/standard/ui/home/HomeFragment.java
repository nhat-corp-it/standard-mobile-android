package com.corpit.standard.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentHomeBinding;
import com.corpit.standard.databinding.GridHomeBinding;
import com.corpit.standard.model.Menu;
import com.corpit.standard.model.Menu.MenuType;
import com.corpit.standard.model.ScrollingBanner;
import com.corpit.standard.share.UserManager;
import com.corpit.standard.ui.base.BaseBannerAdapter;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;

import java.util.ArrayList;
import java.util.Collections;

public class HomeFragment extends BaseFragment implements ItemClickListener<Menu> {

    private OnHomeInteractListener mListener;

    private FragmentHomeBinding binding;

    private StandardBoundAdapter<Menu, GridHomeBinding> gridAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        gridAdapter = new StandardBoundAdapter<>(R.layout.grid_home, this);
        gridAdapter.setHasStableIds(true);
        binding.menuList.setAdapter(gridAdapter);
        if (getResources().getBoolean(R.bool.isTablet)) {
            ((GridLayoutManager) binding.menuList.getLayoutManager()).setSpanCount(6);
        }

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadDataFromDb();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHomeInteractListener) {
            mListener = (OnHomeInteractListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnHomeInteractListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void loadDataFromDb() {
        ArrayList<ScrollingBanner> banners = (ArrayList<ScrollingBanner>) daoSession
                .getScrollingBannerDao().loadAll();
        BaseBannerAdapter<ScrollingBanner> adapter = new ScrollingBannerAdapter(getContext(), banners);
        binding.scrollingBanner.setAdapter(adapter);

        ArrayList<Menu> menus = (ArrayList<Menu>) daoSession.getMenuDao().loadAll();
        menus = filterMenu(menus);
        gridAdapter.setList(menus);
    }

    private static ArrayList<Menu> filterMenu(ArrayList<Menu> menuList) {
        ArrayList<Menu> result = new ArrayList<>();
        String role = UserManager.getInstance().getRole();
        for (Menu item : menuList) {
            if (item.getMenuType().contains(MenuType.QUICK_MENU)
                    && item.getPermission().contains(role)) {
                result.add(item);
            }
        }
        Collections.sort(result);

        return result;
    }

    @Override
    public void onClick(Menu item) {
        if (mListener != null) {
            mListener.onMenuTap(item);
        }
    }

    public interface OnHomeInteractListener {
        void onMenuTap(Menu item);
    }
}
