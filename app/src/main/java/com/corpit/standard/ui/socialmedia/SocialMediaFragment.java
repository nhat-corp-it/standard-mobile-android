package com.corpit.standard.ui.socialmedia;


import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentSocialMediaBinding;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SocialMediaFragment extends BaseFragment implements View.OnClickListener {

    public SocialMediaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentSocialMediaBinding binding = FragmentSocialMediaBinding.inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        binding.title.setText(getCurrentFragmentName());

        binding.setListener(this);

        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        Bundle translateBundle = ActivityOptions.makeCustomAnimation(getContext(),
                R.anim.slide_in_left, R.anim.slide_out_left).toBundle();

        String appDeepLink = "";

        String url = "";

        switch (v.getId()) {
            case R.id.facebook_button:
                appDeepLink = "fb://profile/spemembers";
                url = "http://m.facebook.com/spemembers";
                break;
            case R.id.linkedin_button:
                appDeepLink = "linkedin://Society-Petroleum-Engineers-57660";
                url = "http://www.linkedin.com/groups/Society-Petroleum-Engineers-57660/about";
                break;
            case R.id.twitter_button:
                appDeepLink = "http://twitter.com/SPEtweets";
                url = "https://twitter.com/SPEtweets";
                break;
            case R.id.youtube_button:
                appDeepLink = "http://www.youtube.com/2012SPE";
                url = "http://www.youtube.com/2012SPE";
                break;
            case R.id.google_plus_button:
                appDeepLink = "https://plus.google.com/+SpeOrg";
                url = "https://plus.google.com/+SpeOrg";
                break;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(appDeepLink));
            startActivity(intent, translateBundle);
        } catch (Exception e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent, translateBundle);
        }

    }
}
