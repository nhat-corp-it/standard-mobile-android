package com.corpit.standard.ui.eposter;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.corpit.standard.R;
import com.corpit.standard.data.local.DaoHelper;
import com.corpit.standard.databinding.FragmentEposterCategoriesBinding;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.speakers.SpeakerListFragment;
import com.corpit.standard.widget.FixListView;

import java.util.ArrayList;

import static com.corpit.standard.ui.eposter.EPosterListFragment.FILTER_BY_ALL;
import static com.corpit.standard.ui.eposter.EPosterListFragment.FILTER_BY_CATEGORY;

public class EPosterCategoriesFragment extends BaseFragment {

    private FixListView categoryListView;

    public EPosterCategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentEposterCategoriesBinding binding = FragmentEposterCategoriesBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        binding.title.setText(getCurrentFragmentName());

        categoryListView = binding.categoryList;
        initCategoryList();

        binding.byAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(EPosterListFragment.newInstance(FILTER_BY_ALL, ""));
            }
        });

        binding.byCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isVisible = categoryListView.getVisibility() == View.VISIBLE;
                if (isVisible) {
                    categoryListView.setVisibility(View.GONE);
                } else {
                    categoryListView.setVisibility(View.VISIBLE);
                    categoryListView.clearFocus();
                    categoryListView.requestFocus();
                }
            }
        });

        binding.byAuthorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFragment(new SpeakerListFragment());
            }
        });

        return binding.getRoot();
    }

    private void initCategoryList() {
        ArrayList<String> categories = DaoHelper.getEPosterCategoryList();

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.row_simple_item,
                android.R.id.text1, categories);

        categoryListView.setAdapter(adapter);

        categoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoFragment(EPosterListFragment.newInstance(FILTER_BY_CATEGORY, adapter.getItem(position)));
            }
        });
    }

}
