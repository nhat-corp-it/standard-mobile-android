package com.corpit.standard.ui.news;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.databinding.FragmentNewsBinding;
import com.corpit.standard.model.News;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String newsId;

    public NewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param newsId Parameter 1.
     * @return A new instance of fragment NewsFragment.
     */
    public static NewsFragment newInstance(String newsId) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, newsId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            newsId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentNewsBinding binding = FragmentNewsBinding.inflate(inflater, container, false);
        binding.setColorScheme(colorScheme);

        News news = daoSession.getNewsDao().load(newsId);

        binding.setNews(news);

        String content = news.getContent();
        StringBuilder html = new StringBuilder();
        html.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />")
                .append("</head><body>");
        if (content != null && !content.isEmpty()) {
            content = content.replaceAll(" width: [0-9]*px;", "")
                    .replaceAll("font-family:[^;]*;", "")
                    .replaceAll("14px", "18px")
                    .replaceAll("13px", "18px")
                    .replaceAll("12px", "18px")
                    .replaceAll("background-color:[^;]*;", "");
        }
        html.append(content)
                .append("</body>")
                .append("</html>");

        binding.newsContent.loadDataWithBaseURL(null, html.toString(), "text/html",
                "utf-8", null);

        return binding.getRoot();
    }

}
