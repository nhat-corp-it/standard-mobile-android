package com.corpit.standard.ui.base;

/**
 * Created by nhatton on 25/3/18.
 */

public interface Expandable {

    String getText();
    String getIconUrl();
}
