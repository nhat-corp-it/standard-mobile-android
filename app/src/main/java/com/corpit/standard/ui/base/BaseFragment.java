package com.corpit.standard.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.inputmethod.InputMethodManager;

import com.corpit.standard.R;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.ColorScheme;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.ui.home.MainActivity;

/**
 * Created by nhatton on 24/3/18.
 */

public abstract class BaseFragment extends Fragment {

    protected final String TAG = this.getClass().getSimpleName();

    protected DaoSession daoSession;
    protected ColorScheme colorScheme;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        daoSession = Injector.get().daoSession();
        colorScheme = Injector.get().colorScheme();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && getView() != null) {
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    protected void gotoFragment(Fragment fragment) {
        Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (!fragment.getClass().toString().equals(currentFragment.getTag())) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out,
                            R.anim.push_right_in, R.anim.push_right_out)
                    .replace(R.id.fragment_container, fragment, fragment.getClass().toString())
                    .addToBackStack(currentFragment.getClass().toString())
                    .commit();
        }
    }

    protected String getCurrentFragmentName() {
        if (getActivity() instanceof MainActivity) {
            return ((MainActivity) getActivity()).currentFragmentName;
        }
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
        Injector.get().eventTracker().onPageStart(TAG);
    }

    @Override
    public void onPause() {
        super.onPause();
        Injector.get().eventTracker().onPageEnd(TAG);
    }
}
