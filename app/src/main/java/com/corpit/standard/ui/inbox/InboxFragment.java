package com.corpit.standard.ui.inbox;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.databinding.FragmentInboxBinding;
import com.corpit.standard.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InboxFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InboxFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String inboxId;

    public InboxFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param inboxId Parameter 1.
     * @return A new instance of fragment InboxFragment.
     */
    public static InboxFragment newInstance(String inboxId) {
        InboxFragment fragment = new InboxFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, inboxId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            inboxId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentInboxBinding binding = FragmentInboxBinding
                .inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        binding.setInbox(daoSession.getInboxDao().load(inboxId));

        return binding.getRoot();
    }

}
