package com.corpit.standard.ui.generalinfo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentGeneralInfoCategoriesBinding;
import com.corpit.standard.model.GeneralInfo;
import com.corpit.standard.model.GeneralInfoDao;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.base.StringArrayAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GeneralInfoCategoriesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GeneralInfoCategoriesFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String menuId;

    public GeneralInfoCategoriesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param mainCategory Parameter 1.
     * @return A new instance of fragment GeneralInfoCategoriesFragment.
     */
    public static GeneralInfoCategoriesFragment newInstance(String mainCategory) {
        GeneralInfoCategoriesFragment fragment = new GeneralInfoCategoriesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, mainCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menuId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentGeneralInfoCategoriesBinding binding = FragmentGeneralInfoCategoriesBinding
                .inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        binding.title.setText(getCurrentFragmentName());

        final ArrayList<GeneralInfo> generalInfos = (ArrayList<GeneralInfo>) daoSession
                .getGeneralInfoDao()
                .queryBuilder()
                .where(GeneralInfoDao.Properties.MenuId.eq(menuId))
                .list();

        ArrayList<String> categoryStrings = new ArrayList<>();
        for (GeneralInfo element : generalInfos) {
            categoryStrings.add(element.getTitle());
        }

        final StringArrayAdapter adapter = new StringArrayAdapter(getContext(), categoryStrings);

        ListView listView = binding.categoryList;
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GeneralInfo info = generalInfos.get(position);
                if (info.getContent().isEmpty()) {
                    gotoFragment(GeneralInfoCategoriesFragment.newInstance(info.getTitle()));
                } else {
                    gotoFragment(GeneralInfoFragment.newInstance(info.getId()));
                }
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        ArrayList<GeneralInfo> generalInfos = (ArrayList<GeneralInfo>) daoSession
                .getGeneralInfoDao()
                .queryBuilder()
                .where(GeneralInfoDao.Properties.MenuId.eq(menuId))
                .list();

        if (generalInfos.size() == 1) {
            GeneralInfoFragment infoFragment = GeneralInfoFragment.newInstance(generalInfos.get(0).getId());
            if (!infoFragment.getClass().toString().equals(this.getTag())) {
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, infoFragment, infoFragment.getClass().toString())
                        .addToBackStack(null)
                        .commit();
            }

        }
    }
}
