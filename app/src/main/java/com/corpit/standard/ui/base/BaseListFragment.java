package com.corpit.standard.ui.base;

import android.content.Context;
import android.content.res.Configuration;
import android.databinding.ObservableBoolean;
import android.databinding.ViewDataBinding;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.corpit.standard.R;
import com.corpit.standard.databinding.BaseListBinding;
import com.corpit.standard.ui.base.databoundrecyclerview.ListItem;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;
import com.corpit.standard.util.SizeConverter;
import com.corpit.standard.util.VectorUtil;
import com.corpit.standard.widget.AssortView;

import org.greenrobot.greendao.async.AsyncOperation;
import org.greenrobot.greendao.async.AsyncOperationListener;
import org.greenrobot.greendao.async.AsyncSession;
import org.greenrobot.greendao.query.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The base class for fragments that deals with a list of product, with search bar, sectioned and
 * assort view. The model class should implement {@link ListItem} and the adapter should
 * extend {@link StandardBoundAdapter}.
 *
 * @param <T> The model class used in this fragment
 * @author nhatton
 */
public abstract class BaseListFragment<T extends ListItem<T> & WithDetails> extends BaseFragment {

    private static final int DELAY_REMOVE_WINDOW = 1200;

    private static final int SCROLL_ITEMS_LIMIT = 50;

    private static final int STROKE_WIDTH = 2;

    private static final String KEY_LIST_STATE = "listState";

    protected final ObservableBoolean isDataReady = new ObservableBoolean(false);

    protected RecyclerView listView;
    protected StandardBoundAdapter<T, ? extends ViewDataBinding> adapter;
    protected BaseListBinding binding;
    private RemoveWindow mRemoveWindow = new RemoveWindow();
    private WindowManager mWindowManager;
    private TextView mDialogText;
    private boolean mShowing;
    private boolean mReady;
    private char mPrevLetter = Character.MIN_VALUE;
    private Parcelable state;
    private RecyclerView.SmoothScroller smoothScroller;
    private Handler mHandler = new Handler();
    private HashMap<String, Integer> letterHashMap = new HashMap<>();
    private AtomicBoolean isFiltering = new AtomicBoolean(false);
    private AsyncOperationListener dataListener = new AsyncOperationListener() {
        @SuppressWarnings("unchecked")
        @Override
        public void onAsyncOperationCompleted(final AsyncOperation operation) {

            isDataReady.set(true);

            if (getActivity() == null) return;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.setList((ArrayList<T>) operation.getResult());
                    if (state != null) {
                        listView.getLayoutManager().onRestoreInstanceState(state);
                    }

                }
            });
            filterHashMap((ArrayList<T>) operation.getResult());
        }
    };
    private boolean isLandscape;

    private void filterHashMap(final ArrayList<T> data) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit((new Runnable() {
            @Override
            public void run() {
                isFiltering.set(true);
                HashMap<String, Integer> map = new HashMap<>();
                if (!data.isEmpty()) {
                    final int size = data.size();
                    for (int i = 1; i < size; i++) {
                        T element = data.get(size - i - 1);
                        map.put(element.getSectionName(), size - i - 1);
                    }
                }

                letterHashMap = map;
                isFiltering.set(false);
            }
        }));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = BaseListBinding.inflate(inflater, container, false);

        binding.setColorScheme(colorScheme);

        binding.setIsDataReady(isDataReady);

        listView = binding.list;

        return binding.getRoot();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        state = listView.getLayoutManager().onSaveInstanceState();
        outState.putParcelable(KEY_LIST_STATE, state);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            state = savedInstanceState.getParcelable(KEY_LIST_STATE);
        }
        if (state != null) {
            listView.getLayoutManager().onRestoreInstanceState(state);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prepareLetterOverlay((ViewGroup) view);
        binding.title.setText(getTitle());

        isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

        setUpListView();
        if (!isLandscape) {
            setUpAssortView();
        }
        setUpSearchBar();

        setUpListBy(binding.listByText);

    }

    /**
     * Only override if need to have a textview that display the category of the list
     *
     * @param listByText category text
     */
    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected void setUpListBy(TextView listByText) {

    }

    @Override
    public void onStart() {
        super.onStart();

        if (!isDataReady.get()) {
            loadDataFromDb();
        } else {
            if (state != null) {
                listView.getLayoutManager().onRestoreInstanceState(state);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mReady = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        state = listView.getLayoutManager().onSaveInstanceState();
        removeWindow();
        if (mReady && mDialogText != null && mDialogText.getParent() != null) {
            mWindowManager.removeView(mDialogText);
        }
        mReady = false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mReady && mDialogText != null) {
            mWindowManager.removeView(mDialogText);
        }
        mWindowManager = null;
        mReady = false;
        mDialogText = null;
    }

    protected abstract String getTitle();

    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected abstract StandardBoundAdapter<T, ? extends ViewDataBinding> getAdapter();

    private void setUpListView() {
        if (!isDataReady.get()) {
            adapter = getAdapter();
            adapter.setHasStableIds(true);
        }

        listView.setAdapter(adapter);

        if (isLandscape) {
            GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 4);
            listView.setLayoutManager(layoutManager);
        } else {

            smoothScroller = new LinearSmoothScroller(getContext()) {
                @Override
                protected int getVerticalSnapPreference() {
                    return LinearSmoothScroller.SNAP_TO_START;
                }
            };

            listView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (mReady && dy != 0) {
                        char firstLetter = ' ';
                        int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager())
                                .findFirstVisibleItemPosition();
                        final String text = adapter.getList().get(firstVisibleItem).getText();
                        if (!TextUtils.isEmpty(text)) {
                            firstLetter = text.charAt(0);
                        }

                        if (!mShowing && firstLetter != mPrevLetter) {
                            mShowing = true;
                            mDialogText.setVisibility(View.VISIBLE);
                        }

                        mDialogText.setText(((Character) firstLetter).toString().toUpperCase());
                        mHandler.removeCallbacks(mRemoveWindow);
                        mHandler.postDelayed(mRemoveWindow, DELAY_REMOVE_WINDOW);
                        mPrevLetter = firstLetter;
                    }
                }
            });
        }
    }

    private void setUpAssortView() {
        binding.assortView.setOnTouchAssortListener(new AssortView.OnTouchAssortListener() {
            @Override
            public void onTouchAssortListener(String s) {
                if (!isFiltering.get() && letterHashMap.get(s) != null) {
                    int position = letterHashMap.get(s);

                    int lastVisibleItemPosition = ((LinearLayoutManager) listView.getLayoutManager())
                            .findLastVisibleItemPosition();
                    int firstVisibleItemPosition = ((LinearLayoutManager) listView.getLayoutManager())
                            .findFirstVisibleItemPosition();

                    if (firstVisibleItemPosition - position > SCROLL_ITEMS_LIMIT) {
                        listView.getLayoutManager().scrollToPosition(position + SCROLL_ITEMS_LIMIT);
                    } else if (position - lastVisibleItemPosition > SCROLL_ITEMS_LIMIT) {
                        listView.getLayoutManager().scrollToPosition(position - SCROLL_ITEMS_LIMIT);
                    }

                    smoothScroller.setTargetPosition(position);

                    listView.getLayoutManager().startSmoothScroll(smoothScroller);
                }
            }

            @Override
            public void onTouchAssortUp() {

            }
        });
    }

    private void setUpSearchBar() {
        GradientDrawable drawable = (GradientDrawable) binding.searchBar.getBackground();
        int strokeColor = daoSession.getColorSchemeDao().loadAll().get(0)
                .getColorMargin1Code();
        drawable.setStroke(SizeConverter.dipToPixel(STROKE_WIDTH, getContext()), strokeColor);

        Drawable searchIcon = VectorUtil.getVector(R.drawable.ic_search, getContext());
        binding.searchBar.setCompoundDrawablesWithIntrinsicBounds(searchIcon, null, null, null);


        binding.searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listView.getRecycledViewPool().clear();
                if (mReady) {
                    search(s.toString().trim().toLowerCase());
                }
            }
        });
        binding.searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });
        binding.clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchBar.getText().clear();
                binding.searchBar.clearFocus();
            }
        });
    }

    private void removeWindow() {
        if (mShowing) {
            mShowing = false;
            mDialogText.setVisibility(View.INVISIBLE);
        }
    }

    private void prepareLetterOverlay(ViewGroup container) {
        mDialogText = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.list_position, container, false);
        mDialogText.setVisibility(View.INVISIBLE);

        mWindowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        mHandler.post(new Runnable() {
            public void run() {
                mReady = true;
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.TYPE_APPLICATION,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
                mWindowManager.addView(mDialogText, lp);
            }
        });
    }

    private void loadDataFromDb() {
        AsyncSession asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(dataListener);
        asyncSession.queryList(fetchData());
    }

    /**
     * @param query string text to search on list of data
     */
    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected void search(String query) {
        AsyncSession asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(dataListener);
        asyncSession.queryList(fetchData(query));
    }

    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected abstract Query<T> fetchData(String query);

    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected Query<T> fetchData() {
        return fetchData("");
    }

    private final class RemoveWindow implements Runnable {
        public void run() {
            removeWindow();
        }
    }

}
