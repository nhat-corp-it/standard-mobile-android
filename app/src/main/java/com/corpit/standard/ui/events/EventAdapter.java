package com.corpit.standard.ui.events;

import android.graphics.drawable.GradientDrawable;

import com.corpit.standard.BR;
import com.corpit.standard.databinding.RowEventBinding;
import com.corpit.standard.di.Injector;
import com.corpit.standard.model.Booth;
import com.corpit.standard.model.DaoSession;
import com.corpit.standard.model.Event;
import com.corpit.standard.ui.base.ItemClickListener;
import com.corpit.standard.ui.base.databoundrecyclerview.DataBoundViewHolder;
import com.corpit.standard.ui.base.databoundrecyclerview.StandardBoundAdapter;
import com.corpit.standard.util.SizeConverter;

import java.util.List;

public class EventAdapter extends StandardBoundAdapter<Event, RowEventBinding> {

    private static final int BORDER_SIZE_DIP = 1;

    private DaoSession daoSession;

    /**
     * Creates a DataBoundAdapter with the given item layout
     *
     * @param layoutId The layout to be used for items. It must use data binding.
     * @param listener Basic item click listener
     */
    EventAdapter(int layoutId, ItemClickListener<Event> listener) {
        super(layoutId, listener);
        this.daoSession = Injector.get().daoSession();
    }

    @Override
    protected void bindItem(DataBoundViewHolder<RowEventBinding> holder, int position, List<Object> payloads) {
        super.bindItem(holder, position, payloads);
        Booth booth = daoSession.getBoothDao().load(getList().get(holder.getAdapterPosition()).getBoothId());
        holder.binding.setVariable(BR.booth, booth);

        GradientDrawable background = (GradientDrawable) holder.binding.getRoot().getBackground();
        int borderSize = SizeConverter.dipToPixel(BORDER_SIZE_DIP, holder.binding.getRoot().getContext());
        background.setStroke(borderSize, getColorScheme().getColorMargin1Code());
    }
}
