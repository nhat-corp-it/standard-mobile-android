package com.corpit.standard.ui.login;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.corpit.standard.R;
import com.corpit.standard.databinding.FragmentVisitorLoginBinding;
import com.corpit.standard.share.Role;
import com.corpit.standard.share.UserManager;
import com.corpit.standard.ui.base.BaseFragment;
import com.corpit.standard.ui.home.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisitorLoginFragment extends BaseFragment {

    private EditText companyNameInput;
    private EditText companyEmailInput;

    public VisitorLoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentVisitorLoginBinding binding = FragmentVisitorLoginBinding.inflate(inflater,
                container, false);

        binding.setColorScheme(colorScheme);

        companyNameInput = binding.companyNameInput;
        companyEmailInput = binding.companyEmailInput;

        binding.visitorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    Toast.makeText(getContext(), "Ok", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        binding.guestLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserManager.getInstance().setRole(Role.GUEST);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return binding.getRoot();
    }

    private boolean isValid() {
        companyNameInput.setError(null);
        companyEmailInput.setError(null);

        boolean isValid = true;
        String companyName = companyNameInput.getText().toString();
        String companyEmail = companyEmailInput.getText().toString();

        if (companyName.isEmpty()) {
            companyNameInput.setError(getString(R.string.error_empty_company));
            isValid = false;
        }

        if (companyEmail.isEmpty()) {
            companyEmailInput.setError(getString(R.string.error_empty_email));
            isValid = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(companyEmail).matches()) {
            companyEmailInput.setError(getString(R.string.error_invalid_email));
            isValid = false;
        }

        return isValid;
    }

}
