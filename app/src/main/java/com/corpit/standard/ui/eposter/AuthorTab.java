package com.corpit.standard.ui.eposter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.standard.databinding.TabAuthorBinding;
import com.corpit.standard.model.EPoster;
import com.corpit.standard.ui.base.BaseFragment;

public class AuthorTab extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";

    private String eposterId;

    public AuthorTab() {
        // Required empty public constructor
    }

    public static AuthorTab newInstance(String eposterId) {
        AuthorTab fragment = new AuthorTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, eposterId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eposterId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TabAuthorBinding binding = TabAuthorBinding.inflate(inflater, container, false);

        EPoster ePoster = daoSession.getEPosterDao().load(eposterId);

        binding.setEposter(ePoster);

        return binding.getRoot();
    }
}
