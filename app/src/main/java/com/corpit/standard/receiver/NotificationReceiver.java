package com.corpit.standard.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.corpit.standard.ui.home.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

public class NotificationReceiver extends BroadcastReceiver {

    private Handler handler = new Handler();

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onReceive(final Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        if (intent.getAction().equals(JPushInterface.ACTION_NOTIFICATION_OPENED)) {
            if (!MainActivity.isForeground) {
                Intent newIntent = new Intent(context, MainActivity.class);
                context.startActivity(newIntent);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        processCustomMessage(context, bundle);
                    }
                }, 1500);

            } else {
                processCustomMessage(context, bundle);
            }
        }
    }

    private void processCustomMessage(Context context, Bundle bundle) {
        if (MainActivity.isForeground) {
            String title = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);
            String message = bundle.getString(JPushInterface.EXTRA_ALERT);
            String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
            Intent msgIntent = new Intent(MainActivity.MESSAGE_RECEIVED_ACTION);
            msgIntent.putExtra(MainActivity.KEY_TITLE, title);
            msgIntent.putExtra(MainActivity.KEY_MESSAGE, message);
            if (!TextUtils.isEmpty(extras)) {
                try {
                    JSONObject extraJson = new JSONObject(extras);
                    if (extraJson.length() > 0) {
                        msgIntent.putExtra(MainActivity.KEY_EXTRAS, extras);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            LocalBroadcastManager.getInstance(context).sendBroadcast(msgIntent);
        }
    }
}
