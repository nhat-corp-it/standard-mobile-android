package com.corpit.standard.share;

import android.app.Application;

import com.corpit.standard.BuildConfig;
import com.corpit.standard.di.AppComponent;
import com.corpit.standard.di.DaggerAppComponent;
import com.corpit.standard.di.TrackModule;
import com.corpit.standard.di.module.AppModule;
import com.corpit.standard.di.module.DaoModule;
import com.corpit.standard.di.module.ServiceModule;
import com.squareup.leakcanary.LeakCanary;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by nhatton on 24/3/18.
 */

public class StandardApp extends Application {

    private static StandardApp INSTANCE;

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);

        if (BuildConfig.DEBUG) {
            JPushInterface.setDebugMode(true);
        }
        JPushInterface.init(this);

        INSTANCE = this;
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .serviceModule(new ServiceModule())
                .daoModule(new DaoModule())
                .trackModule(new TrackModule(this))
                .build();
    }

    public static StandardApp get() {
        return INSTANCE;
    }

    public AppComponent getComponent() {
        return mAppComponent;
    }

}
