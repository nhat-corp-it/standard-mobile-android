package com.corpit.standard.share;

import android.app.Activity;
import android.content.SharedPreferences;

import com.corpit.standard.di.Injector;

public class UserManager {

    private static final String KEY_ROLE = "role";
    private static final String KEY_LANGUAGE = "language";
    private static final String KEY_USER_ID = "userId";

    private static volatile UserManager sInstance;
    @Role
    private String mRole;
    private String mUserId;
    private Language mLanguage;

    private SharedPreferences sharedPref;

    private UserManager() {
        this.sharedPref = Injector.get().sharedPreferences();
        mRole = Role.GUEST;
        mLanguage = Language.English;
    }

    public static UserManager getInstance() {
        if (sInstance == null) {
            synchronized (UserManager.class) {
                if (sInstance == null) {
                    sInstance = new UserManager();
                }
            }
        }
        return sInstance;
    }

    @Role
    public String getRole() {
        return mRole;
    }

    public void setRole(@Role String role) {
        synchronized (this) {
            this.mRole = role;
            cacheUser();
        }
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        this.mUserId = userId;
        cacheUser();
    }

    public Language getLanguage() {
        return mLanguage;
    }

    public void setLanguage(Language language, Activity activity) {
        this.mLanguage = language;
        cacheUser();
        activity.recreate();
    }

    private void cacheUser() {
        sharedPref.edit().putString(KEY_ROLE, mRole).apply();
        sharedPref.edit().putString(KEY_LANGUAGE, mLanguage.locale).apply();
        sharedPref.edit().putString(KEY_USER_ID, mUserId).apply();
    }

    public void loadUser() {
        mRole = sharedPref.getString(KEY_ROLE, Role.GUEST);
        mLanguage = Language.fromLocale(sharedPref.getString(KEY_LANGUAGE, Language.English.locale));
        mUserId = sharedPref.getString(KEY_USER_ID, "");
    }

    public void clear() {
        SharedPreferences sharedPref = Injector.get().sharedPreferences();
        sharedPref.edit().clear().apply();
        mRole = Role.GUEST;
    }

    public enum Language {
        English("en"),
        Chinese("zh"),
        Thailand("th");

        public String locale;

        Language(String locale) {
            this.locale = locale;
        }

        public static Language fromLocale(String locale) {
            switch (locale) {
                case "en":
                    return English;
                case "zh":
                    return Chinese;
                case "th":
                    return Thailand;
                default:
                    return English;
            }
        }
    }
}
