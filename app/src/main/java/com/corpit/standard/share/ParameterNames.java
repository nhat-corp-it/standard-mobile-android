package com.corpit.standard.share;

/**
 * Created by nhatton on 23/3/18.
 * Can move the parameter names back to the model class
 */

public class ParameterNames {

    public static final String DELETE_FLAG = "deleteFlag";
    public static final String UPDATED_DATE = "UpdatedDate";

    public static final String ACCOUNT_ID = "a_ID";
    public static final String ACCOUNT_FULL_NAME = "a_fullname";
    public static final String ACCOUNT_TYPE = "a_type";
    public static final String ACCOUNT_SALUTATION = "a_sal";
    public static final String ACCOUNT_FIRST_NAME = "a_fname";
    public static final String ACCOUNT_LAST_NAME = "a_lname";
    public static final String ACCOUNT_DESIGNATION = "a_designation";
    public static final String ACCOUNT_EMAIL = "a_email";
    public static final String ACCOUNT_COMPANY = "a_company";
    public static final String ACCOUNT_ADDRESS_OFFICE = "a_addressOfc";
    public static final String ACCOUNT_ADDRESS_HOME = "a_addressHome";
    public static final String ACCOUNT_COUNTRY = "a_country";
    public static final String ACCOUNT_TEL = "a_Tel";
    public static final String ACCOUNT_MOBILE = "a_Mobile";
    public static final String ACCOUNT_LOGIN_NAME = "a_loginName";
    public static final String ACCOUNT_PASSWORD = "a_Password";
    public static final String ACCOUNT_IS_ACTIVATED = "a_isActivated";

    public static final String BOOKMARK_ID = "bmark_ID";
    public static final String BOOKMARK_USER_ID = "bmark_userid";
    public static final String BOOKMARK_EXHIBITOR_ID = "bmark_exhid";

    public static final String BOOTH_ID = "vb_ID";
    public static final String BOOTH_NAME = "vb_name";
    public static final String BOOTH_LOCATION = "vb_location";
    public static final String BOOTH_WIDTH = "vb_width";
    public static final String BOOTH_HEIGHT = "vb_height";
    public static final String BOOTH_X = "vb_x";
    public static final String BOOTH_Y = "vb_y";

    public static final String BOTTOM_BANNER_ID = "bb_ID";
    public static final String BOTTOM_BANNER_TITLE = "bb_title";
    public static final String BOTTOM_BANNER_IMAGE = "bb_image";

    public static final String BUSINESS_MATCHING_ID = "bm_ID";
    public static final String BUSINESS_MATCHING_VISITOR_ID = "bm_vid";
    public static final String BUSINESS_MATCHING_EXHIBITOR_ID = "bm_eid";
    public static final String BUSINESS_MATCHING_DATE = "bm_date";
    public static final String BUSINESS_MATCHING_TIME = "bm_time";
    public static final String BUSINESS_MATCHING_STATUS = "bm_status";
    public static final String BUSINESS_MATCHING_EXHIBITOR_COMMENT = "bm_ecomment";
    public static final String BUSINESS_MATCHING_REQUESTED_TIME = "bm_reqtime";
    public static final String BUSINESS_MATCHING_VISITOR_COMMENT = "bm_vcomment";
    public static final String BUSINESS_MATCHING_INTERESTED_PRODUCT = "bm_interestedProduct";

    public static final String COLOR_MARGIN_1 = "color_margin_1";
    public static final String COLOR_MARGIN_2 = "color_margin_2";
    public static final String COLOR_BUTTON_1 = "color_button_1";
    public static final String COLOR_BUTTON_2 = "color_button_2";
    public static final String COLOR_COLUMN_1 = "color_column_1";
    public static final String COLOR_COLUMN_2 = "color_column_2";
    public static final String COLOR_BACKGROUND_1 = "color_background_1";
    public static final String COLOR_BACKGROUND_2 = "color_background_2";
    public static final String COLOR_FONT_1 = "color_font_1";
    public static final String COLOR_FONT_2 = "color_font_2";
    public static final String COLOR_FONT_3 = "color_font_3";
    public static final String COLOR_TIMESTAMP = "timestamp";

    public static final String CONFERENCE_ID = "c_ID";
    public static final String CONFERENCE_CAT = "c_cc";
    public static final String CONFERENCE_DATE = "c_date";
    public static final String CONFERENCE_START_TIME = "c_stime";
    public static final String CONFERENCE_TITLE = "c_title";
    public static final String CONFERENCE_VENUE = "c_venue";
    public static final String CONFERENCE_LIVE_QA = "c_liveQA";
    public static final String CONFERENCE_BRIEF = "c_brief";
    public static final String CONFERENCE_PARENT_CONF_ID = "c_parentconfID";
    public static final String CONFERENCE_END_TIME = "c_etime";

    public static final String CONFERENCE_CAT_ID = "cc_ID";
    public static final String CONFERENCE_CAT_NAME = "cc_categoryname";
    public static final String CONFERENCE_CAT_SEQ = "cc_seq";

    public static final String CONF_SPEAKER_ID = "cspeaker_ID";
    public static final String CONF_SPEAKER_SPEAKER_ID = "cspeaker_speakerID";
    public static final String CONF_SPEAKER_SPEAKER_CAT = "cspeaker_category";
    public static final String CONF_SPEAKER_SPEAKER_CAT_SEQ = "speakerCat_seq";
    public static final String CONF_SPEAKER_CONF_ID = "cspeaker_conferenceID";
    public static final String CONF_SPEAKER_CONF_CAT = "cspeaker_confcategory";

    public static final String CONF_SPONSOR_ID = "csponsor_ID";
    public static final String CONF_SPONSOR_SPONSOR_ID = "csponsor_sponsorID";
    public static final String CONF_SPONSOR_SPONSOR_CAT = "csponsor_scategory";
    public static final String CONF_SPONSOR_CONF_ID = "csponsor_connferenceID";

    public static final String CONFIG_ID = "config_id";
    public static final String CONFIG_TIME_STAMP = "timestamp";
    public static final String CONFIG_SHOW_NAME = "showName";
    public static final String CONFIG_VENUE = "showVenue";
    public static final String CONFIG_SHOW_START_DATE = "showStartDate";
    public static final String CONFIG_SHOW_END_DATE = "showEndDate";
    public static final String CONFIG_SURVEY_START_DATE = "surveyStartDate";
    public static final String CONFIG_SURVEY_END_DATE = "surveyEndDate";
    public static final String CONFIG_REG_URL = "visitorRegUrl";
    public static final String CONFIG_LANGUAGE = "systemLanguage";
    public static final String CONFIG_TEMPLATE = "homePageTemplate";
    public static final String CONFIG_APP_ICON = "appIcon";
    public static final String CONFIG_ANDROID_VERSION = "AndroidVersion";
    public static final String CONFIG_ANDROID_LINK = "AndroidDownloadLink";
    public static final String CONFIG_IOS_VERSION = "iOSVersion";
    public static final String CONFIG_IOS_LINK = "iOSDownloadLink";
    public static final String CONFIG_ENABLE_ACCOUNT = "enableAccount";

    public static final String CONFIG_IMG_ID = "img_ID";
    public static final String CONFIG_IMG_KEY = "img_key";
    public static final String CONFIG_IMG_FILE = "img_file";

    public static final String DAY_ID = "d_ID";
    public static final String DAY_KEY = "d_key";
    public static final String DAY_DATE = "d_date";
    public static final String DAY_YEAR = "d_year";

    public static final String E_POSTER_ID = "ep_ID";
    public static final String E_POSTER_TITLE = "ep_title";
    public static final String E_POSTER_CAT = "ep_category";
    public static final String E_POSTER_AUTHOR = "ep_author";
    public static final String E_POSTER_CO_AUTHOR = "ep_coauthor";
    public static final String E_POSTER_VENUE = "ep_venue";
    public static final String E_POSTER_DURATION = "ep_duration";
    public static final String E_POSTER_FILE = "ep_file";
    public static final String E_POSTER_ABSTRACT = "ep_abstract";

    public static final String EVENT_ID = "e_ID";
    public static final String EVENT_IMAGE = "e_image";
    public static final String EVENT_DATE = "e_date";
    public static final String EVENT_TIME = "e_time";
    public static final String EVENT_TITLE = "e_title";
    public static final String EVENT_CAT = "e_eventcategory";
    public static final String EVENT_VENUE = "e_venue";
    public static final String EVENT_BRIEF = "e_brief";

    public static final String EXHIBITING_COMPANY_ID = "ec_ID";
    public static final String EXHIBITING_COMPANY_NAME = "ec_name";
    public static final String EXHIBITING_COMPANY_ADDRESS = "ec_address";
    public static final String EXHIBITING_COMPANY_COUNTRY = "ec_country";
    public static final String EXHIBITING_COMPANY_EMAIL = "ec_email";
    public static final String EXHIBITING_COMPANY_CONTACT_NO = "ec_contactno";
    public static final String EXHIBITING_COMPANY_WEBSITE = "ec_website";
    public static final String EXHIBITING_COMPANY_BROCHURE = "ec_brochure";
    public static final String EXHIBITING_COMPANY_LOGO = "ec_logo";
    public static final String EXHIBITING_COMPANY_PROFILE = "ec_companyprofile";
    public static final String EXHIBITING_COMPANY_TYPE = "ec_companytype";
    public static final String EXHIBITING_COMPANY_HALL = "ec_hall";
    public static final String EXHIBITING_COMPANY_BOOTH = "ec_booth";
    public static final String EXHIBITING_COMPANY_TRENDS = "ec_trends";
    public static final String EXHIBITING_COMPANY_MARKET_SEGMENT = "ec_marketseg";
    public static final String EXHIBITING_COMPANY_CONTACT_PERSON_ID = "contactpersonID";
    public static final String EXHIBITING_COMPANY_PRODUCT = "ec_product";

    public static final String GENERAL_INFO_ID = "gi_ID";
    public static final String GENERAL_INFO_TITLE = "gi_title";
    public static final String GENERAL_INFO_CONTENT = "gi_content";
    public static final String GENERAL_INFO_ICON = "gi_icon";
    public static final String GENERAL_INFO_MAIN_ID = "gi_mainID";
    public static final String GENERAL_INFO_SEQ = "gi_seq";
    public static final String GENERAL_INFO_MENU_ID = "menuID";

    public static final String HOME_PAGE_ID = "hp_ID";
    public static final String HOME_PAGE_MAIN_TITLE = "hp_mtitle";
    public static final String HOME_PAGE_SUB_TITLE = "hp_stitle";
    public static final String HOME_PAGE_CONTENT = "hp_content";

    public static final String INBOX_ID = "i_ID";
    public static final String INBOX_TITLE = "i_title";
    public static final String INBOX_CONTENT = "i_content";
    public static final String INBOX_DATE = "i_date";
    public static final String INBOX_TIME = "i_time";
    public static final String INBOX_IS_PUSH = "i_isPush";

    public static final String LOCATION_ID = "l_ID";
    public static final String LOCATION_NAME = "l_name";
    public static final String LOCATION_MAP = "l_image";
    public static final String LOCATION_MENU_ID = "menuID";

    public static final String MENU_ID = "m_id";
    public static final String MENU_ORDER_ID = "m_OrderID";
    public static final String MENU_PARENT_MENU_ID = "m_parentMenuID";
    public static final String MENU_TYPE = "m_menuType";
    public static final String MENU_PAGE_TYPE = "m_PageType";
    public static final String MENU_TITLE_EN = "m_TitleEn";
    public static final String MENU_TITLE_CN = "m_TitleCn";
    public static final String MENU_WEB_LINK = "m_weblink";
    public static final String MENU_PERMISSION_CONTROL = "m_permissioncontrol";
    public static final String MENU_ICON_URL = "m_IconUrl";
    public static final String MENU_STATUS = "status";
    public static final String MENU_TIMESTAMP = "timestamp";

    public static final String MOMENT_ID = "m_ID";
    public static final String MOMENT_TEXT_COMMENT = "m_txtcomment";
    public static final String MOMENT_PHOTO_COMMENT = "m_photocomment";
    public static final String MOMENT_STATUS = "m_status";
    public static final String MOMENT_ADDED_TIME = "m_addedtime";

    public static final String NEWS_ID = "n_ID";
    public static final String NEWS_TITLE = "n_title";
    public static final String NEWS_CONTENT = "n_content";
    public static final String NEWS_PICTURE = "n_picture";
    public static final String NEWS_MENU_ID = "menuID";

    public static final String NEWSLETTER_ID = "nl_ID";
    public static final String NEWSLETTER_TITLE = "nl_title";
    public static final String NEWSLETTER_ISSUE = "nl_issue";
    public static final String NEWSLETTER_ISSUE_DATE = "nl_doissue";
    public static final String NEWSLETTER_COVER = "nl_cover";
    public static final String NEWSLETTER_DOWNLOAD_LINK = "nl_dlink";
    public static final String NEWSLETTER_TYPE_ID = "menuID";

    public static final String NEWS_TYPE_ID = "nt_ID";
    public static final String NEWS_TYPE_NAME = "nt_name";

    public static final String PRODUCT_ID = "p_ID";
    public static final String PRODUCT_ORDER = "sororder";
    public static final String PRODUCT_NAME = "p_name";
    public static final String PRODUCT_CAT = "p_category";
    public static final String PRODUCT_MAIN_PRODUCT = "p_mainproductID";

    public static final String PRODUCT_CAT_ID = "pc_ID";
    public static final String PRODUCT_CAT_NAME = "pc_name";

    public static final String REWARD_ID = "rw_ID";
    public static final String REWARD_TITLE = "rw_title1";
    public static final String REWARD_TITLE_OTHER = "rw_title2";
    public static final String REWARD_IMAGE = "rw_image1";
    public static final String REWARD_IMAGE_OTHER = "rw_image2";
    public static final String REWARD_PASSWORD = "rw_password";
    public static final String REWARD_CONTENT = "rw_content1";
    public static final String REWARD_CONTENT_OTHER = "rw_content2";

    public static final String SCROLLING_BANNER_ID = "sb_ID";
    public static final String SCROLLING_BANNER_TITLE = "sb_title";
    public static final String SCROLLING_BANNER_IMAGE = "sb_image";

    public static final String SPEAKER_ID = "s_ID";
    public static final String SPEAKER_FULL_NAME = "s_fullname";
    public static final String SPEAKER_JOB = "s_job";
    public static final String SPEAKER_COMPANY = "s_company";
    public static final String SPEAKER_EMAIL = "s_email";
    public static final String SPEAKER_MOBILE = "s_mobile";
    public static final String SPEAKER_ADDRESS = "s_address";
    public static final String SPEAKER_COUNTRY = "s_country";
    public static final String SPEAKER_BIO = "s_bio";
    public static final String SPEAKER_PROFILE_PIC = "s_profilepic";

    public static final String SPEAKER_CAT_ID = "speakerCat_ID";
    public static final String SPEAKER_CAT_NAME = "speakerCat_name";
    public static final String SPEAKER_CAT_SEQ = "speakerCat_seq";

    public static final String SPECIAL_EVENT_ID = "spe_ID";
    public static final String SPECIAL_EVENT_DATE = "spe_date";
    public static final String SPECIAL_EVENT_TIME = "spe_time";
    public static final String SPECIAL_EVENT_TITLE = "spe_title";
    public static final String SPECIAL_EVENT_VENUE = "spe_venue";
    public static final String SPECIAL_EVENT_BRIEF = "spe_brief";
    public static final String SPECIAL_EVENT_IMAGE = "spe_image";

    public static final String SPONSOR_ID = "s_ID";
    public static final String SPONSOR_MENU_ID = "menuID";
    public static final String SPONSOR_SEQ = "s_seq";
    public static final String SPONSOR_CAT = "s_category";
    public static final String SPONSOR_NAME = "s_name";
    public static final String SPONSOR_LOGO = "s_logo";
    public static final String SPONSOR_CONTENT = "s_content";
    public static final String SPONSOR_EXHIBITOR_ID = "s_exhID";

    public static final String SPONSOR_CAT_ID = "sponsorCat_ID";
    public static final String SPONSOR_CAT_NAME = "sponsorCat_name";
    public static final String SPONSOR_CAT_SEQ = "sponsorCat_seq";

    public static final String TIME_LIST_ID = "tl_ID";
    public static final String TIME_LIST_SLOT = "tl_slot";

}
