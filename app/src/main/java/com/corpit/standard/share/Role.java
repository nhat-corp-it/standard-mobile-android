package com.corpit.standard.share;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({Role.VIP, Role.EXHIBITOR, Role.DELEGATE, Role.VISITOR, Role.GUEST})
@Retention(RetentionPolicy.SOURCE)
public @interface Role {

    String VIP = "VIP";
    String EXHIBITOR = "Exhibitor";
    String DELEGATE = "Delegate";
    String VISITOR = "Visitor";
    String GUEST = "Guest";
}
