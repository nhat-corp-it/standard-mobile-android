# Standard app for Android

## Customize for new event

To create app for new event:

1. Fork the [standard repo](https://bitbucket.org/nhat-corp-it/standard-mobile-android/)

1. Change app icon

1. Change variables in gradle.properties
    - application_id: package name to put on google play. Should follow format: com.corpit.standard.{eventname}
    - api_root_url: url to the api which usually ends before "MasterAPI"
    - api_end_point: usually "MasterAPI"
    - release_store_file: route to the key file
    - demo_store_file: route to key file for signing demo apk
    - multiple_language_support: enable to display a spinner to choose language
    - event_timezone: for saving event to calendar. Based on java format for timezone. Usualy Asia/{city_name}

1. Modifiy AndroidManifest.xml
    - in ".receiver.NotificationReceiver", change the category to the same as application_id

## Build variants

There are three build types:

- debug: for development
- demo: for sending to client
- release: for upload to play store

There are two product flavors:

- amazon: build using AmazonPinpoint for tracking user interaction
- umeng: build using Umeng for tracking user interaction

How to change build variant: <https://stackoverflow.com/a/45746101/8575792>